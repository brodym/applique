'''
SSL_PCB_COIL_DESIGNER Modules
'''
#To "install a new module" it simply needs to be listed under the 
#moduleID list. Provided that the module follows standard formatting
#it will be loaded as a tab under plug-ins
#To add new module, ensure module contains:
#sslLabel and
#class SSL_designer_Tab(HasTraits)
moduleID = ['module_simulation','module_cadexport']

