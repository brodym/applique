import subprocess
import sys

print("""\n
************************************
* Appliqué Python Module Installer *
************************************
\n""")

version = -1
packageListA = ["termcolor","colorama","pywin32","descartes","svgpathtools","numpy","scipy","matplotlib"]
packageListB = ["pyqt5","pyqtwebkit","traitsui","mayavi"]
def Install_Modules(version):
    if(version != 6 and version != 7):
        print("Python version 3." + str(version) + ".x not supported.\n")
        exit()

    for packageName in packageListA:
        install(packageName)
    
    if(version == 6):
        installURL("https://download.lfd.uci.edu/pythonlibs/w3jqiv8s/VTK-8.2.0-cp36-cp36m-win_amd64.whl")
    elif(version == 7):
        installURL("https://download.lfd.uci.edu/pythonlibs/w3jqiv8s/VTK-8.2.0-cp37-cp37m-win_amd64.whl")

    for packageName in packageListB:
        install(packageName)
        
    print("\n\nModule Installation Complete")
 
#From https://stackoverflow.com/questions/12332975/installing-python-module-within-code
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

def installURL(packageURL):
    subprocess.check_call([sys.executable, "-m", "pip", "install", "-index-url", packageURL])
if len(sys.argv)>1:
    for i,cmd in enumerate(sys.argv):
        if cmd=='-v':
            try:
                version = int(sys.argv[i+1])
            except IndexError:
                print("Error: No Version number entered.\n")
                exit()
            except ValueError:
                print("Error: Version must be numeric.\n")
                exit()
            Install_Modules(version)
        if cmd=='-h':
            print("""Installer Usage:\n
-v [n]  | Python version. For v3.6.x, n=6. For v3.7.x, n=7\n
-h      | Help menu.\n
""")

else:
    print("Please provide version number. Use -h option for info.\n")
exit()


