'''
################################################################################################################
#                                                                                                              #
#  **********************************************                                                              #
#  * Appliqué: Simulation Module                *                                                              #
#  * By: Brody Mahoney                          *                                                              #
#  **********************************************                                                              #
#                                                                                                              #
#                                                                                                              #
# This module provides a modeling platform leveraging a combination of                                         #
# FastHenry2, FasterCap and SPICE. The result is an accurate AC model of                                       #
# a small-scale PCB Based inductor with multiple layers.                                                       #
#                                                                                                              #
################################################################################################################
'''
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
from traits.api import HasTraits, Range, Instance, on_trait_change, String, Button, Enum, Bool, List
from traitsui.api import View, Item, Group, HSplit, DirectoryEditor, FileEditor, TableEditor
from traitsui.table_column import ObjectColumn
import config
import subprocess
import time
import win32com.client as win32
import threading
from win32process import DETACHED_PROCESS
from util import *
from sys import stdout
from shutil import copyfile
import os
import re

from descartes.patch import PolygonPatch

sslLabel = 'Model Simulation'

class FastHenry2_sub(HasTraits):
    #Note auto mode will run all n-port, single frequency
    typeExp = Enum('n-Port: Layered Coil','1-Port: Lumped Coil') #This will always be n-port for model
    unitsExp = Enum('mil','mm')
    sigma = String('5.8e7')
    sweepFreq_Bool = Bool(False)
    singleFreqHz = String('13.56e6')
    freq_minHz = String('5.00e6')
    freq_maxHz = String('25.0e6')
    points_per_decade = String('100')
    points_per_span = String('10')
    convert_pps_ppd = Button('Convert PPS -> PPD')
    fasthenryWriteLoc = "."
    heightFils = Range(1,100,1)
    widthFils = Range(1,100,1)
    ratHeightFils = String('1.0')
    ratWidthFils = String('1.0')
    autoCalcFils_Bool = Bool(True)
    autoCalcFils_tooltip = "Automatically calculate the number of filaments based on skin effect at frequency of operation."
    maxRatio = String('6.0')
    expFileName = String('fasthenry.inp')
    expHenry_Button = Button('Generate Fast Henry inp File')
    fastHenryTabView = View(
                        Item('typeExp', label='Coil Configuration'),
                        Item('unitsExp', label='Units'),
                        Item('sigma', label='Conductor Conductivity(S/m)'),
                        Item('sweepFreq_Bool', label='Frequency Sweep Enabled'),
                        Item('_'),
                        Group(
                            Item('singleFreqHz', label='Single Frequency Analysis'),
                            visible_when='not sweepFreq_Bool'
                            ),
                        Group(
                            Item('freq_minHz', label='Minumum Sweep Frequency'),
                            Item('freq_maxHz', label='Maximum Sweep Frequency'),
                            Item('points_per_decade', label='Sweep Points per Decade (PPD)'),
                            Item('convert_pps_ppd',show_label=False),
                            Item('points_per_span', label='Sweep Points for Frequency Span (PPS)'),
                            visible_when='sweepFreq_Bool'
                            ),
                        Item('_'),
                        Item('fasthenryWriteLoc',editor=DirectoryEditor(),label='File Write Location'),
                        Group(
                            Item('autoCalcFils_Bool', label='Auto-Calculate Filaments', tooltip=autoCalcFils_tooltip),
                            Item('maxRatio', label = 'Maximum Adjacent Filament Ratio', visible_when='autoCalcFils_Bool'),
                            Item('heightFils', label='Number of Vertical Filaments', visible_when='not autoCalcFils_Bool'),
                            Item('widthFils', label='Number of Horizontal Filaments', visible_when='not autoCalcFils_Bool'),
                            Item('ratHeightFils', label='Ratio of Adjacent Vertical Filaments', visible_when='not autoCalcFils_Bool'),
                            Item('ratWidthFils', label='Ratio of Adjacent Horizontal Filaments', visible_when='not autoCalcFils_Bool'),
                            label = 'Filaments',
                            show_border = True
                            ),
                        Item('_'),
                        Item('expFileName', label='Fast Henry inp File Name'),
                        Item('expHenry_Button', show_label=False)
                        )
    #FastHenry2 uses a sweep different than SPICE
    #Need to convert for convenience
    @on_trait_change('convert_pps_ppd')
    def conv_pps_ppd(self):
        if(is_number(self.points_per_span) and is_number(self.freq_maxHz) and is_number(self.freq_minHz)):
            self.points_per_decade=str(float(self.points_per_span)/np.log10(float(self.freq_maxHz)/float(self.freq_minHz)))
    #This is the main FastHenry2 File Export Methos
    @on_trait_change('expHenry_Button')
    def export_fast_henry(self):
        #modify sigma for units
        #Either mm or mil. Sigma provided in S/m. Needs to be S/mm or S/mil
        try:
            float(self.sigma)
        except ValueError:
            writeConsole('Aborting Export: Conductor Conductivity value not valid','red')
            return
        if(self.unitsExp=='mm'):
            correctedSigma = float(self.sigma)/1000
            commentSigma = 'Sigma Units: ' + self.sigma + ' S/m converted to S/mm'
        elif(self.unitsExp=='mil'):
            correctedSigma = float(self.sigma)/(1000*39.3701)
            commentSigma = 'Sigma Units: ' + self.sigma + ' S/m converted to S/mil'
        #Calculate width and height of segments
        #Width and Height input in mils. The segment width is the trace width and the segment height is the trace thickness
        #segWidth
        if(self.unitsExp=='mm'):
            segWidth=str(mil_to_mm(config.trace_width))
            segHeight=str(mil_to_mm(float(config.copper_thickness)))
        elif(self.unitsExp=='mil'):
            segWidth=str(config.trace_width)
            segHeight=str(config.copper_thickness)
        #Try to write to file
        pcbDict = {}
        pcbDict['copper weight (oz)'] = config.copper_weight
        pcbDict['trace width'] = config.trace_width
        pcbDict['material'] = config.pcb_material
        pcbDict['stack-up order'] = config.pcb_construction
        pcbDict['pre-preg layer thickness'] = config.prepreg_thickness
        pcbDict['core layer thickness'] = config.core_thickness
        #Calculate Filements
        if(self.autoCalcFils_Bool):
            #skin effect:
            if(float(self.maxRatio) < 1.1):
                self.maxRatio = '1.1'
            mu = 1.25662910*10**-6
            if(self.sweepFreq_Bool):
                fastHenryFrequency = self.freq_maxHz
            else:
                fastHenryFrequency = self.singleFreqHz
            self.widthFils, self.heightFils, self.ratWidthFils, self.ratHeightFils = autoCalcFils(float(fastHenryFrequency), float(self.sigma), mu, float(config.trace_width),
                                                                                        float(config.copper_thickness), float(self.ratWidthFils), float(self.ratHeightFils),float(self.maxRatio))
            writeConsole('Auto-Calc Filaments: NW = ' + str(self.widthFils) + ' NH = ' + str(self.heightFils) + ' RW = ' + str(self.ratWidthFils) + ' RH = ' + str(self.ratHeightFils),'green')
        if(self.expFileName==''):
            self.expFileName='fasthenry.inp'
        headerInfo = buildFastHenryHeader(self.expFileName,pcbDict)
        try:
            with open(self.fasthenryWriteLoc+"\\"+self.expFileName,"w") as newFH:
                #Write Header Info: Comments, Defaults
                newFH.write(headerInfo)
                newFH.write("* "+self.expFileName+" generated "+genDateStamp()+"\n")
                newFH.write("* Contact: brodym@uw.edu\n")
                newFH.write("* PCB Data: CopperWeight="+config.copper_weight+"oz/(sq ft) Trace Width=" + segWidth + self.unitsExp + "\n")
                newFH.write(".units "+self.unitsExp+"\n")
                newFH.write("* "+commentSigma+"\n")
                newFH.write(".Default sigma = "+str(correctedSigma)+"\n")
                newFH.write(".Default nhinc="+str(self.heightFils)+" nwinc="+str(self.widthFils)+" rh="+str(self.ratHeightFils)+" rw="+str(self.ratWidthFils)+"\n")
                if config.coil_type == 'Import SVG':
                    xa, ya = importSVG(config.evenLayerSVG,config.evenTransXSVG,config.evenTransYSVG,config.evenScaleSVG,config.evenFlipHorzSVG,config.evenFlipVertSVG,config.evenAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                    xb, yb = importSVG(config.oddLayerSVG,config.oddTransXSVG,config.oddTransYSVG,config.oddScaleSVG,config.oddFlipHorzSVG,config.oddFlipVertSVG,config.oddAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                else:
                    xa, ya, xb, yb = single_tube_master(config.inner_coil_width,config.inner_coil_height,config.inner_corner_rad,config.trace_width, config.trace_spacing, config.turns, config.outer_diam,
                                    config.axial_offset_x, config.axial_offset_y, config.line_segments, config.rectCoilStraightSegsX, config.rectCoilStraightSegsY, config.midPointsSegs, 0, config.numLayers,config.coil_type,0,
                                    config.polyPortLocation,config.polyPortGap)
                if(self.typeExp == '1-Port: Lumped Coil'):
                    writeConsole('Exporting 1 port, Lumped Coil Fast Henry inp File.','green')
                    newFH.write("\n* 1 port, Lumped Coil\n")
                    xfh,yfh,zfh=merge_layers_FH(0,0,0,xa,ya,xb,yb,config.numLayers,config.odd_layer_separation,config.even_layer_separation,float(config.copper_weight))
                    #Generate Nodes
                    newFH.write("\n* Node List:\n")
                    for index, xNode in enumerate(xfh):
                        newFH.write("N"+str(index)+" x="+str(xfh[index])+" y="+str(yfh[index])+" z="+str(zfh[index])+"\n")
                        #Generate Segs
                    newFH.write("\n* Segment List:\n")
                    for index, xNode in enumerate(xfh):
                        if(index+1<len(xfh)):
                            newFH.write("E"+str(index)+" N"+str(index)+" N"+str(index+1)+" w="+segWidth+" h="+segHeight+"\n")
                            #Generate Port
                    newFH.write("\n* Port\n")
                    newFH.write(".external N0 N"+str(len(xfh)-1)+"\n")
                    #For single lumped case. First and last nodes are the port!
                    writeConsole('PCB Layers Merged','green')
                elif(self.typeExp == 'n-Port: Layered Coil'):
                    writeConsole('\nExporting n port, Layered Coil inp File.','green')
                    zArrayList=generate_z_arrays(len(xa),config.odd_layer_separation,config.even_layer_separation,config.numLayers,config.copper_thickness,config.min_tube_radius,'EXPORT',1)
                    i=0
                    masterIndex=0
                    while i < config.numLayers:
                        newFH.write('\n* Layer '+str(i+1)+'\n')
                        if i % 2 == 0:
                            xWrite = xa
                            yWrite = ya
                        else:
                            xWrite = xb
                            yWrite = yb
                        #Generate Nodes
                        newFH.write("\n* Node List "+str(i+1)+":\n")
                        for index, xNode in enumerate(xWrite):
                            newFH.write("N"+str(index+masterIndex)+" x="+str(xWrite[index])+" y="+str(yWrite[index])+" z="+str(zArrayList[i][index])+"\n")
                        #Generate Segs
                        newFH.write("\n* Segment List "+str(i+1)+":\n")
                        for index, xNode in enumerate(xWrite):
                            if(index+1<len(xWrite)):
                                newFH.write("E"+str(index+masterIndex)+" N"+str(index+masterIndex)+" N"+str(index+masterIndex+1)+" w="+segWidth+" h="+segHeight+"\n")
                        #Generate Port
                        newFH.write("\n* Port "+str(i+1)+":\n")
                        #MUST FLIP PORT WITH REVERSE COIL
                        if i % 2 == 0:
                            aPort = masterIndex
                            bPort = len(xWrite)+masterIndex-1
                        else:
                            aPort = masterIndex
                            bPort = len(xWrite)+masterIndex-1
                        newFH.write(".external N"+str(aPort)+" N"+str(bPort)+"\n")
                        i+=1
                        masterIndex+=len(xa)
                if(self.sweepFreq_Bool):
                    newFH.write(".freq fmin="+self.freq_minHz+" fmax="+self.freq_maxHz+" ndec="+self.points_per_decade+"\n")
                else:
                    newFH.write(".freq fmin="+self.singleFreqHz+" fmax="+self.singleFreqHz+" ndec=1\n")
                newFH.write(".end\n")
                newFH.close()
                writeConsole('FastHenry file created Successfully!','green')
        except EnvironmentError:
                writeConsole('Aborting Export: Failed to open ' + self.expFileName + ' for writing.','red')
                return
                
def autoCalcFils(frequency, sigma, mu, trace_width, copper_thickness, ratWidthFils, ratHeightFils, maxRatio):
    #Calculate skin depth
    delta=np.sqrt(1/(np.pi*frequency*mu*sigma))
    #writeConsole('Skin depth = ' + str(delta),'green')
    rw = 1.0
    rh = 1.0
    #Start with some arbitrarily high number
    best_nw = 10000000
    best_nh = 10000000
    delta_r = 0.1
    #maxRatio = 
    Dtotal_W = mil_to_m(trace_width)
    Dtotal_H = mil_to_m(copper_thickness)
    best_rw = rw
    best_rh = rh
    iter_nw = best_nw-1 
    iter_nh = best_nh-1
    #Iterate width with ratio
    while(iter_nw <= best_nw and rw <= maxRatio):
        best_nw = iter_nw
        best_rw = rw
        iter_nw = deltaIter(delta, rw, Dtotal_W)
        if(iter_nw == 1.0):
            best_nw = 1.0
            break
        rw += delta_r
    #Iterate Height
    while(iter_nh <= best_nh and rh <= maxRatio):
        best_nh = iter_nh
        best_rh = rh
        iter_nh = deltaIter(delta, rh, Dtotal_H)
        if(iter_nh == 1.0):
            best_nh = 1.0
            break
        rh +=  delta_r
    return int(best_nw), int(best_nh), str('%.1f'%(best_rw)), str('%.1f'%(best_rh))

def deltaIter(delta, ratio, Dtotal):
    delta_iter = Dtotal
    n = 0
    while(delta_iter > delta):
        n += 1
        sumStop = int(np.ceil((n-1)/2.0))
        tempSum = 0
        for i in range(2, sumStop+1):
            tempSum += ratio**(i-1)
        delta_iter = Dtotal/(2+2*tempSum+ratio**sumStop)
    if(n == 0):
        n = 1
    return n

class FasterCap_sub(HasTraits):
    unitsStatement = String('Units in MKSA only')
    fastcapWriteLoc = "."
    expFileBaseName = String('fastcap')
    dissFactor_Bool = Bool(False)
    dissFactorPCB = String(config.pcbDielDF_init)
    relPermPCB = String('4.5-j0.072') #Standard Sunstone FR-4
    expCap_Button = Button('Generate Fast Cap Files')
    includeDielInterface = Bool(True)
    extendPCBcopperRatio = Range(1.1, 100, 7)
    dielGrid_X = Range(1, 1000, 50)
    dielGrid_Y = Range(1, 1000, 50)
    dielGrid_Z = Range(1, 1000, 10)
    extPerm = String('1.0')
    fastcapScale = Range(1, 100000, 1000)
    fastCapTabView = View(
                        Item('unitsStatement', style='readonly', label='Units'),
                        Item('relPermPCB', label='Relative Permittivity of PCB', tooltip='Enter complex permitivity as e\'-je\''),
                        Item('dissFactor_Bool', label='Enter loss as dissipation factor.'),
                        Item('dissFactorPCB', label='Dissipation Factor', visible_when='dissFactor_Bool'),
                        Item('includeDielInterface', label='Include Dielectric PCB Boundary', tooltip='If not included, PCB traces will resided in a homogeneous region PCB permittivity'),
                        Group(
                            Item('extendPCBcopperRatio', label='PCB extention ratio', tooltip='To avoid FasterCap warnings, the modelled dielectric is extended above and below copper traces'),
                            Item('dielGrid_X', label='Number of X-axis descritizations'),
                            Item('dielGrid_Y', label='Number of Y-axis descritizations'),
                            Item('dielGrid_Z', label='Number of Z-axis descritizations'),
                            Item('extPerm', label='Permitivity of External Environment'),
                            label='Dielectic Boundaries',
                            show_border = True,
                            visible_when='includeDielInterface'
                            ),
                        Item('fastcapScale', label='Geometric Scaling'),
                        Item('_'),
                        Item('fastcapWriteLoc',editor=DirectoryEditor(),label='File Write Location'),
                        Item('expFileBaseName', label='Base name for qui files and lst file.'),
                        Item('_'),
                        Item('expCap_Button', show_label=False)
                        )

    @on_trait_change('expCap_Button')
    def expFasterCapHandler(self):
        #First check permittivity and convert dissipation factor to complex if necessary
        #Check if numbers
        if(self.dissFactor_Bool):
            if(not is_number(self.dissFactorPCB) or not is_number(self.relPermPCB)):
                writeConsole('Check dissipation factor. Not a number.','red')
                return
            else:
                self.relPermPCB = str((float(self.relPermPCB)).real) + '-j'+str(float(config.pcb_diel_perm)*float(config.pcb_diel_DF))
        writeConsole('Exporting FastCap files','green')
        #Convert all units to meters!
        copperThMeter = mil_to_m(config.copper_thickness)
        if config.coil_type == 'Import SVG':
            xaOut, xaIn, yaOut, yaIn = importSVG(config.evenLayerSVG,config.evenTransXSVG,config.evenTransYSVG,config.evenScaleSVG,config.evenFlipHorzSVG,config.evenFlipVertSVG,config.evenAutoCenterSVG,config.sampPointsSVG,config.trace_width,1)
            xbOut, xbIn, ybOut, ybIn = importSVG(config.oddLayerSVG,config.oddTransXSVG,config.oddTransYSVG,config.oddScaleSVG,config.oddFlipHorzSVG,config.oddFlipVertSVG,config.oddAutoCenterSVG,config.sampPointsSVG,config.trace_width,1)
        else:
            xaOut, xaIn, yaOut, yaIn, xbOut, xbIn, ybOut, ybIn = single_tube_master(config.inner_coil_width,config.inner_coil_height,config.inner_corner_rad,config.trace_width, 
                                                                            config.trace_spacing, config.turns, config.outer_diam, config.axial_offset_x, 
                                                                            config.axial_offset_y, config.line_segments, config.rectCoilStraightSegsX, config.rectCoilStraightSegsY, config.midPointsSegs, 0, 
                                                                            config.numLayers,config.coil_type,4,config.polyPortLocation,config.polyPortGap)
        xaOut = mil_to_m(xaOut)
        xaIn = mil_to_m(xaIn)
        yaOut = mil_to_m(yaOut)
        yaIn = mil_to_m(yaIn)
        xbOut = mil_to_m(xbOut)
        xbIn = mil_to_m(xbIn)
        ybOut = mil_to_m(ybOut)
        ybIn = mil_to_m(ybIn)
        hexaFOR, hexaREV = segs_to_hexa(xaOut,yaOut,xaIn,yaIn,xbOut,ybOut,xbIn,ybIn,self.fastcapScale)
        if(self.expFileBaseName==''):
            self.expFileBaseName='fastcap'
        #Generate QUI Files for Spirals
        #Get key names that will be used when building files.
        pcbDict = {}
        pcbDict['copper weight (oz)'] = config.copper_weight
        pcbDict['trace width'] = config.trace_width
        pcbDict['material'] = config.pcb_material
        pcbDict['stack-up order'] = config.pcb_construction
        pcbDict['pre-preg layer thickness'] = config.prepreg_thickness
        pcbDict['core layer thickness'] = config.core_thickness
        pcbDict['geometric scaling'] = self.fastcapScale
        TBIOfileDictA = {}
        TBIOfileDictB = {}
        TBIOfileDictA = buildTBIOfileDict(hexaFOR,self.expFileBaseName,'FWD', pcbDict, copperThMeter, self.fastcapScale)
        TBIOfileDictB = buildTBIOfileDict(hexaREV,self.expFileBaseName,'REV', pcbDict, copperThMeter, self.fastcapScale)
        writeTBIOquiFiles(TBIOfileDictA, self.fastcapWriteLoc, self.expFileBaseName, 'FWD')
        writeTBIOquiFiles(TBIOfileDictB, self.fastcapWriteLoc, self.expFileBaseName, 'REV')
        #Generate PCB Dielectric Files
        if(self.includeDielInterface):
            zPCBBuffer = self.extendPCBcopperRatio
            if(config.numLayers % 2 == 0):
                genWritePCBpanels(mil_to_m(float(config.pcb_width)), mil_to_m(float(config.pcb_height)), mil_to_m(config.odd_layer_separation), mil_to_m(config.even_layer_separation), 
                                    copperThMeter, config.numLayers, self.dielGrid_X, self.dielGrid_Y, self.dielGrid_Z, self.fastcapWriteLoc, self.expFileBaseName, "DIEL", pcbDict, self.fastcapScale, hexaFOR, hexaREV, zPCBBuffer)
            else:
                genWritePCBpanels(mil_to_m(float(config.pcb_width)), mil_to_m(float(config.pcb_height)), mil_to_m(config.odd_layer_separation), mil_to_m(config.even_layer_separation), 
                                    copperThMeter, config.numLayers, self.dielGrid_X, self.dielGrid_Y, self.dielGrid_Z, self.fastcapWriteLoc, self.expFileBaseName, "DIEL", pcbDict, self.fastcapScale, hexaFOR, hexaREV, zPCBBuffer)
            pcbThickness = self.fastcapScale * getTotalPCBThickness(mil_to_m(config.odd_layer_separation), mil_to_m(config.even_layer_separation), copperThMeter, config.numLayers)
        #Generate Starter and Ender Panel QUI for Forward Coil
        startEndpanelA = starterEnderPanel(hexaFOR,0,copperThMeter,self.fastcapScale)
        writeStarterEnderPanel(startEndpanelA,self.fastcapWriteLoc, self.expFileBaseName, 'FWD',pcbDict)
        #Generate Starter and Ender Panel QUI for Reverse Coil
        startEndpanelB = starterEnderPanel(hexaREV,0,copperThMeter,self.fastcapScale)
        writeStarterEnderPanel(startEndpanelB,self.fastcapWriteLoc, self.expFileBaseName, 'REV',pcbDict)
        #Generate Main LST File
        #Get PCBDATA
        oddLayerTh = mil_to_m(config.odd_layer_separation)
        evenLayerTh = mil_to_m(config.even_layer_separation)
        lstHeader = quiHeader(self.expFileBaseName,['LST'], 'LST', pcbDict)
        '''Note the below line is an artifact from a previous mod. Should be cleaned out'''
        variablePerm = self.relPermPCB
        try:
            with open(self.fastcapWriteLoc+"/"+self.expFileBaseName+".lst","w") as newFCLST:
                #writeConsole('Exporting LST File...','green')
                writeConsole('Writing File: ' + self.fastcapWriteLoc+"/"+self.expFileBaseName+".lst" , 'green')
                newFCLST.write(lstHeader['LST'])
                zArrayList=generate_z_arrays(1,config.odd_layer_separation,config.even_layer_separation,config.numLayers,config.copper_thickness,config.min_tube_radius,'EXPORT',self.fastcapScale)
                i=0
                outsidePerm = self.relPermPCB
                while i < config.numLayers:
                    #print(str(mil_to_m(zArrayList[i][0])))
                    baseLine_Z = self.fastcapScale * copperThMeter/2 + mil_to_m(zArrayList[i][0])
                    #print(baseLine_Z)
                    newFCLST.write("\n\n* Layer " + str(i) + "\n")
                    if i % 2 == 0:
                        if(i==0):
                            #On first layer, bottom and sides exposed to air!
                            newFCLST.write("C " + self.expFileBaseName + "_bot_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_out_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_in_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_top_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_starter_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_ender_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +"\n")
                        elif(i==config.numLayers-1):
                            #On last layer, top and sides exposed to air of conductor, not FR-4
                            newFCLST.write("C " + self.expFileBaseName + "_bot_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_out_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_in_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_top_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_starter_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_ender_FWD.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +"\n")
                        else:
                            #Conductors. '+' sign ensures all conductor sections will be treated as one.
                            newFCLST.write("C " + self.expFileBaseName + "_top_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_out_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_bot_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_in_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_starter_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_ender_FWD.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +"\n")
                            #D layer filename permout permin xtran ytran ztran xref yref zref. # All D files Build up from Zero. Top and Bot file + and -z
                    #BREAK
                    else:
                        if(i==config.numLayers-1):
                            newFCLST.write("C " + self.expFileBaseName + "_top_REV.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_out_REV.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_bot_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_in_REV.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_starter_REV.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_ender_REV.qui " + variablePerm + " 0.0 0.0 " + str(baseLine_Z) +"\n")
                        else:
                            #Conductors. '+' sign ensures all conductor sections will be treated as one.
                            newFCLST.write("C " + self.expFileBaseName + "_top_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_out_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_bot_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_in_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_starter_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +" +\n")
                            newFCLST.write("C " + self.expFileBaseName + "_ender_REV.qui " + self.relPermPCB + " 0.0 0.0 " + str(baseLine_Z) +"\n")
                    i+=1
                if(self.includeDielInterface):
                    #Add PCB Dielectric Boundary

                    newFCLST.write("\n\n* PCB Dielectric Boundary\n")
                    newFCLST.write("D " + self.expFileBaseName + "_PCB_DIEL.qui 1.0 " + self.relPermPCB + " 0.0 0.0 0.0 0.0 0.0 " + str(pcbThickness/2.0) +" -\n")
                    #newFCLST.write("B " + self.expFileBaseName + "_PCB_DIEL.qui 1.0 " + self.relPermPCB + " 0.0 0.0 0.0 0.0 0.0 " + str(-10.0) +"\n")
                    #D <file> <outperm> <inperm> <xtran> <ytran> <ztram> <xref> <yref> <zref>
                    #Note we never use negative numbers on Z-Axis, so -10 should ALWAYS be safe.
                    
                newFCLST
                newFCLST.close()
        except EnvironmentError:
            writeConsole('Aborting Export: Failed to open ' + self.fastcapWriteLoc+"/"+self.expFileBaseName+".lst" + ' for writing.','red')
            return
        return

'''
Now the AC resistant type and model are a bit redundant. Omit the acResistance Model.
'''
class spice_sub(HasTraits):
    incCapLoss_Bool = Bool(True)
    incCapLoss_tooltipStr = '''Dielectric loss is modelled as a shunt conductance with the parasitic capacitance. 
A dissipation factor or complex permittivity must be supplied to FasterCap for such extraction. If no loss is
selected, it is assumed the dielectric is ideal (dissipation factor = 0 and permittivity is purely real.'''
    freqDepRes_Bool = Bool(False)
    freqDepInd_Bool = Bool(False)
    freqDepRes_tooltipStr = '''Frequency dependent resistance uses two FastHenry2 extractions to approximate AC resistance due
to skin effect and proximity effect. Left unchecked, it is assumed that resistance is constant at all simulation frequencies,
which will be accurate only over a narrow bandwidth'''
    freqDepInd_tooltipStr = '''Frequency dependent inductance uses three FastHenry2 extractions to approximate frequency
dependent inductance and resistance due to skin effect and proximity effect. Left unchecked, it is assumed that resistance
and inductance is constant at all simulation frequencies, which will be accurate only over a narrow bandwidth.'''
    acSweepType = Enum('Decade','Octave','Linear')
    pps = '1000'
    frequencyStart = String('200000')
    frequencyEnd = String('50000000')
    LRFileName_DC = './ifc_henry_DC.out'
    LRFileName = './ifc_henry.out'
    LRFileName_HIGH = './ifc_henry_HIGH.out'
    CGFileName = './ifc_cap.out'
    netListWriteLoc = './'
    simulatorName = Enum('ngSPICE','LTSPICE')
    netListFileName = 'pcb_coil.cir'
    ngSpiceDataFileName = 'pcb_coil.raw'
    addImpPlot_Bool = Bool(False)
    printNetlist_Bool = Bool(False)
    #The following statement is a place holder for a future function
    useParamStatements_Bool = Bool(True)
    #useParamStatements_tooltipStr = '''Place component values into .param SPICE statements, which may improve readability.'''
    LaplaceOnly_LTSPICE_Bool = Bool(False)
    LaplaceOnly_tooltipStr = '''Force all SPICE components (R,L,C) to arbitrary behavioral B-sources with Laplace definitions. Frequency dependent resistance or inductance forces B-elements and Laplace in LTSPICE.'''
    BelementForce_NGSPICE_Bool = Bool(False)
    BelementForce_tooltipStr = '''Force inductance and resistance components to be modeled as B-sources.'''
    rShunt_option = String('1e12')
    rShunt_option_tooltipStr = '''The rshunt option places a shunt resistance from every node to ground. Providing this value helps with convergence issues'''
    #buildSPICENetlist_ngSpice_model2_3
    genNet_Button = Button('Generate Netlist')
    NgspiceView = View(
                Group(
                    Item('incCapLoss_Bool', label='Include Dielectric Loss', tooltip=incCapLoss_tooltipStr),
                    Item('freqDepRes_Bool', label='Include Frequency Dependent Resistance', tooltip=freqDepRes_tooltipStr, visible_when='not freqDepInd_Bool'),
                    Item('freqDepInd_Bool', label='Include Frequency Dependent Inductance', tooltip=freqDepInd_tooltipStr),
                    label = 'Parasitics', show_border = True
                ),
                Group(
                    Item('acSweepType', label='AC Frequency Variation'),
                    Item('pps', label='Number of Points (per Dec, Oct)'),
                    Item('frequencyStart', label='Frequency Lower'),
                    Item('frequencyEnd', label='Frequency Upper'),
                    label = 'Simulation Control', show_border = True
                ),
                Group(
                    Item('LRFileName_DC', editor=FileEditor(entries=10, filter=['Extract files *.out', 'All files *.*']), label='DC Inductance and Resistance Data File', visible_when='freqDepRes_Bool'),
                    Item('LRFileName',editor=FileEditor(entries=10, filter=['Extract files *.out', 'All files *.*']),label='Op Frequency Inductance and Resistance Data File'),
                    Item('LRFileName_HIGH',editor=FileEditor(entries=10, filter=['Extract files *.out', 'All files *.*']),label='HIGH Frequency Inductance and Resistance Data File', visible_when='freqDepInd_Bool'),
                    Item('CGFileName',editor=FileEditor(entries=10, filter=['Extract files *.out', 'All files *.*']),label='Capacitance and Conductance Data File'),
                    label = 'Output File Location', show_border = True
                ),
                Item('simulatorName', label='Simulator for netlist'),
                #Item('useParamStatements_Bool', label='Use .param statements', tooltip=useParamStatements_tooltipStr),
                Item('LaplaceOnly_LTSPICE_Bool', label='All SPICE components as Laplace defined B-sources', tooltip=LaplaceOnly_tooltipStr, visible_when='not freqDepInd_Bool and not freqDepRes_Bool and simulatorName != \'ngSPICE\''),
                Item('BelementForce_NGSPICE_Bool', label='Inductance and Resistance components as B-sources', tooltip=BelementForce_tooltipStr, visible_when='not freqDepInd_Bool and simulatorName != \'LTSPICE\''),
                Item('rShunt_option', label='.option rshunt', tooltip=rShunt_option_tooltipStr, visible_when='simulatorName == \'ngSPICE\''),
                Item('addImpPlot_Bool', label='Add plot command in netlist', tooltip='Add a plot instruction to netlist file so plot automatically generated in simulator.'),
                Item('netListWriteLoc',editor=DirectoryEditor(),label='Netlist write location'),
                Item('netListFileName', label='Netlist File Name'),
                Item('ngSpiceDataFileName', label='ngSPICE Data Output File', visible_when='simulatorName == \'ngSPICE\''),
                Item('printNetlist_Bool', label='Print Netlist in console'),
                Item('genNet_Button', show_label=False)
                )
    @on_trait_change('freqDepInd_Bool,freqDepRes_Bool,simulatorName')
    def freqDepHandler(self):
        #If we are choosing frequency dependent AC inductance, we are auto selecting frequency depending AC resitance.
        if(self.freqDepInd_Bool):
            self.freqDepRes_Bool = True
            if(self.simulatorName == 'LTSPICE'):
                self.LaplaceOnly_LTSPICE_Bool = True
                self.BelementForce_NGSPICE_Bool = False
            else:
                self.LaplaceOnly_LTSPICE_Bool = False
                self.BelementForce_NGSPICE_Bool = True

    @on_trait_change('genNet_Button')
    def genNetButtonHandler(self):
        #First open each of the files and extract the data
        modelType = 0
        if(self.freqDepInd_Bool):
            #We will assume that if Freq dependent AC Inductance is chosen, then we also want Freq dependent resistance.
            modelType = 3
        elif(self.freqDepRes_Bool):
            modelType = 2
        else:
            modelType = 1
        L_dict_DC = {}
        R_dict_DC = {}
        L_dict_HIGH = {}
        R_dict_HIGH = {}
        freqHIGH = None
        try:
            L_dict, R_dict, freqParse = parseIFCfile(self.LRFileName)
            C_dict, G_dict = parseIFCfile(self.CGFileName)
        except TypeError:
            writeConsole('Aborting Netlist Generation.','red')
            return
        #If 2 point Interpolation we need to parse in two fastHenry2 outputs
        if(self.freqDepRes_Bool):
            try:
                L_dict_DC, R_dict_DC, freqDC = parseIFCfile(self.LRFileName_DC)
            except TypeError:
                return
            if(not L_dict_DC or not R_dict_DC):
                writeConsole('Aborting Netlist Generation on inductance and resistance file!','red')
                return
        #If 3 point Interpolation we need to parse in three fastHenry2 outputs
        if(self.freqDepInd_Bool):
            try:
                L_dict_HIGH, R_dict_HIGH, freqHIGH = parseIFCfile(self.LRFileName_HIGH)
            except TypeError:
                return
            if(not L_dict_HIGH or not R_dict_HIGH):
                writeConsole('Aborting Netlist Generation on inductance and resistance file!','red')
                return
        if(self.simulatorName == 'ngSPICE'):
            exportFileName = self.ngSpiceDataFileName
        else:
            #LTSPICE will automatically generate a similar RAW file when -ascii is used.
            exportFileName = ''
        if(self.acSweepType == 'Decade'):
            sweepType = 'dec'
        elif(self.acSweepType == 'Linear'):
            sweepType = 'lin'
        elif(self.acSweepType == 'Octave'):
            sweepType = 'oct'
        writeConsole('Generating Netlist for ' + self.simulatorName,'green')
        #ESRtype_bool = False
        shuntConductance_bool = False
        if(not self.incCapLoss_Bool):
            G_dict = {}
        #ESRsamp_freq = self.dfSampFreq
        spiceNetlist = None
        #We need to ensure the correct AC Resistance model is selected for SPICE
        if(self.LaplaceOnly_LTSPICE_Bool and self.simulatorName == 'LTSPICE'):
            #print("Debug: Laplace Only")
            spiceNetlist = buildSPICENetlist_Laplace_ONLY(config.numLayers, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH, R_dict_HIGH, self.simulatorName,
                        self.incCapLoss_Bool, freqParse, modelType, self.useParamStatements_Bool)
        elif(self.BelementForce_NGSPICE_Bool and self.simulatorName == 'ngSPICE'):
            #print("Debug: ngSPICE 23")
            spiceNetlist = buildSPICENetlist_ngSpice_model2_3(config.numLayers, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH, R_dict_HIGH, freqParse, self.useParamStatements_Bool, modelType)
        else:
            #print("Debug: else")
            spiceNetlist = buildSPICENetlist_Mix(config.numLayers, self.pps, self.frequencyStart, self.frequencyEnd, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH,
                        R_dict_HIGH, self.printNetlist_Bool,self.simulatorName, self.addImpPlot_Bool, exportFileName, sweepType, self.incCapLoss_Bool, freqParse, freqHIGH, modelType, self.useParamStatements_Bool)
        spiceNetlist = appendStimCmd_netlist(spiceNetlist, self.simulatorName, sweepType, self.pps, self.frequencyStart, self.frequencyEnd, exportFileName, True, self.addImpPlot_Bool, self.rShunt_option)
        if(self.printNetlist_Bool):
            writeConsole('Genrated Netlist:\n','green')
            print(spiceNetlist)
        if(spiceNetlist is None):
            return
        #Now attempt to write the file
        fullOutputFileName = os.path.abspath(self.netListWriteLoc + "\\" + self.netListFileName)
        try:
            with open(fullOutputFileName, "w") as netlistFile:
                netlistFile.write(spiceNetlist)
                netlistFile.close()
                writeConsole('Netlist generated OK','green')
        except EnvironmentError:
            writeConsole('Aborting Netlist File Write!','red')
            return

class AppControl_sub(HasTraits):
    fastHenryinFileName = 'fasthenry.inp'
    fastHenryAutoTimeOut_Bool = Bool(False)
    fastHenryAutoTimeOut = Range(1,1000,60)
    fastHenryExportFile_Bool = Bool(False)
    fastHenryExportFileLoc_String = '.'
    fastHenryExportFileName_String = 'applique_FH.out'
    runFastHenry_Button = Button('Run FastHenry2')
    fasterCapinFileName = 'fastcap.lst'
    fasterCapAutoTimeOut_Bool = Bool(False)
    fasterCapAutoTimeOut = Range(1, 1000, 60)
    fasterCapAutoIterError = Range(0.0,1.0,0.01)
    fasterCapScale = Range(1, 100000, 1000)
    fasterCapShowWindow_Bool = Bool(False)
    fasterCapExportFile_Bool = Bool(False)
    fasterCapExportFileLoc_String = '.'
    fasterCapExportFileName_String = 'applique_FC.out'
    runFasterCap_Button = Button('Run FasterCap')
    ngSpiceInputFile = 'pcb_coil.cir'
    ngSpiceBinLoc = './ngspice-30_64/Spice64/bin/ngspice_con.exe'
    ngSpiceRun_Button = Button('Run NgSPICE')
    ltSpiceInputFile = 'pcb_coil.cir'
    ltSpiceBinLoc = 'C:/Program Files/LTC/LTspiceXVII/XVIIx64.exe'
    #ltSpiceBinLoc = 'C:/Program Files (x86)/LTC/LTspiceIV/scad3.exe'
    ltSpiceRun_Button = Button('Run LTSPICE')
    autoModel_Simulator = Enum('ngSPICE','LTSPICE')
    autoModel_Mode = Enum('Single Point','Double Point','Triple Point','Point-by-Point')
    autoMode_tooltip_Str = '''Fastest: Single Point will run FasterCap and FastHenry2 once. Results extrapolated over bandwidth. Accuracy away from sample frequency least accurate.
Faster: Double Point will execute FasterCap once and FastHenry2 twice (at near DC and Op Frequency). Results interpolated and extrapolated over bandwidth. Accuracy over entire bandwidth good.
Fast: Triple Point will execute FasterCap once and FastHenry2 thrice (at DC, Op Frequency, HIGH Frequency). Results interpolated and extrapolated over bandwidth. Accuracy over entire bandwidth excellent.
Slow: Point-by-point will execute FasterCap once and FastHenry2 at every specified frequency point. Potentially the most accurate and slowest if many points are selected.'''
    autoModel_Single_Select = Enum('Start Freq', 'Mid-Range', 'End Freq', 'Custom')
    autoModel_Triple_Select = Enum('End Freq','10xEnd Freq','100xEnd Freq','1000xEnd Freq','Custom')
    autoModel_IncludeDielLoss_Bool = Bool(True)
    autoModelExtrap_dielLossTooltip_Str = '''Frequency Dependant ESR uses the dissipation factor to generate a series connected B source to model ESR.
'Parallel Resistance uses FasterCap conductance values to generate a parallel connected B source to model losses.'''
    autoModel_Custom_Frequency = String('')
    autoModel_High_Frequency = String('')
    autoModelStartFreq = '200000'
    autoModelEndFreq = '50000000'
    autoModelSpicePoints = '1000'
    autoModelPointsPerBW = '10'
    autoModelWritePath = '.'
    autoModelFreqRespOutputFileName = 'autoModel.csv'
    autoModelParamOutputFileName = 'autoModel.prm' #A file containing FastHenry2 and FasterCap extracted data? We need a way to perform Post analysis!
    #This is a place holder for a future function that will allow users to choose if they want FastHenry to increase discretization
    #if a negative impedance is returned. Presently, non-functional
    #autoModelIncFilaments_Bool = Bool(False)
    autoModelIncDiscretization_Bool = Bool(True)
    autoModelDecExtRatio_Bool = Bool(False)
    autoModelDecDelta = Range(1, 100, 1)
    autoModelIncDisretization_delta = Range(1, 1000, 1)
    autoModelIgnoreCapWarnings_Bool = Bool(False)
    autoModelDeleteWorkFiles_Bool = Bool(False)
    autoModelTermOnTimeOut_Bool = Bool(False)
    autoModel_Button = Button('Auto Model Run')
    autoLock_Bool = Bool(False)
    autoModel_Clean_All_Button = Button('Clean all files')
    autoModel_Thread_Term_Button = Button('Terminate AutoModel')
    AutoExtractView = View(
                        Group(
                            Group(
                                Item('_'),
                                Group(
                                    Item('_'),
                                    Item('fastHenryAutoTimeOut_Bool', label='Auto Timeout'),
                                    Item('fastHenryAutoTimeOut', label='Timeout', visible_when='fastHenryAutoTimeOut_Bool'),
                                    label = 'FastHenry2'
                                ),
                                Item('_'),
                                Group(
                                    Item('fasterCapAutoIterError', label='Auto Iteration Relative Error', tooltip = '(-a)option in FasterCap. Relative Error between 0 and 1. Smaller is more accurate.'),
                                    Item('fasterCapAutoTimeOut_Bool', label='Auto Timeout'),
                                    Item('fasterCapAutoTimeOut', label='Timeout', visible_when='fasterCapAutoTimeOut_Bool'),
                                    Item('fasterCapScale', label='FasterCap Geometric Scale Factor'),
                                    Item('fasterCapShowWindow_Bool', label='Show FasterCap Window'),
                                    label = 'FasterCap'
                                ),
                                Item('_'),
                                Group(
                                    Item('ngSpiceBinLoc', editor=FileEditor(), label='ngSPICE Binary Location'),
                                    Item('_'),
                                    Item('ltSpiceBinLoc', editor=FileEditor(), label='LTSPICE Binary Location'),
                                    label = 'SPICE'
                                ) ,
                                label = 'SETTINGS', show_border = True
                                ),
                            Group(
                                Item('_'),
                                Group(
                                    Item('fastHenryinFileName', editor=FileEditor(entries=10, filter=['FastHenry2 files *.inp', 'All files *.*']),label='FastHenry2 Input File'),
                                    Item('fastHenryExportFile_Bool', label='Export FastHenry2 Results(ASCII)'),
                                    Item('fastHenryExportFileLoc_String', editor=DirectoryEditor(),label='Export File Write Location', visible_when='fastHenryExportFile_Bool'),
                                    Item('fastHenryExportFileName_String', label='Export File Name', visible_when='fastHenryExportFile_Bool'),
                                    Item('runFastHenry_Button', show_label=False),
                                    label = 'FastHenry2'
                                ),
                                Item('_'),
                                Group(
                                    Item('fasterCapinFileName', editor=FileEditor(entries=10, filter=['FasterCap files *.lst', 'All files *.*']),label='FasterCap Input File'),
                                    Item('fasterCapExportFile_Bool', label='Export FasterCap Results(ASCII)'),
                                    Item('fasterCapExportFileLoc_String', editor=DirectoryEditor(),label='Export File Write Location', visible_when='fasterCapExportFile_Bool'),
                                    Item('fasterCapExportFileName_String', label='Export File Name', visible_when='fasterCapExportFile_Bool'),
                                    Item('runFasterCap_Button', show_label=False),
                                    label = 'FasterCap'
                                ),
                                Item('_'),
                                Group(
                                    Item('ngSpiceInputFile', editor=FileEditor(entries=10, filter=['SPICE netlist files *.cir', 'All files *.*']),label='ngSPICE Netlist File'),
                                    Item('ngSpiceBinLoc', editor=FileEditor(), label='ngSPICE Binary Location'),
                                    Item('ngSpiceRun_Button', show_label=False),
                                    Item('_'),
                                    Item('ltSpiceInputFile', editor=FileEditor(entries=10, filter=['SPICE netlist files *.cir', 'All files *.*']),label='LTSPICE Netlist File'),
                                    Item('ltSpiceBinLoc', editor=FileEditor(), label='LTSPICE Binary Location'),
                                    Item('ltSpiceRun_Button', show_label=False),
                                    label = 'SPICE'
                                ) ,
                                label = 'MANUAL', show_border = True
                                ),
                                
                            Group(
                                Item('autoModel_Simulator', label='SPICE Simulator'),
                                Item('autoModel_Mode', label='Modelling Mode', tooltip=autoMode_tooltip_Str),
                                Item('autoModel_Single_Select', label='Op Frequency'),
                                Item('autoModel_Custom_Frequency', label='Custom Op Frequency', visible_when='autoModel_Single_Select==\'Custom\''),
                                Item('autoModel_Triple_Select', label='High Frequency', tooltip='Select a frequency high enough to ensure inductor is minimized through skin effect', visible_when='autoModel_Mode==\'Triple Point\''),
                                Item('autoModel_High_Frequency', label='Custom High Frequency', visible_when='autoModel_Triple_Select==\'Custom\''),
                                Item('autoModel_IncludeDielLoss_Bool', label='Include dielectric loss'),
                                Item('autoModelStartFreq', label='Frequency Lower'),
                                Item('autoModelEndFreq', label='Frequency Upper'),
                                Item('autoModelSpicePoints', label='Spice Number of Sample Points', visible_when='autoModel_Mode==\'Single Point\' or autoModel_Mode==\'Double Point\' or autoModel_Mode==\'Triple Point\''),
                                Item('autoModelPointsPerBW', label='Number of model points', visible_when='autoModel_Mode==\'Point-by-Point\''),
                                Item('autoModelWritePath',editor=DirectoryEditor(),label='Auto model write location'),
                                Item('autoModelFreqRespOutputFileName',label='Auto model frequency response filename'),
                                Item('autoModelParamOutputFileName',label='Auto model parameter extraction filename'),
                                #This is a place holder for a future function that will allow users to choose if they want FastHenry to increase discretization
                                #if a negative impedance is returned. Presently, non-functional
                                #Item('autoModelIncFilaments_Bool', label='Automatically Increment FastHenry2 Filaments'),
                                Item('autoModelIncDiscretization_Bool', label='Automatically Increment FasterCap Discretization', tooltip='If FasterCap fails, automatically increase midpoints.'),
                                Item('autoModelIncDisretization_delta', label='Amount to increase discretization by', visible_when='autoModelIncDiscretization_Bool'),
                                Item('autoModelDecExtRatio_Bool', label='Decrement Dielectric Extension',tooltip='Decrement dielectric extension ratio on successful FasterCap run. Increased accuracy, increase time.'),
                                Item('autoModelDecDelta', label='Dielectric Extension Decrement Amount', visible_when='autoModelDecExtRatio_Bool'),
                                Item('autoModelDeleteWorkFiles_Bool', label='Delete work files on completion'),
                                Item('autoModelIgnoreCapWarnings_Bool', label='Ignore FasterCap Warnings', tooltip='Ignore FasterCap warnings. Warning: Results may not be accurate.'),
                                Item('autoModelTermOnTimeOut_Bool', label='Terminate on Time Out'),
                                Item('autoModel_Button', show_label=False, visible_when = 'not autoLock_Bool'),
                                Item('autoModel_Thread_Term_Button', show_label=False, visible_when = 'autoLock_Bool'),
                                Item('_'),
                                Item('autoModel_Clean_All_Button', show_label=False),
                                label = 'AUTOMATIC', show_border = True
                                ), layout='tabbed'
                            )
                        )
    ind_Dict_Last = {}
    res_Dict_Last = {}
    cap_Dict_Last = {}
    cond_Dict_Last = {}
    FH_Code_Last = 0
    FC_Code_Last = 0
    FC_DER_GOOD = False
    #FIX ERROR CODE
    #NEW FC
    #0 = OK
    #-1 = Pos off-diag
    #-2 neg diag
    #-3 = non-diag dom  
    #-11 = timeout
    #-12 = ret zero
    #NEW FH
    #0 = OK
    #-1 = neg ind
    #-2 = neg res
    #-3 = neg ind and neg res
    #-11 = timeout
    #-12 ret zero

    @on_trait_change('autoModel_Clean_All_Button')
    def autoModelCleanAllButtonHandler(self):
        workDir = os.path.abspath(self.autoModelWritePath + '\\temp')
        cleanDirectory(workDir, True)
    @on_trait_change('runFastHenry_Button')
    def runFastHenryButton_Handler(self):
        writeConsole('Executing FastHenry2 for ' + self.fastHenryinFileName,'green')
        config.threadActiveApp_ID = 1
        parseCode, ind_Dict, res_Dict, freqFH = runFastHenry2(self.fastHenryinFileName,True,self.fastHenryAutoTimeOut_Bool, self.fastHenryAutoTimeOut)
        config.threadActiveApp_ID = -1
        extAppCodeReport(1, parseCode)
        self.FH_Code_Last = parseCode
        if(parseCode < -10):
            self.ind_Dict_Last = {}
            self.res_Dict_Last = {}
            return
        writeConsole('FastHenry2 extraction complete at ' + str(freqFH) + ' Hz','green')
        writeConsole('Extracted Inductance Values:','green')
        printResultsConsole(ind_Dict)
        writeConsole('Extracted Resistance Values:','green')
        printResultsConsole(res_Dict)
        self.ind_Dict_Last = ind_Dict
        self.res_Dict_Last = res_Dict
        if(self.fastHenryExportFile_Bool):
            expFileName = os.path.abspath(self.fastHenryExportFileLoc_String) + "\\" + self.fastHenryExportFileName_String
            writeDictsToFile(ind_Dict,res_Dict, freqFH, expFileName)

    @on_trait_change('runFasterCap_Button')
    def runFasterCapButton_Handler(self):
        writeConsole('Executing FasterCap for ' + self.fasterCapinFileName,'green')
        config.threadActiveApp_ID = 0
        parseCode, cap_Dict, cond_Dict = runFasterCap(self.fasterCapinFileName, self.fasterCapAutoIterError, self.fasterCapScale, self.fasterCapShowWindow_Bool, True, self.fasterCapAutoTimeOut_Bool, self.fasterCapAutoTimeOut)
        config.threadActiveApp_ID = -1
        #Check parseCode
        #fcCodeReportConsole(parseCode)
        extAppCodeReport(0, parseCode)
        self.FC_Code_Last = parseCode
        #printResults
        if(parseCode < -10):
            self.cap_Dict_Last = {}
            self.cond_Dict_Last = {}
            return
        writeConsole('Extracted Capacitance Values:','green')
        printResultsConsole(cap_Dict)
        writeConsole('Extracted Conductance Values:','green')
        printResultsConsole(cond_Dict)
        self.cap_Dict_Last = cap_Dict
        self.cond_Dict_Last = cond_Dict
        #writeToFile on User Request.
        if(self.fasterCapExportFile_Bool):
            expFileName = os.path.abspath(self.fasterCapExportFileLoc_String) + "\\" + self.fasterCapExportFileName_String
            #print(expFileName)
            writeDictsToFile(cap_Dict,cond_Dict,-1, expFileName)
            #Write results to file named same as input file, with new extension:pcd

    @on_trait_change('ngSpiceRun_Button')
    def ngSpiceRunButton_Handler(self):
        if(fileExists(self.ngSpiceInputFile)):
            command = [os.path.abspath(self.ngSpiceBinLoc),self.ngSpiceInputFile]
            writeConsole('Executing ngSpice on ' + self.ngSpiceInputFile,'green')
            config.threadActiveApp_ID = 2
            ngSpiceProcess = subprocess.Popen(command, shell=False, creationflags=DETACHED_PROCESS)
            config.threadActiveApp_ID = -1
            retCode = consoleActiveAnimation(ngSpiceProcess)
            if(retCode != 0):
                writeConsole('Error: ngSPICE exited with error code ' + str(retCode), 'red')
            else:
                writeConsole('ngSPICE completed OK','green')
        else:
            writeConsole('Error: Cannot open ' + self.ngSpiceInputFile, 'red')
    @on_trait_change('ltSpiceRun_Button')
    def ltSpiceRunButton_Handler(self):
        if(fileExists(self.ltSpiceInputFile)):
            command = [os.path.abspath(self.ltSpiceBinLoc),'-b','-ascii',self.ltSpiceInputFile]
            writeConsole('Executing LTSPICE on ' + self.ltSpiceInputFile,'green')
            try:
                config.threadActiveApp_ID = 3
                ltSpiceProcess = subprocess.Popen(command, shell=False, creationflags=DETACHED_PROCESS)
                config.threadActiveApp_ID = -1
                retCode = consoleActiveAnimation(ltSpiceProcess)
                if(retCode != 0):
                    writeConsole('Error: LTSPICE exited with error code ' + str(retCode), 'red')
                else:
                    writeConsole('LTSPICE completed OK','green')
            except FileNotFoundError:
                writeConsole('Could not find LTSPICE binary file.','yellow')
        else:
            writeConsole('Error: Cannot open ' + self.ngSpiceInputFile, 'red')
            
        
        
class PostAnalysis_sub(HasTraits):
    autoResultsInFile1 = './autoModel.csv'
    autoResultsInFile2 = './autoModel.csv'
    autoResultsInFilePRM = './autoModel.csv'
    autoParamInFile = './autoModel.prm'
    compareResults_Bool = Bool(False)
    loadZinData_Button = Button('Load ZIN CSV File(s)')
    postAnalysisReportWriteLocation = '.'
    postAnalysisReportOutFileName = 'postAnalysisReport.txt'
    postAnalysisComplete_Bool = False
    postAnalysisSPICEComplete_Bool = False
    spiceNetlistString = String('')
    genPostAnalysisReport_Button =Button('Generate Report')
    zinRawData1 = []
    zinRawData2 = []
    data1Loaded = False
    data2Loaded = False
    csvLoaded = False
    prmLoaded = False
    quickPlot_Button = Button('Plot Impedance')
    extractParams_Button = Button('Extract Coil Parameters')
    spiceSelect= Enum('ngSPICE', 'LTSPICE')
    acSweepType = Enum('Decade','Octave','Linear')
    pps = '1000'
    frequencyStart = String('200000')
    frequencyEnd = String('50000000')
    spiceFileName = String('RLCG')
    spiceWritePath = '.'
    genZinCSVExtract_Button = Button('Simulate')
    opFrequency = String('Not Calculated')
    selfResFreq = String('Not Calculated')
    lumpInductance = String('Not Calculated')
    lumpResistance = String('Not Calculated')
    lumpCapacitance = String('Not Calculated')
    lumpConductance = String('Not Calculated')
    apparentImpedance = String('Not Calculated')
    apparentInductance = String('Not Calculated')
    apparentResistance = String('Not Calculated')
    qualityFactor = String('Not Calculated')
    ParamsView = View(
                    Group(
                        Item('autoResultsInFile1', editor=FileEditor(entries=10, filter=['SSL AutoModel Files *.csv', 'All files *.*']), label='Zin CSV File #1', tooltip = 'Import any CSV File with Compatible Format'),
                        Item('compareResults_Bool', label='Compare Two ZIN Files'),
                        Item('loadZinData_Button', show_label=False),
                        Item('autoResultsInFile2', editor=FileEditor(entries=10, filter=['SSL AutoModel Files *.csv', 'All files *.*']), label='Zin CSV File #2', tooltip = 'Import any CSV File with Compatible Format', visible_when = 'compareResults_Bool'),
                        Item('quickPlot_Button', show_label=False),
                        label = 'Full Bandwidth Analysis', show_border = True
                        ),
                    Group(
                        Group(
                            Item('autoResultsInFilePRM', editor=FileEditor(entries=10, filter=['SSL AutoModel Files *.csv', 'All files *.*']), label='Zin CSV File', tooltip = 'Import any CSV File with Compatible Format'),
                            Item('autoParamInFile', editor=FileEditor(entries=10, filter=['PRM AutoModel Files *.prm', 'All files *.*']), label='PRM File', tooltip = 'Import PRM file generated by autoModel'),
                            Item('extractParams_Button', show_label=False),
                        ),
                        Group(
                            Item('opFrequency', label='Operation Frequency', style='readonly'),
                            Item('_'),
                            Item('selfResFreq', label='Self Resonant Frequency', style = 'readonly'),
                            Item('lumpInductance', label='Inductance(H)', style = 'readonly'),
                            Item('lumpResistance', label='Resistance(Ω)', style = 'readonly'),
                            Item('lumpCapacitance', label='Capacitance(F)', style = 'readonly'),
                            Item('lumpConductance', label='Conductance(S)', style = 'readonly'),
                            Item('_'),
                            Item('apparentImpedance', label='Apparent Z (Ω)', style = 'readonly'),
                            Item('apparentResistance', label='Apparent R (Ω)', style = 'readonly'),
                            Item('apparentInductance', label='Apparent L (H)', style = 'readonly'),
                            Item('qualityFactor', label='Quality Factor', style = 'readonly'),
                            label = 'Extracted Data', show_border = True
                            ),
                        Group(
                            Item('spiceSelect', label='Simulator'),
                            Item('acSweepType', label='AC Frequency Variation'),
                            Item('pps', label='Number of Points (per Dec, Oct)'),
                            Item('frequencyStart', label='Frequency Lower'),
                            Item('frequencyEnd', label='Frequency Upper'),
                            Item('spiceFileName', label='SPICE, CSV Base File Name', tooltip = 'CIR, RAW and CSV files will be written to same location with same main file name'),
                            Item('spiceWritePath', editor=DirectoryEditor(),label='Export File Write Location'),
                            Item('genZinCSVExtract_Button', show_label=False),
                            label = 'SPICE', show_border = True
                            ),
                        Group(
                            Item('postAnalysisReportWriteLocation', editor=DirectoryEditor(), label='Report File Write Location'),
                            Item('postAnalysisReportOutFileName', label='Post Analysis Report Output Filename'),
                            Item('genPostAnalysisReport_Button', show_label=False),
                            label = 'Report', show_border = True
                            ),
                        label = 'Extract Parameters'
                        ),
                 )
                 
    @on_trait_change('loadZinData_Button')
    def loadZinDataButtonHandler(self):
        #Check if csv file 1 exists
        #3 columns needed:
        freqList, realList, imagList = importCSV(self.autoResultsInFile1)
        if(freqList != None or realList != None or imagList != None):
            self.data1Loaded = True
            self.zinRawData1 = [freqList, realList, imagList]
        if(self.compareResults_Bool):
            freqList, realList, imagList = importCSV(self.autoResultsInFile2)
            if(freqList != None or realList != None or imagList != None):
                self.data2Loaded = True
                self.zinRawData2 = [freqList, realList, imagList]

    @on_trait_change('quickPlot_Button')
    def plotImpButtonHandler(self):
        if(self.data1Loaded and not self.compareResults_Bool):
            freq1Array = np.asarray(self.zinRawData1[0])
            real1Array = np.asarray(self.zinRawData1[1])
            imag1Array = np.asarray(self.zinRawData1[2])
            genImpPlot(freq1Array, real1Array, freq1Array, imag1Array, 'Frequency(Hz)', 'Zin(Ohms)', 'Frequency(Hz)', 'Zin(Ohms)', 'Real', 'Imag', 'Zin')
            writeConsole('Plotting Data 1','green')
        elif(self.data1Loaded and self.data2Loaded and self.compareResults_Bool):
            freq1Array = np.asarray(self.zinRawData1[0])
            real1Array = np.asarray(self.zinRawData1[1])
            imag1Array = np.asarray(self.zinRawData1[2])
            freq2Array = np.asarray(self.zinRawData2[0])
            real2Array = np.asarray(self.zinRawData2[1])
            imag2Array = np.asarray(self.zinRawData2[2])
            genCompareImpPlot(freq1Array, real1Array, freq1Array, imag1Array, freq2Array, real2Array,  freq2Array, imag2Array, 
            'Zin #1', 'Zin #2', 'Frequency(Hz)', 'Zin(Ohms)', 'Frequency(Hz)', 'Zin(Ohms)', 'Real', 'Imag', 'Zin') 
        else:
            writeConsole('Error: Zin Data not loaded yet','red')

        
    @on_trait_change('extractParams_Button')
    def extractParamsButton_Handler(self):
        #Load both CSV and PRM files.
        #For self res frequency, CSV must be present.
        point_by_point_bool = False
        try:
            opPointFreq, indDict, resDict, capDict, condDict = importPRM(self.autoParamInFile)
        except TypeError:
            return
        #print(indDict)
        #For point by point there is no op point.
        if(opPointFreq == -1):
            self.opFrequency = 'None'
            point_by_point_bool = True
        else:
            self.opFrequency = opPointFreq
        indLump, resLump = extractLumpIndRes(indDict, resDict)
        self.lumpInductance = indLump
        self.lumpResistance = resLump
        #Go through CSV File
        freqList, realList, imagList = importCSV(self.autoResultsInFilePRM)
        zinRawData = None
        if(freqList != None or realList != None or imagList != None):
            zinRawData = [freqList, realList, imagList]
        else:
            writeConsole('Could not load Zin data.','yellow')
            return
        #Find the SRF if it exists
        lastReactValue = 0
        srfFreq1 = -1
        #NOTE WE SHOULD INTERPOLATE HERE! 
        #What if there is a ZERO value!
        #Calculate MAXZ/MINY concurrently
        realZMAX = -1
        realZMAX_0 = -1
        realZMAX_1 = -1
        for index, reactValue in enumerate(zinRawData[2]):
            #Highly unlikely
            if(reactValue == 0):
                self.selfResFreq = zinRawData[0][index]
                realZMAX = zinRawData[1][index]
            #More Likely
            if (reactValue < 0 and lastReactValue > 0):
                #We need to interpolate
                Zimag_0 = reactValue
                Zimag_1 = lastReactValue
                freq_0 = zinRawData[0][index - 1]
                freq_1 = zinRawData[0][index]
                realZMAX_0 = zinRawData[1][index - 1]
                realZMAX_1 = zinRawData[1][index]
                Zimag = 0
                srfFreq1 = freq_0 + (Zimag - Zimag_0)*(freq_1-freq_0)/(Zimag_1-Zimag_0)
                realZMAX = realZMAX_0 + (srfFreq1 - freq_0)*(realZMAX_1-realZMAX_0)/(freq_1-freq_0)
                #Zreal_0 + (freq - freq_0)*(Zreal_1-Zreal_0)/(freq_1-freq_0)
                #srfFreq1 = (zinRawData[0][index] + zinRawData[0][index-1])/2
                self.selfResFreq = srfFreq1
                break
            else:
                lastReactValue = reactValue
        #Find real and imag impedance at closest frequency
        lastFrequency = '-1'
        opFreqRealZ = None
        opFreqImagZ = None
        if(not point_by_point_bool):
            for index, frequency in enumerate(zinRawData[0]):
                #Highly unlikely
                if(float(opPointFreq) == float(frequency)):
                    opFreqRealZ = zinRawData[1][index]
                    opFreqImagZ = zinRawData[2][index]
                #More likely
                elif(float(opPointFreq) > float(lastFrequency) and float(opPointFreq) < float(frequency)):
                    #We should interpolate
                    freq_0 = float(lastFrequency)
                    freq_1 = float(frequency)
                    freq = float(opPointFreq)
                    Zreal_0 = float(zinRawData[1][index - 1])
                    Zimag_0 = float(zinRawData[2][index - 1])
                    Zreal_1 = float(zinRawData[1][index])
                    Zimag_1 = float(zinRawData[2][index])
                    opFreqRealZ = Zreal_0 + (freq - freq_0)*(Zreal_1-Zreal_0)/(freq_1-freq_0)
                    opFreqImagZ = Zimag_0 + (freq - freq_0)*(Zimag_1-Zimag_0)/(freq_1-freq_0)
                    break
                else:
                    lastFrequency = frequency
        if(opFreqRealZ is not None and opFreqImagZ is not None):
            self.qualityFactor = str(opFreqImagZ/opFreqRealZ)
            self.apparentImpedance = str(opFreqRealZ) + "+" + str(opFreqImagZ) + "j"
            self.apparentResistance = str(opFreqRealZ);
            self.apparentInductance = str(opFreqImagZ/(float(opPointFreq)*2*np.pi))
        #PLACE APPARENT HERE
        if(srfFreq1 < 0):
            writeConsole('Could not identify self resonant frequency.','yellow')
            return
        else:
            #Now Calculate Lump Cap
            #For this we calculate from the SRF knowing that C=1/((2*pi*f)^2*L)
            capLump = 1/((2*np.pi*srfFreq1)**2*float(indLump))
            self.lumpCapacitance = capLump
            #Next Calculate Conductance
            #We need to solve the equation for a R and L in series in parallel with a G and C.
            #Then plug in the values and solve for G at some frequency?
            #At resonance, Real Z is max and Real Y is min.
            #G = Y - (1/((R+s*L)+s*C))
            condLump = 1/realZMAX - resLump/(resLump**2+indLump**2*(2*np.pi*srfFreq1)**2)
            self.lumpConductance = condLump
        self.postAnalysisComplete_Bool = True

    @on_trait_change('genPostAnalysisReport_Button')
    def genPostAnalysisReportButton_Handler(self):
        if(self.postAnalysisComplete_Bool):
            expFileName = os.path.abspath(self.postAnalysisReportWriteLocation) + "\\" + self.postAnalysisReportOutFileName
            try:
                with open(expFileName,'w') as paReportFile:
                    paReportFile.write("*Post Analysis Report\n")
                    paReportFile.write("Zin Source File = " + self.autoParamInFile + "\n")
                    paReportFile.write("PRM Source File = " + self.autoResultsInFilePRM + "\n")
                    paReportFile.write("\nSelf Resonant Frequency = " + self.selfResFreq + "\n")
                    paReportFile.write("Operation Frequency(fop) = " + self.opFrequency + "\n")
                    paReportFile.write("\nLumped Component Values @ fop:\n")
                    paReportFile.write("Inductance(H) = " + self.lumpInductance + "\n")
                    paReportFile.write("Resistance(ohm) = " + self.lumpResistance + "\n")
                    paReportFile.write("Capacitance(F) = " + self.lumpCapacitance + "\n")
                    paReportFile.write("Conductance(S) = " + self.lumpConductance + "\n")
                    paReportFile.write("\nQuality Factor = " + self.qualityFactor + "\n")
                    if(self.postAnalysisSPICEComplete_Bool):
                        paReportFile.write("\nSPICE Netlist:\n")
                        paReportFile.write(self.spiceNetlistString)
                    paReportFile.close()
                    writeConsole('Report written successfullly to ' + expFileName,'green')
            except EnvironmentError:
                writeConsole('Could not generate ' + expFileName,'red')
        
def importCSV(fileName):
    freqList = []
    realList = []
    imagList = []
    lineOne = True
    try:
        with open(fileName, "r") as csvFile:
            for line in csvFile:
                if(not lineOne):
                    lineList = (line.strip()).split(',')
                    freqList.append(float(lineList[0]))
                    realList.append(float(lineList[1]))
                    imagList.append(float(lineList[2]))
                else:
                    lineOne = False
            csvFile.close()
            writeConsole(fileName + ' imported OK' ,'green')
            return freqList, realList, imagList
    except FileNotFoundError:
        writeConsole('Could not open ' + fileName + ' for reading' ,'red')
        return None, None, None

def importPRM(fileName):
    opPointFreq = ''
    indDict = {}
    indDict_Low = {}
    indDict_High = {}
    resDict = {}
    resDict_Low = {}
    resDict_High = {}
    capDict = {}
    condDict = {}
    resultsDict = {}
    lastFHfrequency = -1
    thisFHfrequency = -1
    opPointFreq_Float = -1
    capCondRange_Bool = False
    indResRange_Bool = False
    interpFHValues_Bool = False
    try:
        with open(fileName, "r") as prmFile:
            for line in prmFile:
                #Check for Keywords
                if(line.split(" ")[0] == "OP_FREQUENCY"):
                    opPointFreq = line.split(" ")[1][:-1]
                    opPointFreq_Float = float(opPointFreq)
                elif(line == "*FasterCap Results:\n"):
                    capCondRange_Bool = True
                elif(line.split(" ")[0] == "FH_FREQUENCY"):
                    thisFHfrequency = float(line.split(" ")[1][:-1])
                    #print(thisFHfrequency)
                    if(thisFHfrequency == opPointFreq_Float):
                        writeConsole('Exact frequency found in PRM file.' ,'green')
                        indResRange_Bool = True
                    elif(thisFHfrequency < opPointFreq_Float):
                        lastFHfrequency = thisFHfrequency
                    elif(thisFHfrequency > opPointFreq_Float):
                        interpFHValues_Bool = True
                        break
                if(capCondRange_Bool):
                    if(line[0] == "C"):
                        capDict[line.split(" ")[0]] = line.split(" ")[1][:-1]
                    elif(line[0] == "G"):
                        condDict[line.split(" ")[0]] = line.split(" ")[1][:-1]
                    elif(line == "\n"):
                        capCondRange_Bool = False
                if(indResRange_Bool):
                    if(line[0] == "L"):
                        indDict[line.split(" ")[0]] = line.split(" ")[1][:-1]
                    elif(line[0] == "R"):
                        resDict[line.split(" ")[0]] = line.split(" ")[1][:-1]
                    elif(line == "\n"):
                        indResRange_Bool = False
            if(interpFHValues_Bool):
                writeConsole('Exact frequency not found in PRM file. Interpolating results.' ,'green')
                #We need to interpolate between lastFHFrequency and thisFHFrequency.
                #Re-read the file contents and get the L and R matrices at lower and higher frequency
                prmFile.seek(0)
                lowIndResRange_Bool = False
                highIndResRange_Bool = False
                for line in prmFile:
                    #Find line for lastFHfrequency
                    if(line.split(" ")[0] == "FH_FREQUENCY"):
                        currentFHfrequency = float(line.split(" ")[1][:-1])
                        if(currentFHfrequency == lastFHfrequency):
                            lowIndResRange_Bool = True
                        elif(currentFHfrequency == thisFHfrequency):
                            highIndResRange_Bool = True
                    if(lowIndResRange_Bool):
                        if(line[0] == "L"):
                            indDict_Low[line.split(" ")[0]] = line.split(" ")[1][:-1]
                        elif(line[0] == "R"):
                            resDict_Low[line.split(" ")[0]] = line.split(" ")[1][:-1]
                        elif(line == "\n"):
                            lowIndResRange_Bool = False
                    if(highIndResRange_Bool):
                        if(line[0] == "L"):
                            indDict_High[line.split(" ")[0]] = line.split(" ")[1][:-1]
                        elif(line[0] == "R"):
                            resDict_High[line.split(" ")[0]] = line.split(" ")[1][:-1]
                        elif(line == "\n"):
                            highIndResRange_Bool = False
                            break
            prmFile.close()
            if(interpFHValues_Bool):
                lowFreq = lastFHfrequency
                highFreq = thisFHfrequency
                for key, value in indDict_Low.items():
                    indDict[key] = interpolate(opPointFreq_Float, lowFreq, highFreq, float(value), indDict_High[key])
                for key, value in resDict_Low.items():
                    resDict[key] = interpolate(opPointFreq_Float, lowFreq, highFreq, float(value), resDict_High[key])
            writeConsole(fileName + ' imported OK' ,'green')
    except FileNotFoundError:
        writeConsole('Could not open ' + fileName + ' for reading' ,'red')
        return None
    return opPointFreq, indDict, resDict, capDict, condDict

def extractLumpIndRes(indDict, resDict):
    #Add Lii to lump
    #If i!=j Add 2*Lij to lump
    #Same for Resistance
    indLump = 0
    resLump = 0
    numLayers = int((np.sqrt(8*len(indDict)+1)-1)/2)
    for i in range(numLayers):
        for j in range(i, numLayers):
            tempInd = float(indDict["L" + str(i+1) + str(j+1)])
            tempRes = float(resDict["R" + str(i+1) + str(j+1)])
            if(i==j):
                indLump += tempInd
                resLump += tempRes
            else:
                indLump += 2.0*tempInd
                resLump += 2.0*tempRes
    return indLump, resLump

def genImpPlot(xR, yR, xI, yI, unitsXstrR, unitsYstrR, unitsXstrI, unitsYstrI, titlestrR, titlestrI, titlestrMain):
    plt.ion()
    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.subplots_adjust(hspace=1.0)
    ax1.plot(xR, yR)
    ax1.set(xlabel=unitsXstrR, ylabel=unitsYstrR,title=titlestrR)
    ax1.grid(True)
    ax2.plot(xI, yI)
    ax2.set(xlabel=unitsXstrI, ylabel=unitsYstrI,title=titlestrI)
    ax2.grid(True)
    fig.suptitle(titlestrMain)
    plt.ioff()
    
def genCompareImpPlot(x1R, y1R, x1I, y1I, x2R, y2R, x2I, y2I, key1str, key2str, unitsXstrR, unitsYstrR, unitsXstrI, unitsYstrI, titlestrR, titlestrI, titlestrMain):    
    plt.ion()
    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.scatter(x1R, y1R, c='b', marker='o', label=key1str)
    ax1.scatter(x2R, y2R, c='r', marker='*', label=key2str)
    ax1.set(xlabel=unitsXstrR, ylabel=unitsYstrR,title= titlestrR)
    ax1.grid(True)
    ax2.scatter(x1I, y1I, c='b', marker='o', label=key1str)
    ax2.scatter(x2I, y2I, c='r', marker='*', label=key2str)
    ax2.set(xlabel=unitsXstrI, ylabel=unitsYstrI, title=titlestrI)
    ax2.grid(True)
    plt.legend(loc='upper left');
    fig.suptitle(titlestrMain)
    plt.ioff()

def simpleSpiceNetlistGen(spiceModule, simulatorName, sweepType, pps, freqStart, freqEnd, ngSpice_rshunt_default_Bool, plotImp_Bool, paramDict, fileName_CIR):
    netList = '*RLCG Equivelant Circuit\n'
    netList += '\n'
    netList += '*Components\n'
    netList += 'R1 1 2 {Rlump}\n'
    netList += 'L1 2 0 {Llump}\n'
    netList += 'C1 1 0 {Clump}\n'
    netList += 'RG1 1 0 {1/Glump}\n'
    netList += '\n*Params\n'
    netList += '.param Rlump='+str(paramDict['Rlump'])+'\n'
    netList += '.param Llump='+str(paramDict['Llump'])+'\n'
    netList += '.param Clump='+str(paramDict['Clump'])+'\n'
    netList += '.param Glump='+str(paramDict['Glump'])+'\n'
    exportFileName = fileName_CIR.replace('cir','raw')
    ngSpice_rshunt_default_Bool = True
    plotImp_Bool = False
    rshunt = '1e12'
    netListOut = appendStimCmd_netlist(netList, simulatorName, sweepType, pps, freqStart, freqEnd, exportFileName, ngSpice_rshunt_default_Bool, plotImp_Bool, rshunt)
    spiceModule.spiceNetlistString = netListOut
    try:
        with open(fileName_CIR, 'w') as simpleNetFile:
            simpleNetFile.write(netListOut)
            simpleNetFile.close()
            return 0
    except EnvironmentError:
        writeConsole('Error writing ' + fileName_CIR,'red')
        return -1

def run_ngSPICE(self,fileName):
    if(fileExists(fileName)):
        command = [os.path.abspath(self.ngSpiceBinLoc),fileName]
        writeConsole('Executing ngSpice on ' + fileName,'green')
        ngSpiceProcess = subprocess.Popen(command, shell=False, creationflags=DETACHED_PROCESS)
        retCode = consoleActiveAnimation(ngSpiceProcess)
        if(retCode != 0):
            writeConsole('Error: ngSPICE exited with error code ' + str(retCode), 'red')
        else:
            writeConsole('ngSPICE completed OK','green')
    else:
        writeConsole('Error: Cannot open ' + fileName, 'red')

def run_LTSPICE(self,fileName):
    if(fileExists(fileName)):
        command = [os.path.abspath(self.ltSpiceBinLoc),'-b','-ascii',fileName]
        writeConsole('Executing LTSPICE on ' + fileName,'green')
        try:
            ltSpiceProcess = subprocess.Popen(command, shell=False, creationflags=DETACHED_PROCESS)
            retCode = consoleActiveAnimation(ltSpiceProcess)
            if(retCode != 0):
                writeConsole('Error: LTSPICE exited with error code ' + str(retCode), 'red')
            else:
                writeConsole('LTSPICE completed OK','green')
        except FileNotFoundError:
            writeConsole('Could not find LTSPICE binary file.','yellow')
    else:
        writeConsole('Error: Cannot open ' + fileName, 'red')

def gen_CSV_from_SPICE(fileName, ZIN_DICT):
    csvFileFullName = os.path.abspath(fileName)
    try:
        with open(csvFileFullName, "w") as csvFile:
            csvFile.write('frequency,zin(real),zin(imag)\n')
            if(ZIN_DICT == None):
                writeConsole('Parsing of SPICE data failed.' ,'red')
                return
            for freqKey, zValues in ZIN_DICT.items():
                csvFile.write(str(freqKey) + ',' + str(zValues[0]) + ',' + str(zValues[1]) + '\n')
            csvFile.close()
    except EnvironmentError:
        writeConsole('Could not open ' + csvFileFullName + ' for writing' ,'red')
    writeConsole('Extracted Data Written to ' + csvFileFullName ,'green')

class File_sub(HasTraits):
    #In dev a default settings MGR
    dev_Statement_String = String('This tab is under construction')
    export_Settings_Button = Button('Export Settings')
    import_Settings_Button = Button('Import Settings')

    fileTabView = View(
                        Item('dev_Statement_String', style='readonly', show_label=False),
                        Item('export_Settings_Button', show_label=False),
                        Item('import_Settings_Button', show_label=False)
                        )

'''
*************************************
* SSL PCB COIL DESIGNER MAIN MODULE *
*************************************
'''
class SSL_designer_Tab(HasTraits):
    FastHenry2_tab = Instance(FastHenry2_sub, ())
    FasterCap_tab = Instance(FasterCap_sub, ())
    spice_tab = Instance(spice_sub, ())
    AppControl_tab = Instance(AppControl_sub, ())
    PostAnalysis_tab = Instance(PostAnalysis_sub, ())
    File_tab = Instance(File_sub, ())
    fullModelSimulationTabView = View(
                                     Group(
                                         Item('FastHenry2_tab', style='custom', label='FastHenry2', show_label=False),
                                         Item('FasterCap_tab', style='custom', label='FasterCap', show_label=False),
                                         Item('spice_tab', style='custom', label='SPICE', show_label=False),
                                         Item('AppControl_tab', style='custom', label='Application Control', show_label=False),
                                         Item('PostAnalysis_tab', style='custom', label='Post Analysis', show_label=False),
                                         Item('File_tab', style='custom', label='File', show_label=False),
                                         layout='tabbed'
                                     ),
                                 )
    recursiveBreak = False
    firstRun = True
    #startMidPointSegs = None
    settingsDict = {}
    def storeInitialSetting(self, store_Bool):
        if(store_Bool):
            #SAVE THE CURRENT SETTINGS
            self.settingsDict['midPointsSegs'] = config.midPointsSegs
            self.settingsDict['rectCoilStraightSegsX']= config.rectCoilStraightSegsX
            self.settingsDict['rectCoilStraightSegsY'] = config.rectCoilStraightSegsY
            self.settingsDict['extendPCBcopperRatio'] = self.FasterCap_tab.extendPCBcopperRatio
        else:
            config.midPointsSegs = self.settingsDict['midPointsSegs']
            config.rectCoilStraightSegsX = self.settingsDict['rectCoilStraightSegsX']
            config.rectCoilStraightSegsY = self.settingsDict['rectCoilStraightSegsY']
            self.FasterCap_tab.extendPCBcopperRatio = self.settingsDict['extendPCBcopperRatio']

    @on_trait_change('PostAnalysis_tab.genZinCSVExtract_Button')
    def genZinCSVExtractButton_Handler(self):
        #Generate SPICE netlist
        paramDict = {'Rlump':self.PostAnalysis_tab.lumpResistance, 'Llump':self.PostAnalysis_tab.lumpInductance,'Clump':self.PostAnalysis_tab.lumpCapacitance,'Glump':self.PostAnalysis_tab.lumpConductance}
        expFileName = os.path.abspath(self.PostAnalysis_tab.spiceWritePath) + "\\" + self.PostAnalysis_tab.spiceFileName + ".cir"
        if(self.PostAnalysis_tab.acSweepType == 'Decade'):
            sweepType = 'dec'
        elif(self.PostAnalysis_tab.acSweepType == 'Linear'):
            sweepType = 'lin'
        elif(self.PostAnalysis_tab.acSweepType == 'Octave'):
            sweepType = 'oct'
        #Need to clean this up. Why send object and then children of object? Just send the object
        simpleSpiceNetlistGen(self.PostAnalysis_tab, self.PostAnalysis_tab.spiceSelect, sweepType, self.PostAnalysis_tab.pps, self.PostAnalysis_tab.frequencyStart, self.PostAnalysis_tab.frequencyEnd, True, False, paramDict, expFileName)
        #Simulate
        if(self.PostAnalysis_tab.spiceSelect == 'LTSPICE'):
            run_LTSPICE(self.AppControl_tab,expFileName)
        elif(self.PostAnalysis_tab.spiceSelect == 'ngSPICE'):
            run_ngSPICE(self.AppControl_tab,expFileName)
        ZIN_DICT = parseMultiPointACngSPICE(expFileName.replace('cir','raw'))
        gen_CSV_from_SPICE(expFileName.replace('cir','csv'), ZIN_DICT)
        self.PostAnalysis_tab.postAnalysisSPICEComplete_Bool = True
            
    @on_trait_change('AppControl_tab.autoModel_Button')
    def recursionBlock(self):
        self.recursiveBreak = False
    #Auto App Control must be executed at parent tab, as it needs access to all child tabs.
    
    #New autoModelFunc will start Thead, block button, and add a thread kill button
    
    @on_trait_change('AppControl_tab.autoModel_Button')
    def autoModelButton_handler(self):
        if(not self.AppControl_tab.autoLock_Bool):
            self.AppControl_tab.autoLock_Bool = True
            autoThread = threading.Thread(target=autoModel_Thread, args=(1, self))
            autoThread.start()
        else:
            print("Error: Thread Active")
    
    @on_trait_change('AppControl_tab.autoModel_Thread_Term_Button')
    def autoModelThreadTermButton_handler(self):
        config.killAutoThread_bool = True
        terminateApplication(config.threadActiveApp_ID)
        



def autoModel_Thread(tid, self):
    writeConsole('AutoModel: Thread Version','green')
    writeConsole('AutoModel: Beginning automatic PCB Inductor model generation...','green')
    #***
    #First: Setup for correct mode
    #-1 is not selected
    #1. 1-point: All standard components. L, R, C. Mutual resistance uses a B element.
    ##          self.spice_tab.freqDepRes_Bool=False
    ##          self.spice_tab.freqDepInd_Bool=False
    #2. 2-Point: Frequency dependent resistance. Self resistance and mutual resistance use B-elements. L, C standard.
    ##          self.spice_tab.freqDepRes_Bool=True
    ##          self.spice_tab.freqDepInd_Bool=False
    #3. 3-Point: Frequency dependent resistance and inductance. 3-point netlists will only be used with B-element only.
    ##          self.spice_tab.freqDepRes_Bool=True
    ##          self.spice_tab.freqDepInd_Bool=True
    #3. n-Point: All standard components. L, R, C. Mutual resistance uses a B element.
    ##          self.spice_tab.freqDepRes_Bool=False
    ##          self.spice_tab.freqDepInd_Bool=False
    #***
    if(self.firstRun):
        #self.startMidPointSegs = config.midPointsSegs
        self.storeInitialSetting(True)
        self.firstRun = False
    COMP_LOG_FILE_LIST = {}
    COMP_LOG_FILE_LIST["SPICE"] = self.AppControl_tab.autoModel_Simulator
    autoModel_SPICE_Mode = -1
    if(self.AppControl_tab.autoModel_Mode == 'Triple Point'):
        autoModel_SPICE_Mode = 3
        self.spice_tab.freqDepRes_Bool=True
        self.spice_tab.freqDepInd_Bool=True
    elif(self.AppControl_tab.autoModel_Mode == 'Double Point'):
        autoModel_SPICE_Mode = 2
        self.spice_tab.freqDepRes_Bool=True
        self.spice_tab.freqDepInd_Bool=False
    elif(self.AppControl_tab.autoModel_Mode == 'Single Point'):
        autoModel_SPICE_Mode = 1
        self.spice_tab.freqDepRes_Bool=False
        self.spice_tab.freqDepInd_Bool=False
        self.AppControl_tab.autoModelPointsPerBW = 1
    elif(self.AppControl_tab.autoModel_Mode == 'Point-by-Point'):
        autoModel_SPICE_Mode = 99
        self.spice_tab.freqDepRes_Bool=False
        self.spice_tab.freqDepInd_Bool=False
    else:
        #We should never get here
        autoModel_SPICE_Mode = -1
    #Check if Dielectric loss specified
    if(self.AppControl_tab.autoModel_IncludeDielLoss_Bool):
        self.spice_tab.incCapLoss_Bool = True
    else:
        self.spice_tab.incCapLoss_Bool = False
    config.abort_flag = False
    modeShort = -1
    opPointFREQ = -1
    triplePointHighFreq = -1
    #Configure user-selected frequency points
    if(autoModel_SPICE_Mode >= 1):
        #Select the correct operating point frequency
        if(self.AppControl_tab.autoModel_Single_Select == 'Start Freq'):
            opPointFREQ = float(self.AppControl_tab.autoModelStartFreq)
        elif(self.AppControl_tab.autoModel_Single_Select ==  'Mid-Range'):
            opPointFREQ = (float(self.AppControl_tab.autoModelEndFreq) + float(self.AppControl_tab.autoModelStartFreq))/2.0
        elif(self.AppControl_tab.autoModel_Single_Select ==  'End Freq'):
            opPointFREQ = float(self.AppControl_tab.autoModelEndFreq)
        elif(self.AppControl_tab.autoModel_Single_Select ==  'Custom'):
            opPointFREQ = float(self.AppControl_tab.autoModel_Custom_Frequency)
        writeConsole('AutoModel: Configuring Sample Op Frequency at ' + str(opPointFREQ),'green')
        #Set High Frequency points for Triple Point
        if(autoModel_SPICE_Mode >= 3):
            if(self.AppControl_tab.autoModel_Triple_Select == 'End Freq'):
                triplePointHighFreq = float(self.AppControl_tab.autoModelEndFreq)
            elif(self.AppControl_tab.autoModel_Triple_Select ==  '10xEnd Freq'):
                triplePointHighFreq = float(self.AppControl_tab.autoModelEndFreq) * 10.0
            elif(self.AppControl_tab.autoModel_Triple_Select ==  '100xEnd Freq'):
                triplePointHighFreq = float(self.AppControl_tab.autoModelEndFreq) * 100.0
            elif(self.AppControl_tab.autoModel_Triple_Select ==  '1000xEnd Freq'):
                triplePointHighFreq = float(self.AppControl_tab.autoModelEndFreq) * 1000.0
            elif(self.AppControl_tab.autoModel_Single_Select ==  'Custom'):
                triplePointHighFreq = float(self.AppControl_tab.autoModel_High_Frequency)
            writeConsole('AutoModel: Configuring Sample HIGH Frequency at ' + str(triplePointHighFreq),'green')
    #Total number of processes equals 'fileCreateProc' + FC + len(freqs) * FH + len(freqs) * SPICE
    #1+1+len(freq)*2
    if(autoModel_SPICE_Mode == 99):
        totalProcessCount = 1 + int(self.AppControl_tab.autoModelPointsPerBW) *2
    else:
        totalProcessCount = autoModel_SPICE_Mode + 2
    processComp = 0
    progStr = 'AutoModel Progress'
    #Create a temp directory
    try:
        # Create Temp Dir
        writeConsole('AutoModel: Creating Temp Directory','green')
        os.mkdir('temp')
    except FileExistsError:
        writeConsole('AutoModel: Temp Directory Exists','green')
    #***
    #Second: Generate FasterCap Files and Run
    #***
    FC_IFC_FILE = './temp/auto_fastcap.out'
    FC_IFC_LAST_FILE = './temp/auto_fastcap_LAST.out'
    self.FasterCap_tab.fastcapWriteLoc = './temp'
    self.FasterCap_tab.expFileBaseName = 'auto_fastcap'
    self.FasterCap_tab.expFasterCapHandler()
    fcFullPathFile = os.path.abspath(self.FasterCap_tab.fastcapWriteLoc + '/' + self.FasterCap_tab.expFileBaseName + '.lst')
    self.AppControl_tab.fasterCapinFileName = fcFullPathFile
    self.AppControl_tab.fasterCapExportFile_Bool = True
    self.AppControl_tab.fasterCapExportFileLoc_String = './temp'
    self.AppControl_tab.fasterCapExportFileName_String = 'auto_fastcap.out'
    self.AppControl_tab.fasterCapScale = self.FasterCap_tab.fastcapScale
    self.AppControl_tab.runFasterCapButton_Handler()
    if(config.killAutoThread_bool):
        terminateThread(self)
        return
    #If output file has warnings or errors, try again
    print("DEBUG: FC CODE:" +str(self.AppControl_tab.FC_Code_Last))
    if (self.AppControl_tab.FC_Code_Last < 0 and self.AppControl_tab.autoModelIncDiscretization_Bool and not self.AppControl_tab.autoModelIgnoreCapWarnings_Bool and not self.AppControl_tab.autoModelTermOnTimeOut_Bool):
        '''!!!MODIFY THIS.!!!!'''
        if self.AppControl_tab.FC_DER_GOOD:
            writeConsole('AutoModel: Last Dielectric extension ratio failed.','green')
            self.AppControl_tab.FC_DER_GOOD = False
            copyfile(FC_IFC_LAST_FILE, FC_IFC_FILE)
        else:
            writeConsole('AutoModel: FasterCap failed. Incrementing discretization','yellow')
            #Here we increment midpoints. For circular there will be only one. For RR there will be three.
            #config.midPointsSegs, config.rectCoilStraightSegsX config.rectCoilStraightSegsY
            #config.coil_type = 'Circular' or config.coil_type == 'Rounded Rectangle'. Not sure what to do for SVG
            if(config.coil_type == 'Circular' or config.coil_type == 'Rounded Rectangle'):
                config.midPointsSegs += self.AppControl_tab.autoModelIncDisretization_delta
                print("Midpoints discretization increased to " + str(config.midPointsSegs) +"\n")
                if(config.coil_type == 'Rounded Rectangle'):
                    config.rectCoilStraightSegsX += self.AppControl_tab.autoModelIncDisretization_delta
                    config.rectCoilStraightSegsY += self.AppControl_tab.autoModelIncDisretization_delta
                    print("Straight Section X axis midpoints increased to " + str(config.rectCoilStraightSegsX) +"\n")
                    print("Straight Section Y axis midpoints increased to " + str(config.rectCoilStraightSegsY) +"\n")
            else:
                return
            #print("Current discretization = " + str(config.line_segments) +"\n")
            config.line_segments += self.AppControl_tab.autoModelIncDisretization_delta
            self.AppControl_tab.FC_Code_Last = 0 #This should reset automatically. But just in case.
            self.autoModelButton_handler()
    elif (self.AppControl_tab.FC_Code_Last < 0 and not self.AppControl_tab.autoModelIncDiscretization_Bool and not self.AppControl_tab.autoModelIgnoreCapWarnings_Bool and not self.AppControl_tab.autoModelTermOnTimeOut_Bool):
        writeConsole('AutoModel: Failed on Capacitance Extraction','red')
        if self.AppControl_tab.FC_DER_GOOD:
            writeConsole('AutoModel: Last Dielectric extension ratio failed.','green')
            self.AppControl_tab.FC_DER_GOOD = False
            copyfile(FC_IFC_LAST_FILE, FC_IFC_FILE)
        return
    #If auto dec the ext ration
    elif (self.AppControl_tab.autoModelDecExtRatio_Bool):
        #We should store the last successful results so we don't have to run FasterCap again when it fails!
        #COPY the FC_IFC_FILE to auto_fastcap_BEST.out
        if(self.FasterCap_tab.extendPCBcopperRatio - self.AppControl_tab.autoModelDecDelta >= 1.1):
            self.AppControl_tab.FC_DER_GOOD = True
            copyfile(FC_IFC_FILE, FC_IFC_LAST_FILE)
            writeConsole('AutoModel: Automatically reducing dielectric extension ratio to increase accuracy','green')
            self.FasterCap_tab.extendPCBcopperRatio -= self.AppControl_tab.autoModelDecDelta
            writeConsole('AutoModel: New ratio = ' + str(self.FasterCap_tab.extendPCBcopperRatio),'green')
            self.AppControl_tab.FC_Code_Last = 0 #This should reset automatically. But just in case.
            self.autoModelButton_handler()
        elif self.AppControl_tab.FC_DER_GOOD:
            writeConsole('AutoModel: Dielectric extension ratio at minimum. Cannot decrement further.','green')
            self.AppControl_tab.FC_DER_GOOD = False
            copyfile(FC_IFC_LAST_FILE, FC_IFC_FILE)
    elif(self.AppControl_tab.FC_Code_Last == -1 and self.AppControl_tab.autoModelTermOnTimeOut_Bool):
        writeConsole('AutoModel: Terminating on FasterCap TimeOut.','yellow')
        return
    #Avoid excess runs due to auto increment
    if(self.recursiveBreak):
        return
    #FasterCap is done so add file name to COMP_LOG_FILE_LIST
    COMP_LOG_FILE_LIST["FC"] = FC_IFC_FILE
    processComp += 1
    progressWrite(progStr, processComp, totalProcessCount)
    #***
    #Third: Generate FastHenry2 Files and Run
    ##If autoModel_SPICE_Mode==1, this will run once at opPointFREQ
    ##If autoModel_SPICE_Mode==2, this will run at DC and opPointFREQ
    ##If autoModel_SPICE_Mode==3, run at DC, opPointFREQ and triplePointHighFreq
    ##If autoModel_SPICE_Mode==99, this will loop through all frequencies
    #***
    self.FastHenry2_tab.sweepFreq_Bool = False
    ##It also requires n-coil mode
    self.FastHenry2_tab.typeExp = 'n-Port: Layered Coil'
    self.FastHenry2_tab.fasthenryWriteLoc = './temp'
    ##Loop through freq points
    freqLower = self.AppControl_tab.autoModelStartFreq
    freqUpper = self.AppControl_tab.autoModelEndFreq
    freqPoints = self.AppControl_tab.autoModelPointsPerBW
    highFreqRun_Bool = False
    if(is_number(freqUpper) and is_number(freqLower) and is_number(freqPoints)):
        freqLower = float(freqLower)
        freqUpper = float(freqUpper)
        freqPoints = float(freqPoints)
    else:
        writeConsole('Error: Frequency specifications must be numeric','red')
    if(autoModel_SPICE_Mode == 99):
        deltaFreq = (freqUpper - freqLower)/(freqPoints-1)
        fIndex = freqLower
        if(freqPoints<2):
            writeConsole('Error: There must be more than 1 frequency point.','red')
            #config.midPointsSegs = self.startMidPointSegs
            self.storeInitialSetting(False)
            return
    elif(autoModel_SPICE_Mode == 1):
        deltaFreq = 1
        fIndex = opPointFREQ
        freqUpper = opPointFREQ
    elif(autoModel_SPICE_Mode >= 2):
        #Note we can't use 0Hz for FastHenry. Choose something arbitrarily small.
        deltaFreq = opPointFREQ - 1
        fIndex = 1.0
        freqUpper = opPointFREQ
    if(autoModel_SPICE_Mode == 3):
        highFreqRun_Bool = True
    ##The following lists will be synchroinzed for processing
    LR_IFC_FILE_LIST = []
    FREQ_LIST = []
    self.AppControl_tab.fastHenryExportFile_Bool = True
    self.AppControl_tab.fastHenryExportFileLoc_String = './temp'
    while (fIndex <= freqUpper or highFreqRun_Bool):
        ##We will create one FastHenry inp file for each frequency
        if(autoModel_SPICE_Mode == 3 and fIndex > freqUpper):
            self.FastHenry2_tab.singleFreqHz = str(triplePointHighFreq)
            self.FastHenry2_tab.expFileName = 'fasthenry_' + str(triplePointHighFreq).replace('.','_') + 'Hz.inp'
            highFreqRun_Bool = False
        else:
            self.FastHenry2_tab.singleFreqHz = str(fIndex)
            self.FastHenry2_tab.expFileName = 'fasthenry_' + str(fIndex).replace('.','_') + 'Hz.inp'
        self.FastHenry2_tab.export_fast_henry()
        ##Now run fastHenry for this file
        ###Fast Henry input file name
        ###Fast Henry Prefix
        fhFullPathFile = os.path.abspath(self.FastHenry2_tab.fasthenryWriteLoc + '/' + self.FastHenry2_tab.expFileName)
        self.AppControl_tab.fastHenryinFileName = fhFullPathFile
        outFilePrefix = 'auto_hen_' + str(fIndex).replace('.','_')
        #self.AppControl_tab.fastHenryoutFilePrefix = outFilePrefix
        if(autoModel_SPICE_Mode == 3 and fIndex > freqUpper):
            self.AppControl_tab.fastHenryExportFileName_String = 'auto_hen_' + str(triplePointHighFreq).replace('.','_') + '.out'
        else:
            self.AppControl_tab.fastHenryExportFileName_String = 'auto_hen_' + str(fIndex).replace('.','_') + '.out'
        self.AppControl_tab.runFastHenryButton_Handler()
        if(config.killAutoThread_bool):
            terminateThread(self)
            return
        LR_IFC_FILE_LIST.append(self.AppControl_tab.fastHenryExportFileName_String)
        #FastHenry run complete, append to list
        tempFH_freq = ''
        if(autoModel_SPICE_Mode == 3 and fIndex > freqUpper):
            tempFH_freq = str(triplePointHighFreq)
        else:
            tempFH_freq = str(fIndex)
        COMP_LOG_FILE_LIST["FH_"+tempFH_freq] = './temp/' + self.AppControl_tab.fastHenryExportFileName_String
        FREQ_LIST.append(fIndex)
        fIndex += deltaFreq
        processComp += 1
        progressWrite(progStr, processComp, totalProcessCount)
    #***
    #Fourth: Generate SPICE netlists  for each FastHenry2 run
    ##If autoModel_SPICE_Mode==1, 2 or 3: One SPICE netlist file will be created, with a full bandwidth.
    ##If autoModel_SPICE_Mode==99: A SPICE netlist will be generated for each of the FastHenry runs with a 1Hz bandwidth.
    #***
    NG_FILE_LIST = []
    self.spice_tab.acSweepType = 'Linear'
    #If this is a Point-by-Point, then the Bandwidth of each simulation will be 1 Hz.
    if(autoModel_SPICE_Mode == 99):
        self.spice_tab.pps = '1'
    else:
        self.spice_tab.pps = self.AppControl_tab.autoModelSpicePoints
    self.spice_tab.CGFileName = FC_IFC_FILE
    self.spice_tab.netListWriteLoc = './temp/'
    self.spice_tab.simulatorName=self.AppControl_tab.autoModel_Simulator
    if(self.AppControl_tab.autoModel_Simulator == 'LTSPICE' and autoModel_SPICE_Mode >= 2):
        self.spice_tab.LaplaceOnly_LTSPICE_Bool = True
        self.spice_tab.BelementForce_NGSPICE_Bool = False
    elif(self.AppControl_tab.autoModel_Simulator == 'ngSPICE' and autoModel_SPICE_Mode == 3):
        self.spice_tab.LaplaceOnly_LTSPICE_Bool = False
        self.spice_tab.BelementForce_NGSPICE_Bool = True
    self.spice_tab.addImpPlot_Bool = False
    self.spice_tab.printNetlist_Bool = False
    ZIN_DICT = {}
    tempCompLog_Freq = -1
    for index, fileName in enumerate(LR_IFC_FILE_LIST):
        if(autoModel_SPICE_Mode == 99):
            self.spice_tab.frequencyStart = str(FREQ_LIST[index])
            #sets the DF to the correct value at the freq of interest.
            self.spice_tab.dfSampFreq = str(FREQ_LIST[index])
            #This last data point is not included
            self.spice_tab.frequencyEnd = str(FREQ_LIST[index] + 1)
        else:
            self.spice_tab.frequencyStart = str(self.AppControl_tab.autoModelStartFreq)
            self.spice_tab.frequencyEnd = str(self.AppControl_tab.autoModelEndFreq)
        #If single point mode or point-by-point, we only need the single FastHenry result file for each frequency
        if(autoModel_SPICE_Mode == 1 or autoModel_SPICE_Mode == 99):
            self.spice_tab.LRFileName = './temp/' + LR_IFC_FILE_LIST[index]
            self.spice_tab.netListFileName = 'auto_'+ str(FREQ_LIST[index]).replace('.','_') + '_pcb_coil.cir'
            self.spice_tab.ngSpiceDataFileName = self.spice_tab.netListWriteLoc + 'auto_' + str(FREQ_LIST[index]).replace('.','_') + '_pcb_coil.raw'
            tempCompLog_Freq = FREQ_LIST[index]
        #For two-point we have two FastHenry files to use.
        elif(autoModel_SPICE_Mode == 2):
            self.spice_tab.LRFileName_DC = './temp/' + LR_IFC_FILE_LIST[index]
            self.spice_tab.LRFileName = './temp/' + LR_IFC_FILE_LIST[index + 1]
            #self.spice_tab.netListFileName = 'auto_'+ str(FREQ_LIST[index + 1]).replace('.','_') + '_pcb_coil.cir'
        #For three-point we have three FastHenry files to use.
        elif(autoModel_SPICE_Mode == 3):
            self.spice_tab.LRFileName_DC = './temp/' + LR_IFC_FILE_LIST[index]
            self.spice_tab.LRFileName = './temp/' + LR_IFC_FILE_LIST[index + 1]
            self.spice_tab.LRFileName_HIGH = './temp/' + LR_IFC_FILE_LIST[index + 2]
        if(autoModel_SPICE_Mode >= 2 and autoModel_SPICE_Mode != 99):
            tempCompLog_Freq = str(FREQ_LIST[index + 1])
            self.spice_tab.ngSpiceDataFileName = self.spice_tab.netListWriteLoc + 'auto_' + str(FREQ_LIST[index+1]).replace('.','_') + '_pcb_coil.raw'
            self.spice_tab.netListFileName = 'auto_'+ str(FREQ_LIST[index + 1]).replace('.','_') + '_pcb_coil.cir'
        self.spice_tab.genNetButtonHandler()
        if(config.abort_flag):
            #We have a bad netlist. About automodel
            writeConsole('Error: Aborting automodel due to bad netlist.' ,'red')
            #config.midPointsSegs = self.startMidPointSegs
            self.storeInitialSetting(False)
            return
        #***
        #Fifth: Run SPICE netlists.
        ##If autoModel_SPICE_Mode==1, 2 or 3: Single SPICE netlist will be simulated.
        ##If autoModel_SPICE_Mode==99: Each SPICE netlist will be simulated.
        #***
        if(self.spice_tab.simulatorName == 'ngSPICE'):
            self.AppControl_tab.ngSpiceInputFile = self.spice_tab.netListWriteLoc + self.spice_tab.netListFileName
            #COMP_LOG_FILE_LIST["SPICE_NETLIST_"+str(tempCompLog_Freq)] = ["NGSPICE",self.spice_tab.netListWriteLoc + self.spice_tab.netListFileName]
            self.AppControl_tab.ngSpiceRunButton_Handler()
        else:
            self.AppControl_tab.ltSpiceInputFile = self.spice_tab.netListWriteLoc + self.spice_tab.netListFileName
            #COMP_LOG_FILE_LIST["SPICE_NETLIST_"+str(tempCompLog_Freq)] = ["LTSPICE",self.spice_tab.netListWriteLoc + self.spice_tab.netListFileName]
            self.AppControl_tab.ltSpiceRunButton_Handler()
        if(config.killAutoThread_bool):
            terminateThread(self)
            return
        COMP_LOG_FILE_LIST["SPICE_NETLIST_"+str(tempCompLog_Freq)] = self.spice_tab.netListWriteLoc + self.spice_tab.netListFileName
        #***
        #Sixth: Extract simulation data from RAW files and compile.
        ##If autoModel_SPICE_Mode==1, 2 or 3: Single SPICE netlist will be simulated.
        ##If autoModel_SPICE_Mode==99: Each SPICE netlist will be simulated.
        #***
        ##Extract Data from each run and place in to csv doc.
        ##For ngSPICE, rawFile is named self.spice_tab.ngSpiceDataFileName
        ##For LTSPICE, rawFile is named self.spice_tab.netListFileName replace .cir with .raw.
        #rawFileName = ''
        
        #if(self.spice_tab.simulatorName == 'ngSPICE'):
         #   rawFileName = self.spice_tab.ngSpiceDataFileName
        #else:
         #   rawFileName = self.spice_tab.ngSpiceDataFileName
            #rawFileName = self.AppControl_tab.ltSpiceInputFile.replace('.cir','.raw')
        #print(rawFileName)
        rawFileName = self.spice_tab.ngSpiceDataFileName
        COMP_LOG_FILE_LIST["SPICE_OUTPUT_"+str(tempCompLog_Freq)] = self.spice_tab.netListWriteLoc + rawFileName
        if(autoModel_SPICE_Mode == 99):
            #frequency, zinReal, zinImag = parseSinglePointACngSPICE(rawFileName)
            #There will be only one item returned in this case
            ZIN_DICT_temp = parseMultiPointACngSPICE(rawFileName)
            #print(ZIN_DICT_temp)
            key_temp = list(ZIN_DICT_temp.keys())[0]
            ZIN_DICT[key_temp] = ZIN_DICT_temp[key_temp]
            #print(ZIN_DICT)
            #ZIN_DICT[frequency] = [zinReal, zinImag]
            #print(frequency)
            #print(zinReal)
            if(key_temp < 0):
                writeConsole('Error: Raw file data not extracted correctly...' ,'red')
        else:
            #This needs to be parsed differently
            ZIN_DICT = parseMultiPointACngSPICE(rawFileName)
        processComp += 1
        progressWrite(progStr, processComp, totalProcessCount)
        if(autoModel_SPICE_Mode < 99):
            #We need to force a break here so SPICE doesn't run again
            break
    csvFileFullName = os.path.abspath(self.AppControl_tab.autoModelWritePath + '/' + self.AppControl_tab.autoModelFreqRespOutputFileName)
    try:
        with open(csvFileFullName, "w") as csvFile:
            csvFile.write('frequency,zin(real),zin(imag)\n')
            if(ZIN_DICT == None):
                writeConsole('Parsing of SPICE data failed.' ,'red')
                config.midPointsSegs = self.startMidPointSegs
                self.storeInitialSetting(False)
                return
            for freqKey, zValues in ZIN_DICT.items():
                csvFile.write(str(freqKey) + ',' + str(zValues[0]) + ',' + str(zValues[1]) + '\n')
            csvFile.close()
    except EnvironmentError:
        writeConsole('Could not open ' + csvFileFullName + ' for writing' ,'red')
    writeConsole('Extracted Data Written to ' + csvFileFullName ,'green')
    COMP_LOG_FILE_LIST["CSV"] = csvFileFullName
    #***
    #Seventh: Process, and generated Comp Log File of Parameters
    #***
    #print(COMP_LOG_FILE_LIST);
    #Open up each file in dictionary and Extract results. Write to the COMP_LOG_FILE
    #Format of File 
    
    #Create 
    paramsFileFullName = os.path.abspath(self.AppControl_tab.autoModelWritePath + '/' + self.AppControl_tab.autoModelParamOutputFileName)
    try:
        with open(paramsFileFullName, "w") as paramFile:
            paramFile.write("*Auto-Model Comprehensive Extracted Data\n\n")
            paramFile.write("OP_FREQUENCY " + str(opPointFREQ) + "\n\n") 
            paramFile.write("*FasterCap Results:\n")
            try:
                with open(COMP_LOG_FILE_LIST["FC"], "r") as fcFile:
                    for line in fcFile:
                        paramFile.write(line)
                    fcFile.close() 
            except EnvironmentError:
                writeConsole('Could not open ' + COMP_LOG_FILE_LIST["FC"],'red')
            paramFile.write("\n\n")
            paramFile.write("*FastHenry2 Results:\n")
            #Cycle through keys and get FH_* keys
            for key, value in COMP_LOG_FILE_LIST.items():
                if(key[:2] == 'FH'):
                    frequencyPoint = key.split("_")[1]
                    paramFile.write("FH_FREQUENCY "+ frequencyPoint + "\n")
                    try:
                        with open(value, "r") as fhFile:
                            for line in fhFile:
                                if(line.split(" ")[0] != "FREQ"):
                                    paramFile.write(line)
                            fhFile.close()
                    except EnvironmentError:
                         writeConsole('Could not open ' + value,'red')
                    paramFile.write("\n")
            paramFile.write("\n\n")
            paramFile.write("*SPICE Netlists\n")
            paramFile.write("TARGET " + COMP_LOG_FILE_LIST["SPICE"] + "\n")
            for key, value in COMP_LOG_FILE_LIST.items():
                if(key[:13] == 'SPICE_NETLIST'):
                    frequencyPoint = key.split("_")[2]
                    paramFile.write("SPICE_FREQUENCY " + frequencyPoint + "\n")
                    paramFile.write("\n*------------ Begin Netlist ------------\n")
                    try:
                        with open(value, "r") as spiceFile:
                            for line in spiceFile:
                                paramFile.write(line)
                            spiceFile.close()
                    except EnvironmentError:
                         writeConsole('Could not open ' + value,'red')
                    paramFile.write("\n*------------ End Netlist ------------\n\n")
            paramFile.close()
            #csvFile.write('frequency,zin(real),zin(imag)\n')
            #if(ZIN_DICT == None):
             #   writeConsole('Parsing of SPICE data failed.' ,'red')
              #  return
            #for freqKey, zValues in ZIN_DICT.items():
             #   csvFile.write(str(freqKey) + ',' + str(zValues[0]) + ',' + str(zValues[1]) + '\n')
            #csvFile.close()
    except EnvironmentError:
        writeConsole('Could not open ' + paramsFileFullName + ' for writing' ,'red')
    #for key, value in COMP_LOG_FILE_LIST.items():
     #   print(str(key) + "=" + str(value))
    #***
    #Eigth: Clean up.
    #***
    if(self.AppControl_tab.autoModelDeleteWorkFiles_Bool):
        workDir = os.path.abspath(self.AppControl_tab.autoModelWritePath + '\\temp')
        cleanDirectory(workDir, True)
    self.recursiveBreak = True
    #config.midPointsSegs = self.startMidPointSegs
    self.storeInitialSetting(False)
    self.AppControl_tab.autoLock_Bool = False
    return
    
def terminateThread(self):
    if(self.AppControl_tab.autoModelDeleteWorkFiles_Bool):
        workDir = os.path.abspath(self.AppControl_tab.autoModelWritePath + '\\temp')
        cleanDirectory(workDir, True)
    self.recursiveBreak = True
    self.storeInitialSetting(False)
    self.AppControl_tab.autoLock_Bool = False
    config.killAutoThread_bool = False
'''
*******************
* SPICE Functions *
*******************
Spice netlist generation is dependent on model_Type and generation function.
The Mix function generates the SPICE netlist using more intuitive R,L,C where possible.


model_Type
1. Point-by-point or Single-Point: All standard components. L, R, C. Mutual resistance uses a B element. (LTSPICE and ngSpice)
2. 2-Point: Frequency dependent resistance. Self resistance and mutual resistance use B-elements. L, C standard. (ngSpice only)
3. 3-Point: Frequency dependent resistance and inductance. 3-point netlists will only be used with B-element only. (ngSpice only)

Note: LTSPICE requires Laplace statements for frequency dependent components. For simplicity, when LTSPICE is selected and model_Type
is 2 or 3, the Laplace only statements will be used. We could use Laplace for just R and L and G, and leave C as a standard component, but
making everything a Laplace statement simplifies the process and may be better for future work if we need frequency dependent capacitance.
'''
def buildSPICENetlist_Mix(numLayers, pps, freqStart, freqEnd, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH, R_dict_HIGH,
                    postPrint, simulatorName, plotImp, exportFileName, sweepType, incCapLoss_Bool, RAC_freq, LAC_HIGH_freq, model_Type, useParamStatements_Bool):
    #For now we will ignore the "Use Params" statements. In the future we may provide the option
    #for where the param statements are generated in the netlist file.
    #if(not useParamStatements_Bool):
    #    print("In future param statements will be suppressed.")
    #######################
    #Build Header Comments#
    #######################
    netList = '*Generic ' + str(numLayers) + '-Layer PCB SPICE Netlist\n'
    netList += '*Created by Appliqué:' + genDateStamp() + ' \n'
    netList += '\n*This netlist was designed use in ' + simulatorName
    #############################
    #Build Inductance Components#
    #############################
    netList += '\n*Self Inductance\n' 
    if(simulatorName == 'LTSPICE' or model_Type <=2):
        netList += '*<Lii> <node 1> <node 2> <Inductance>\n'
        if(numLayers > 1):
            for i in range(numLayers):
                if i == 0:
                    netList += 'L11 L11R11 1 {L11p}\n'
                else:
                    netList += 'L'  + str(i+1) + str(i+1) +  ' L' + str(i+1) + str(i+1) + 'R' + str(i+1) + str(i+1)+ ' ' + str(i+1) + ' {L' + str(i+1) + str(i+1) + 'p}\n'
        else:
            netList += 'L11 L11R11 1 {L11p}\n'
    elif(model_Type == 3):
        netList += '*<Lii> <node 1> <node 2> <L=\'f(hertz)\'\n'
        netList +='IN PROGRESS\n'
        #Lij_temp = '{Lhigh' + str(i+1) + str(j+1) + 'p}+{Lint' + str(i+1) + str(j+1) + 'p}/sqrt(hertz)'
        if(numLayers > 1):
            for i in range(numLayers):
                if i == 0:
                    netList += 'L11 L11R11 1 L=\'{Lhigh11p}+{Lint11p}/sqrt(hertz)\'\n'
                else:
                    netList += 'L'  + str(i+1) + str(i+1) +  ' L' + str(i+1) + str(i+1) + 'R' + str(i+1) + str(i+1)+ ' ' + str(i+1) + ' L=\'{Lhigh'+ str(i+1) + str(i+1) + 'p}+{Lint' + str(i+1) + str(i+1) + 'p}/sqrt(hertz)\'\n'
        else:
            netList += 'L11 L11R11 1 {L11p}\n'

    #############################
    #Build Resistance Components#
    #############################
    netList += '\n*Parasitic Inductor Series Resistance\n'
    if(simulatorName == 'LTSPICE' or model_Type == 1):
        netList += '*<Rii> <node 1> <node 2> <Parasitic Resistance>\n'
        if(numLayers > 1):
            for i in range(numLayers):
                if i == 0:
                    netList += 'R11 L11R11 R11Bmr12 {R11p}\n'
                else:
                    netList += 'R'  + str(i+1) + str(i+1) +  ' L' + str(i+1) + str(i+1) + 'R' + str(i+1) + str(i+1) +' R' + str(i+1) + str(i+1) + 'Bmr' + str(i+1) + '1 {R' + str(i+1) + str(i+1) + 'p}\n'
        else:
            netList += 'R11 L11R11 0 {R11p}\n'
    else:
        netList += '*<Rii> <node 1> <node 2> <R=\'f(hertz)\'>\n'
        if(numLayers > 1):
            for i in range(numLayers):
                if i == 0:
                    netList += 'R11 L11R11 R11Bmr12 R=\'{Rdc11p}+{Rconst11p}*sqrt(hertz)\'\n'
                else:
                    netList += 'R'  + str(i+1) + str(i+1) +  ' L' + str(i+1) + str(i+1) + 'R' + str(i+1) + str(i+1) +' R' + str(i+1) + str(i+1) + 'Bmr' + str(i+1) + '1 R=\'{Rdc' + str(i+1) + str(i+1) + 'p}+{Rconst'+ str(i+1) + str(i+1) +'p}*sqrt(hertz)\'\n'
        else:
            netList += 'R11 L11R11 0 R=\'{Rdc11p}+{Rconst11p}*sqrt(hertz)\'\n'
        
        
    ###########################
    #Build Coupling statements#
    ###########################
    #Note: this will not be applicable to Model=3 for LTSPICE
    #1. Build coupling params
    netList += '\n*Inductor Coupling Terms\n'
    if(simulatorName == 'LTSPICE' or model_Type <=2):
        if(numLayers > 1):
            netList += '\n*Mutual Inductance Coupling Parameters\n'
            for i in range(numLayers):
                for j in range(i + 1, numLayers):
                    netList += '.param k' + str(i+1) + str(j+1) + 'calc=' +'L' + str(i+1) + str(j+1) + 'p/sqrt(L' + str(i+1) + str(i+1) + 'p*L' + str(j+1) + str(j+1) + 'p)\n'
    #2. Build coupling SPICE directives
            netList += '\n*Coupling SPICE Directives\n'
            netList += '*<kij> <Lii> <Ljj> <Lij/(sqrt(Lii*Ljj))>\n'
            for i in range(numLayers):
                for j in range(i + 1, numLayers):
                    netList += 'k' + str(i+1) + str(j+1) + ' L' + str(i+1) + str(i+1) + ' L' + str(j+1) + str(j+1) + ' {k' + str(i+1) + str(j+1) + 'calc}\n'
        
    ##############################
    #Build Capacitance Components#
    ##############################
    netList += '\n*Mutual and Auto Capacitances\n'
    netList += '*<Cnn> <node 1> <node 2> <Capacitance>\n'
    for i in range(numLayers):
        for j in range(i, numLayers):
            netList += 'C' + str(i+1) + str(j+1)
            if(i == j):
                netList += ' 0 '
            else:
                netList += ' ' + str(i+1) + ' '
            netList += str(j+1) + ' {C' + str(i+1) + str(j+1) + 'p}'
            netList += '\n'

    ####################################
    #Build Shunt Conductance Components#
    ####################################
    if(incCapLoss_Bool):
        netList += '\n*Dielectric Loss, Shunt Conductance\n'
        if(model_Type == 1):
            netList += '*<RCij> <node 1> <node 2> <1/GCij>\n'
        else:
            netList += '*<RCij> <node 1> <node 2> <R=\'f(hertz)\'>\n'
        for i in range(numLayers):
            for j in range(i, numLayers):
                node1 = str(i+1)
                node2 = 0
                netList += 'RC' + str(i+1) + str(j+1)
                if(i == j):
                    node2 = '0'
                else:
                    node2 = str(j+1)
                if(model_Type ==1):
                    netList += ' ' + node1 + ' ' + node2 + ' {1/GC' + str(i+1) + str(j+1) + 'p}\n'
                else:
                    netList += ' ' + node1 + ' ' + node2 + ' R=\'1/(2*pi*hertz*{GCconst' + str(i+1) + str(j+1) +'p})\'\n'
    ####################################
    #Build Mutual Resistance Components#
    ####################################
    if(numLayers > 1):
        netList += '\n*Mutual Resistance\n'
        netList += '*B-source=Current Dependent Voltage Source\n'
        if(model_Type == 1):
            netList += '*<Bmrij> <node 1> <node 2> <v=f(I)*Transresistance>\n'
        else:
            netList += '*<Bmrij> <node 1> <node 2> <v=\'f(hertz)\'>\n'
        for i in range(numLayers):
            lastH = ''
            for j in range(numLayers):
                if(i!=j):
                    netList += 'Bmr' + str(i+1)+ str(j+1) 
                    if(i == 0 or j == 0) and (j<=i+1):
                        #second stmt will include LiiandRepeat above
                        netList += ' R' + str(i+1)+ str(i+1)
                    else:
                        #else starts with previous Hij value
                        netList += lastH
                    #end with same
                    netList += 'Bmr' + str(i+1)+ str(j+1)
                    lastH = ' Bmr' + str(i+1)+ str(j+1)
                    #start 3rd term
                    if(i == numLayers - 1 and j == numLayers - 2):
                        netList += ' 0'
                    elif(i < numLayers -1 and j == numLayers - 1):
                        netList += ' ' +  str(i+2)
                    else:
                        netList += ' Bmr' + str(i+1)+ str(j+1)
                        #Final part of third term, if not interger node,
                        #is always the next term to come. We will never
                        #pass i=0, j=0. If the next term is i=j, then we need
                        #to increment one more!
                        #if(i = j+1):
                        #Next value needs an inc from there on
                        if(i==j+1):
                            netList += 'Bmr' + str(i+1)+ str(j+3)
                        else:
                            netList += 'Bmr' + str(i+1)+ str(j+2)
                    if(model_Type == 1):
                        #Add transresistance source.
                        #This is where we mod with v=i(Hxy)*trans
                        #Here we can choose whether Transresistance will remain linear
                        #or will increase with frequency by sqrt(f)
                        #Formula-> v=i(Bmr)*Rmr
                        #Rmr is constant here
                        #Finally add transresistance factor
                        #Make sure i always less than j
                        netList += ' v=(i(Bmr' + str(j+1)+ str(i+1)+')'
                        if(i<j):
                            netList += '*{R' + str(i+1)+ str(j+1) + 'p})\n'
                        else:
                            netList += '*{R' + str(j+1)+ str(i+1) + 'p})\n'
                    else:
                        netList += ' v=\'((i(Bmr' + str(j+1)+ str(i+1)+')'
                        if(i<j):
                            netList += '*{Rdc'+ str(i+1)+ str(j+1) +'p}+{Rconst'+ str(i+1) + str(j+1) +'p}*sqrt(hertz)))\'\n'
                        else:
                            netList += '*{Rdc'+ str(j+1)+ str(i+1) +'p}+{Rconst'+ str(j+1) + str(i+1) +'p}*sqrt(hertz)))\'\n'

    ############################
    #Generate .PARAM Statements#
    ############################
    if(model_Type >= 2):
        Rconst  = extractTwoPointData(numLayers, R_dict, R_dict_DC, RAC_freq)
    if(model_Type >= 3):
        Lint_const = extractTriplePointData(numLayers, L_dict, L_dict_HIGH, RAC_freq)
    netList += '\n*Resistance Parameters\n'
    if(model_Type == 1):
        for key, value in R_dict.items():
            netList += '.param ' + key + 'p=' + value + '\n'
    else:
        for i in range(numLayers):
            for j in range(i,numLayers):
                if(model_Type >= 2):
                    RDC_temp =  R_dict_DC['R' + str(i+1) + str(j+1)]
                    Rconst_temp = Rconst['R' + str(i+1) + str(j+1)]
                netList += '.param Rdc' + str(i+1) + str(j+1) + 'p='+str(RDC_temp)+'\n'
                netList += '.param Rconst' + str(i+1) + str(j+1) + 'p='+str(Rconst_temp)+'\n\n'

    netList += '\n*Capacitance Parameters\n'
    for key, value in C_dict.items():
        netList += '.param ' + key + 'p=' + value + '\n'
    if(incCapLoss_Bool):
        netList += '\n*Cap Conductance Parameters\n'
        for i in range(numLayers):
            for j in range(i, numLayers):
                tempGC = G_dict['G' + str(i+1) + str(j+1)]
                if(model_Type == 1):
                    tempGC = 2*np.pi*float(RAC_freq) * float(tempGC)
                    netList += '.param GC' + str(i+1) + str(j+1) + 'p=' + str(tempGC) + '\n'
                else:
                    #GCconstijp
                    netList += '.param GCconst' + str(i+1) + str(j+1) + 'p=' + str(tempGC) + '\n'
    netList += '\n*Inductance Parameters\n'
    if(model_Type <= 2):
        for key, value in L_dict.items():
            netList += '.param ' + key + 'p=' + value + '\n'
    else:
        for i in range(numLayers):
            for j in range(i,numLayers):
                if(model_Type >= 3):
                    Lint_temp = Lint_const['L' + str(i+1) + str(j+1)]
                    Lhigh_temp = L_dict_HIGH['L' + str(i+1) + str(j+1)]
                netList += '.param Lhigh' + str(i+1) + str(j+1) + 'p='+str(Lhigh_temp)+'\n'
                netList += '.param Lint' + str(i+1) + str(j+1) + 'p='+str(Lint_temp)+'\n\n'
    return netList
    
#In order to build frequency dependent mutual inductance, additional B elements must mirror currents into mutual inductances
#so that the voltages may be reflected properly into the mesh current. Fractional order Laplace cannot be used with ngSpice.
#The .param and K statements do not allow the hertz keyword to be used. At the present, this is the only workaround to generate
#frequency dependent impedance with fractional order roots in ngSpice.
def buildSPICENetlist_ngSpice_model2_3(numLayers, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH, R_dict_HIGH, RAC_freq, useParamStatements_Bool, model_Type):
    #This may be implented in the future so the user can choose where to place the param statements.
    ############################
    #Include header information#
    ############################
    netList = ''
    netList += '*Generic ' + str(numLayers) + '-Layer Planar Printed Inductor SPICE netlist\n'
    netList += '*Created by Appliqué:' + genDateStamp() + ' \n'
    netList += '*For use with following SPICE software: ngSpice\n'
    netList += '*Arbitrary behavioral source (B-source) is used for Resistance and Inductance.\n'
    netList += '*Capacitance and shunt conductance will be modelled as standard capacitance and frequency dependent resistance.\n'
    ##############################################
    #Generate Inductance and Resistance B-sources#
    ##############################################
    netList += '*Inductance and Resistance Components->\n'
    netList += '\n*B-source:\n'
    netList += '*   <BLRij> <node 1> <node 2> <V=v(vLijm)>\n'
    for i in range(numLayers):
        for j in range(numLayers):
            #Generate Component Name and first node
            if(j==0):
                netList += 'BLR'+str(i+1)+str(j+1) + ' ' + str(i+1)  
            else:
                netList += 'BLR'+str(i+1)+str(j+1) + ' BLR' + str(i+1)+str(j)+'_BLR' + str(i+1)+str(j+1)
            #Generate second node.
            if(j<numLayers-1):
                netList += ' BLR' + str(i+1)+str(j+1)+'_BLR' + str(i+1)+str(j+2)
            elif(j==numLayers-1 and i==numLayers-1):
                netList += ' 0'
            else:
                netList += ' ' + str(i+2)
            #Reference mirrored current source voltage.
            netList += ' V=v(VLR' + str(i+1) + str(j+1) + 'm1)\n'
    #####################################################
    #Generate Mirrored Current Sources and RL components#
    #####################################################
    netList += '\n\n*Mirrored Circuits for frequency dependent coupled impedance->\n'
    netList += '*   <BIij> <node 1> <node 2> <I=i(BLRij)>\n'
    if(model_Type >= 3):
        netList += '*   <Lij> <node 3> <node 1> <L=\'L(hertz)\'>\n'
    else:
        netList += '*   <Lij> <node 3> <node 1> <{Lijp}>\n'
    if(model_Type >= 2):
        netList += '*   <Rij> <node 2> <node 3> <R=\'R(hertz)\'>\n'
    else:
        netList += '*   <Rij> <node 2> <node 3> <{Rijp}>\n'
    
    for i in range(numLayers):
        for j in range(numLayers):
            #Generate Component Name and first node
            netList += '\n*   Z' + str(i+1) + str(j+1) + ' network\n' 
            netList += 'BI'+str(i+1) + str(j+1) + ' 0 ' + 'VLR' + str(i+1) + str(j+1) + 'm1 I=i(BLR' + str(j+1) + str(i+1) + ')\n'
            netList += 'R'+ str(i+1) + str(j+1) + ' VLR' + str(i+1) + str(j+1) +'m1 VLR' + str(i+1) + str(j+1) +'m2 '
            if(model_Type >= 2):
                if(i<=j):
                    netList += 'R=\'{Rdc'+ str(i+1) + str(j+1) + 'p}+{Rconst'+ str(i+1) + str(j+1) + 'p}*sqrt(hertz)\'\n'
                else:
                    netList += 'R=\'{Rdc'+ str(j+1) + str(i+1) + 'p}+{Rconst'+ str(j+1) + str(i+1) + 'p}*sqrt(hertz)\'\n'
            else:
                if(i<=j):
                    netList += '{R' + str(i+1) + str(j+1) + 'p}\n'
                else:
                    netList += '{R' + str(j+1) + str(i+1) + 'p}\n'
                    
            netList += 'L'+ str(i+1) + str(j+1) + ' VLR' + str(i+1) + str(j+1) +'m2 0 '
            if(model_Type >= 3):
                if(i<=j):
                    netList += 'L=\'{Lhigh'+ str(i+1) + str(j+1) + 'p}+{Lint'+ str(i+1) + str(j+1) + 'p}/sqrt(hertz)\'\n'
                else:
                    netList += 'L=\'{Lhigh'+ str(j+1) + str(i+1) + 'p}+{Lint'+ str(j+1) + str(i+1) + 'p}/sqrt(hertz)\'\n'
            else:
                if(i<=j):
                    netList += '{L'+ str(i+1) + str(j+1) + 'p}\n'
                else:
                    netList += '{L'+ str(j+1) + str(i+1) + 'p}\n'
    ##############################
    #Build Capacitance Components#
    ##############################
    netList += '\n*Mutual and Auto Capacitances\n'
    netList += '*<Cnn> <node 1> <node 2> <{Cijp}>\n'
    for i in range(numLayers):
        for j in range(i, numLayers):
            netList += 'C' + str(i+1) + str(j+1)
            if(i == j):
                netList += ' 0 '
            else:
                netList += ' ' + str(i+1) + ' '
            netList += str(j+1) + ' {C' + str(i+1) + str(j+1) + 'p}'
            netList += '\n'

    ####################################
    #Build Shunt Conductance Components#
    ####################################
    #Shunt Conductance
    netList += '\n*Dielectric Loss, Shunt Conductance\n'
    if(model_Type >=2):
        netList += '*<RCij> <node 1> <node 2> <R=\'f(hertz)\'>\n'
    else:
        netList += '*<RCij> <node 1> <node 2> <1/{GCijp}>\n'
    for i in range(numLayers):
        for j in range(i, numLayers):
            node1 = str(i+1)
            node2 = 0
            netList += 'RC' + str(i+1) + str(j+1)
            if(i == j):
                node2 = '0'
            else:
                node2 = str(j+1)
            if(model_Type ==1):
                netList += ' ' + node1 + ' ' + node2 + ' {1/GC' + str(i+1) + str(j+1) + 'p}\n'
            else:
                netList += ' ' + node1 + ' ' + node2 + ' R=\'1/(2*pi*hertz*{GCconst' + str(i+1) + str(j+1) +'p})\'\n'
    ############################
    #Generate .PARAM Statements#
    ############################
    if(model_Type >=3):
        Lint_const = extractTriplePointData(numLayers, L_dict, L_dict_HIGH, RAC_freq)
    if(model_Type >=2):
        Rconst  = extractTwoPointData(numLayers, R_dict, R_dict_DC, RAC_freq)
    netList += '\n*Resistance Parameters\n'
    for i in range(numLayers):
        for j in range(i,numLayers):
            if(model_Type >=2):
                RDC_temp =  R_dict_DC['R' + str(i+1) + str(j+1)]
                Rconst_temp = Rconst['R' + str(i+1) + str(j+1)]
                netList += '.param Rdc' + str(i+1) + str(j+1) + 'p=' + str(RDC_temp) + '\n'
                netList += '.param Rconst' + str(i+1) + str(j+1) + 'p=' + str(Rconst_temp) + '\n'
            else:
                R_temp = R_dict['R' + str(i+1) + str(j+1)]
                netList += '.param R' + str(i+1) + str(j+1) + 'p=' + str(R_temp) + '\n'
    netList += '\n'
    netList += '\n*Capacitance Parameters\n'
    for key, value in C_dict.items():
        netList += '.param ' + key + 'p=' + value + '\n'
    netList += '\n*Cap Conductance Parameters\n'
    for i in range(numLayers):
        for j in range(i, numLayers):
            tempGC = G_dict['G' + str(i+1) + str(j+1)]
            if(model_Type == 1):
                tempGC = 2*np.pi*float(RAC_freq) * float(tempGC)
                netList += '.param GC' + str(i+1) + str(j+1) + 'p=' + str(tempGC) + '\n'
            else:
                #GCconstijp
                netList += '.param GCconst' + str(i+1) + str(j+1) + 'p=' + str(tempGC) + '\n'

    netList += '\n*Inductance Parameters\n'
    for i in range(numLayers):
        for j in range(i,numLayers):
            if(model_Type >=3):
                Lint_temp = Lint_const['L' + str(i+1) + str(j+1)]
                Lhigh_temp = L_dict_HIGH['L' + str(i+1) + str(j+1)]
                netList += '.param Lhigh' + str(i+1) + str(j+1) + 'p='+str(Lhigh_temp)+'\n'
                netList += '.param Lint' + str(i+1) + str(j+1) + 'p='+str(Lint_temp)+'\n'
            else:
                L_temp = L_dict['L' + str(i+1) + str(j+1)]
                netList += '.param L'+ str(i+1) + str(j+1) + 'p='+str(L_temp)+'\n'
    netList += '\n'
    return netList


#This function builds SPICE netlists from Laplace transfer function. For LTSPICE, the Laplace is the only way to generate frequency
#dependent components. In ngSpice, there are various ways to generate frequency dependent impedance. The R,L,C and subckt may use the
#hertz keyword to generate frequency dependent impedance. However, with the usage of hertz, the op point must be calculated at each frequency.
#The Laplace method is much faster in ngSpice. However, fractioncal order Laplace statements cannot be simulated in ngSpice, limiting this only
#applicable to 1-point model for ngSpice.
#
#In LTSPICE, the Laplace statement coupled with the B-source is fairly straightforward and is applicable to 1, 2 or 3-point models.
#ngSpice, however, does not directly allow the usage of Laplace. XSPICE option must be installed with ngSpice. A B and A-element with 
#.model statement and s_xfer can be used to define the transfer function.

def buildSPICENetlist_Laplace_ONLY(numLayers, L_dict, R_dict, C_dict, G_dict, L_dict_DC, R_dict_DC, L_dict_HIGH, R_dict_HIGH,
                    simulatorName, incCapLoss_Bool, RAC_freq, model_Type, useParamStatements_Bool):
    #Model Type:
    #If 1, then these are B-elements acting as standard L, R, C.
    #If 2, then resistors will have included frequency dependent effects. Skin + Prox
    #If 3, then inductors will also have included frequency dependent effects.
    
    #For inductance and resistance, the B-elements will be current controlled voltage sources. Bx,y will be sourced by Iy. and the impedance will be Zx,y.
    #So if this is self-impedance, the B11 will be sourced by I(B1) and the transimpedance will be R11(s)+s*L11(s), etc. 

    #This may be implemented in the future so the user can select where they want the param statements placed.
    #if(useParamStatements_Bool):
    #    print("In future the B-element only option will support param statements.")
    #Check if ngSpice and model is larger than 1
    if(simulatorName == 'ngSPICE' and model_Type > 1):
        writeConsole('Error: Laplace only netlists not available for 2 or 3 point models with ngSpice','red')
        return None
    if(model_Type >= 2):
        Rconst  = extractTwoPointData(numLayers, R_dict, R_dict_DC, RAC_freq)
    if(model_Type >= 3):
        Lint_const = extractTriplePointData(numLayers, L_dict, L_dict_HIGH, RAC_freq)
    #Include header information
    netList = ''
    netList += '*Generic ' + str(numLayers) + '-Layer Planar Printed Inductor SPICE netlist\n'
    netList += '*Created by Appliqué:' + genDateStamp() + ' \n'
    netList += '*For use with following SPICE software: ' + simulatorName + '\n'
    netList += '*All R, L, and C elements are simulated with arbitrary behavioral source (B-source).\n'
    netList += '\n*Notes:\n' 
    netList += '*   1. Self and mutual impedances will comprise both resistance and inductance or conductance and capacitance in one single B-source.\n'
    netList += '*   2. Unless a primary node (1,2,3,etc.) node naming convention is the combination of connected devices.\n\n\n'
    #Generate Inductance and Resistance components
    netList += '*Inductance and Resistance Components->\n'
    netList += '\n*B-source:\n'
    netList += '*   <BLRij> <node 1> <node 2> '
    if(simulatorName == 'LTSPICE'):
        netList += '<V=I(BLRjj)> <Laplace = Z(s)>\n'
    elif(simulatorName == 'ngSPICE'):
        netList += '<V=v(aLRij)>\n'
    for i in range(numLayers):
        for j in range(numLayers):
            #Generate Component Name and first node
            if(j==0):
                netList += 'BLR'+str(i+1)+str(j+1) + ' ' + str(i+1)  
            else:
                netList += 'BLR'+str(i+1)+str(j+1) + ' BLR' + str(i+1)+str(j)+'_BLR' + str(i+1)+str(j+1)
            #Generate second node.
            if(j<numLayers-1):
                netList += ' BLR' + str(i+1)+str(j+1)+'_BLR' + str(i+1)+str(j+2)
            elif(j==numLayers-1 and i==numLayers-1):
                netList += ' 0'
            else:
                netList += ' ' + str(i+2)
            #Generate I source. Any of the B-sources in the current path will work.
            if(simulatorName == 'LTSPICE'):
                netList += ' V=I(BLR' + str(j+1) + str(j+1) + ')'
            #For ngSPICE, a different mechanism is used to supply the B supply
            elif(simulatorName == 'ngSPICE'):
                netList += ' V=v(aLR' + str(i+1) + str(j+1) + ')'
            #Collect values for frequency dependent impedance
            if(i<j):
                Rij_temp = R_dict['R' + str(i+1) + str(j+1)]
                Lij_temp = L_dict['L' + str(i+1) + str(j+1)]
                if(model_Type >= 2):
                    RDC_temp =  R_dict_DC['R' + str(i+1) + str(j+1)]
                    Rconst_temp = Rconst['R' + str(i+1) + str(j+1)]
                if(model_Type >= 3):
                    Lint_const_temp = Lint_const['L' + str(i+1) + str(j+1)]
                    L_dict_HIGH_temp = L_dict_HIGH['L' + str(i+1) + str(j+1)]
            else:
                Rij_temp = R_dict['R' + str(j+1) + str(i+1)]
                Lij_temp = L_dict['L' + str(j+1) + str(i+1)]
                if(model_Type >= 2):
                    RDC_temp =  R_dict_DC['R' + str(j+1) + str(i+1)]
                    Rconst_temp = Rconst['R' + str(j+1) + str(i+1)]
                if(model_Type >= 3):
                    Lint_const_temp = Lint_const['L' + str(j+1) + str(i+1)]
                    L_dict_HIGH_temp = L_dict_HIGH['L' + str(j+1) + str(i+1)]
            if(simulatorName == 'LTSPICE'):
                #Include Laplace statement
                #Create frequency dependent resistance part
                if(model_Type>=2):
                    netList += ' Laplace = ('+str(RDC_temp) + '+' +str(Rconst_temp) + '*sqrt(abs(s)/(2*pi))'
                else:
                    netList += 'Laplace = (' + str(Rij_temp)
                #Now append frequency dependent inductance part
                if(model_Type>=3):
                    netList += ' + s*(' + str(L_dict_HIGH_temp) + '+' +str(Lint_const_temp) + '/sqrt(abs(s)/(2*pi))))'
                else:
                    netList += ' + s*' + str(Lij_temp) + ')'
            netList += '\n'
    #This will only work for 1-point model!
    if(simulatorName == 'ngSPICE'):
        netList += '\n*X-Spice Laplace Blocks for Inductance and Resistance Components->\n'
        for i in range(numLayers):
            for j in range(numLayers):
                ##Generate Current Mirror.
                netList += '*BLR'+str(i+1)+str(j+1)+'\n'
                netList += 'BLR'+str(i+1)+str(j+1)+'mirror aLR'+str(i+1)+str(j+1)+ ' 0'
                #Generate controlling current statement
                netList += ' I=-1*i(BLR'+str(i+1)+str(j+1)+')\n'
                ##Next Generate G-source and physical nodes.
                netList += 'GLR'+str(i+1)+str(j+1) + 'mirror aLR'+str(i+1)+str(j+1)+ ' 0' 
                #Next generate G driving nodes and Gain
                netList += ' aLR'+str(i+1)+str(j+1)+ 'out 0 1\n'
                ##Next Generate the A-source
                netList += 'ALR'+str(i+1)+str(j+1)+'mirror %v(aLR'+str(i+1)+str(j+1)+ ') %v(aLR'+str(i+1)+str(j+1)+'out) LR'+str(i+1)+str(j+1)+'xfer\n'
                ##Next Generate the Laplace model statment
                netList += '.model LR'+str(i+1)+str(j+1)+'xfer s_xfer(gain=1.0 num_coeff=[1] den_coeff=['
            #Collect values for frequency dependent impedance
            if(i<j):
                Rij_temp = R_dict['R' + str(i+1) + str(j+1)]
                Lij_temp = L_dict['L' + str(i+1) + str(j+1)]
            else:
                Rij_temp = R_dict['R' + str(j+1) + str(i+1)]
                Lij_temp = L_dict['L' + str(j+1) + str(i+1)]
            netList += '{' + str(Lij_temp) + '} {' + str(Rij_temp) + '})\n'

    #Generate Capacitance and Conductance components.
    netList += '\n\n*Capacitance and Conductance Components->\n'
    netList += '\n*B-source:\n'
    netList += '*   <BCGij> <node 1> <node 2> '
    if(simulatorName == 'LTSPICE'):
        netList += '<I=V(BCGxx,BCGyy)> <Laplace = Y(s)>\n'
    elif(simulatorName == 'ngSPICE'):
        netList += '*Not built out yet for ngspice.'
        #netList += '<I=V(BCGxx,BCGyy)*Y(hertz)>\n'
    for i in range(numLayers):
        for j in range(i,numLayers):
            #Generate Component Name, first and second node, and source statement
            if(i==j):
                netList += 'BCG'+str(i+1)+str(j+1) + ' ' + str(i+1) + ' 0 I=V(' + str(i+1) + ',0)'
            else:
                netList += 'BCG'+str(i+1)+str(j+1) + ' ' + str(i+1) + ' ' + str(j+1) + ' I=V(' + str(i+1) + ',' + str(j+1) + ')'
            #Add frequency dependent component
            if(incCapLoss_Bool):
                Gij_temp =  G_dict['G' + str(i+1) + str(j+1)]
            Cij_temp =  C_dict['C' + str(i+1) + str(j+1)]
            if(simulatorName == 'LTSPICE'):
                if(incCapLoss_Bool):
                    netList += ' Laplace = (abs(s)*' + str(Gij_temp) + '+s*' + str(Cij_temp) + ')'
                else:
                    netList += ' Laplace = (s*' + str(Cij_temp) + ')'
            elif(simulatorName == 'ngSPICE'):
                if(incCapLoss_Bool):
                    netList += '*(2*pi*hertz*' + str(Gij_temp) + '+2i*pi*hertz*' + str(Cij_temp) + ')'
                else:
                    netList += '*(2i*pi*hertz*' + str(Cij_temp) + ')'
            netList += '\n'
    return netList

def appendStimCmd_netlist(netList, simulatorName, sweepType, pps, freqStart, freqEnd, exportFileName, ngSpice_rshunt_default_Bool, plotImp_Bool, rshunt):
    if(simulatorName == 'ngSPICE'):
        netList += '\n*ngSPICE Current Stimulus\n'
        netList += 'i1 0 measure AC 1 DC 0\n'
        netList += '\n*ngSPICE Dummy Voltage Source\n'
        netList += 'vdummy measure 1 DC 0\n'
        if(ngSpice_rshunt_default_Bool):
            #Required to avoid singular matrix error
            #Note LTSPICE has a default gshunt = 1e-12 for floating nodes
            #netList += '.option rshunt=1e12'
            netList += '.option rshunt='+rshunt
        netList += '\n*ngSPICE Simulation Control\n'
        netList += '.control\n'
        #Here change pps freqStart freqEnd as needed
        netList += 'AC ' + sweepType + ' ' + str(pps) + ' ' + str(freqStart) + ' ' + str(freqEnd) + ';\n'
        netList += 'write ' + exportFileName + ' v(1);\n'
        if(plotImp_Bool):
            netList += 'let ZIN=v(1)/i(vdummy);\n'
            netList += 'PLOT ZIN title \'PCB Coil Input Impedance\';\n'
        else:
            netList += 'quit;\n'
        netList += '.endc\n'
    elif(simulatorName == 'LTSPICE'):
        netList += '\n*Current Stimulus\n'
        netList += 'I1 0 1 AC 1\n'
        netList += '\n*Simulation Type\n'
        netList += '.AC ' + sweepType + ' ' + str(pps) + ' ' + str(freqStart) + ' ' + str(freqEnd) + '\n'
        netList += '.PROBE V(1)\n'
        if(plotImp_Bool):
            netList += '.PRINT AC V(1)\n'
    netList += '\n.end\n'
    return netList

def batchRunSPICE(netList_dict, outputFile):
    writeConsole('Beginning Batch Ngspice run','green')

'''
**************************
* Parse ngSPICE Raw File *
**************************
'''
def parseSinglePointACngSPICE(fileName):
    print("Filename=" + fileName)
    frequency = -1
    zinReal = 1
    zinImag = 1
    rawExtract = []
    try:
        with open(fileName, "r") as ngRawFile:
            for line in ngRawFile:
                rawExtract.append(line)
            ngRawFile.close()
    except EnvironmentError:
        writeConsole('Aborting Parse: Failed to open ' + fileName + '!','red')
        return
    freqRaw = (((rawExtract[-3])[2:]).strip()).split(',')
    print(freqRaw)
    zinComp = (rawExtract[-2].strip()).split(',')
    if(is_number(freqRaw[0]) and is_number(zinComp[0]) and is_number(zinComp[1])):
        frequency = float(freqRaw[0])
        zinReal = float(zinComp[0])
        zinImag = float(zinComp[1])
    return frequency, zinReal, zinImag

#Note Different versions of LTSPICE generate spacing differently!
def parseMultiPointACngSPICE(fileName):
    zinDict = {}
    frequency = -1
    zinReal = 1
    zinImag = 1
    rawExtract = []
    #Open all files and copy all lines to rawExtract List
    try:
        with open(fileName, "r") as ngRawFile:
            for line in ngRawFile:
                rawExtract.append(line)
            ngRawFile.close()
    except EnvironmentError:
        writeConsole('Aborting Parse: Failed to open ' + fileName + '!','red')
        return
    #Now go through each line and get freq, real, imag
    #This needs to be fixed DOES NOT WORK. Need to sort through all lines
    #in raw file!
    refIndex = 0
    for index,line in enumerate(rawExtract):
        if(line.find('Values:') > -1):
            refIndex = index + 1
            break
    while(refIndex < len(rawExtract) - 1):
         freqRaw = (((rawExtract[refIndex])[3:]).strip()).split(',')
         zinComp = (((rawExtract[refIndex+1])[0:]).strip()).split(',')
         if(len(freqRaw) > 1):
            try:
                freqRaw = ((re.split(r'\s[0-9]+\s', rawExtract[refIndex])[1]).strip()).split(',')
                frequency = float(freqRaw[0])
                zinReal = float(zinComp[0])
                zinImag = float(zinComp[1])
                zinDict[frequency] = [zinReal, zinImag]
                refIndex+=2
            except:
                #Older versions of SPICE may not have leading space. Try without space
                try:
                    freqRaw = ((re.split(r'[0-9]+\s', rawExtract[refIndex])[1]).strip()).split(',')
                    frequency = float(freqRaw[0])
                    zinReal = float(zinComp[0])
                    zinImag = float(zinComp[1])
                    zinDict[frequency] = [zinReal, zinImag]
                    refIndex+=2
                except:
                    writeConsole('Aborting Parse: RAW file unrecognized','yellow')
                    return
         else:
            refIndex+=1
    return zinDict
    
'''
********************************
* FastHenry2 VBS-COM Interface *
********************************
'''
def runFastHenry2(inFileName,recipBool,timeOutBool,timeOut):
    fileNameAbs = os.path.abspath(inFileName)
    inductance_Dict = {}
    resistance_Dict = {}
    verify_Dict = {}
    frequencyEval = -1
    optsList = ['cscript', '//nologo', 'FastHenry2_COM_ifc.vbs', '-fh', fileNameAbs, '-ap']
    if(timeOutBool):
        optsList.append('-to')
        optsList.append(str(timeOut))
    fhProc = subprocess.Popen(optsList, shell=False, creationflags=DETACHED_PROCESS, stdout=subprocess.PIPE)
    consoleActiveAnimation(fhProc)
    rawFHcomIFC = fhProc.communicate()[0]
    FHcom_output = rawFHcomIFC.decode("utf-8").splitlines()
    parseCode = 0
    for line in FHcom_output:
        if(line is not ""):
            splitLine = line.split()
            #print(str(splitLine[0]) + "=" + str(splitLine[1]))
            if splitLine[0][:1] == "L":
                #We have Inductance Values
                inductance_Dict[splitLine[0]] = splitLine[1]
            elif splitLine[0][:1] == "R":
                #We have Resistance Values
                resistance_Dict[splitLine[0]] = splitLine[1]
            elif splitLine[0][:4] == "FREQ":
                #We have the Frequency
                frequencyEval = float(splitLine[1])
            else:
                #The rest is verification information
                verify_Dict[splitLine[0]] = splitLine[1]
    #We should improve the error handling ability here
    try:
        matDimension = int(verify_Dict['DIM'])
        if matDimension < 0:
            parseCode = -12
            return parseCode, None, None, None
        parseCode = checkFHMat(inductance_Dict, resistance_Dict)
        if(recipBool):
            finalInd_Dict, finalRes_Dict = averageFHMat(inductance_Dict, resistance_Dict)
            return parseCode, finalInd_Dict, finalRes_Dict, frequencyEval
        else:
            return parseCode, inductance_Dict, resistance_Dict, frequencyEval
    except KeyError:
        parseCode = -11
        return parseCode, None, None, None

def checkFHMat(ind_Dict, res_Dict):
    code = 0
    codeL = 0
    codeR = 0
    dimension = int(np.sqrt(len(ind_Dict)))
    for i in range(dimension):
        for j in range(dimension):
            if(float(ind_Dict["L"+str(i+1)+str(j+1)]) < 0):
                codeL = -1
            if(float(res_Dict["R"+str(i+1)+str(j+1)]) < 0):
                codeR = -2
    return codeL+codeR

def averageFHMat(ind_Dict, res_Dict):
    dimension = int(np.sqrt(len(ind_Dict)))
    avgInd_Dict = {}
    avgRes_Dict = {}
    for i in range(dimension):
        for j in range(i, int(dimension)):
            if(i == j):
                avgInd_Dict["L"+str(i+1)+str(j+1)] = float(ind_Dict["L"+str(i+1)+str(j+1)])
                avgRes_Dict["R"+str(i+1)+str(j+1)] = float(res_Dict["R"+str(i+1)+str(j+1)])
            else:
                temp1 = float(ind_Dict["L"+str(i+1)+str(j+1)])
                temp2 = float(ind_Dict["L"+str(j+1)+str(i+1)])
                avgInd_Dict["L"+str(i+1)+str(j+1)] = (temp1+temp2)/2
                temp1 = float(res_Dict["R"+str(i+1)+str(j+1)])
                temp2 = float(res_Dict["R"+str(j+1)+str(i+1)])
                avgRes_Dict["R"+str(i+1)+str(j+1)] = (temp1+temp2)/2
    return avgInd_Dict, avgRes_Dict

'''
********************************
* FasterCap VBS-COM Interface  *
********************************
'''

def runFasterCap(inFileName, relErrLim, scale, showWindow, recipBool, timeOutBool, timeOut):
    #add timeOut options
    #def runFasterCap(inFileName, relErrLim, scale, showWindow, recipBool, timeOutBool, timeOut):
    fileNameAbs = os.path.abspath(inFileName)
    capacitance_Dict = {}
    conductance_Dict = {}
    verify_Dict = {}
    optsList = ['cscript', '//nologo', 'FasterCap_COM_ifc.vbs', '-fc', fileNameAbs, '-ap','-a',str(relErrLim)]
    if(timeOutBool):
        optsList.append('-to')
        optsList.append(str(timeOut))
    if showWindow:
        optsList.append('-w')
    fcProc = subprocess.Popen(optsList, shell=False, creationflags=DETACHED_PROCESS, stdout=subprocess.PIPE)
    consoleActiveAnimation(fcProc)
    rawFCcomIFC = fcProc.communicate()[0]
    FCcom_output = rawFCcomIFC.decode("utf-8").splitlines()
    #DEBUG
    #print('RAW LIST:')
    #print(FCcom_output)
    #DEBUG
    for line in FCcom_output:
        if(line is not ""):
            splitLine = line.split()
            #print(str(splitLine[0]) + "=" + str(splitLine[1]))
            if splitLine[0][:2] == "CM":
                #We have the Maxwell Cap Values
                capacitance_Dict[splitLine[0]] = splitLine[1]
            elif splitLine[0][:2] == "GC":
                #We have the Conductance Values
                conductance_Dict[splitLine[0]] = splitLine[1]
            else:
                #The rest is verification information
                verify_Dict[splitLine[0]] = splitLine[1]
    #print(verify_Dict)
    parseCode, parseCap_Dict, parseCond_Dict = processFasterCapIFC(capacitance_Dict, conductance_Dict, verify_Dict, scale)
    if(parseCode == -1 or parseCode == -2 or parseCode < -5):
        return parseCode, None, None
    if(recipBool):
        finalCap_Dict, finalCond_Dict = averageFCMat(parseCap_Dict, parseCond_Dict)
        return parseCode, finalCap_Dict, finalCond_Dict
    else:
        return parseCode, parseCap_Dict, parseCond_Dict

def processFasterCapIFC(maxCap_Dict, maxCond_Dict, ver_Dict, scale):
    cap_Dict = {}
    cond_Dict = {}
    warningCode = 0
    #Make sure these aren't empty first
    if(maxCap_Dict and maxCond_Dict and ver_Dict):
        #Make sure EXIT Code is okay and DIM>0
        exitCode = ver_Dict['EXIT']
        matDimension = ver_Dict['DIM']
        if(exitCode == '0' and int(matDimension) > 0):
            #Check for off-diagonal positives and non dom
            for i in range(int(matDimension)+1):
                sumCM = 0
                sumGC = 0
                for j in range(int(matDimension)+1):
                    #Add up all columns in a row.
                    if(i == j):
                        #Diagonal cap must be positive
                        #Diagonal cond must be negative
                        if((float(maxCap_Dict["CM"+str(i+1)+str(j+1)]) < 0) or (float(maxCond_Dict["GC"+str(i+1)+str(j+1)]) > 0) ):
                            warningCode = -2
                    else:
                        #Off Diagonal cap must be negative
                        #Off Diagonal cond must be positive
                        if((float(maxCap_Dict["CM"+str(i+1)+str(j+1)]) > 0) or (float(maxCond_Dict["GC"+str(i+1)+str(j+1)]) < 0) ):
                            warningCode = -1
                        #print("C"+str(i+1)+str(j+1)+ " =" + maxCap_Dict["CM"+str(i+1)+str(j+1)])
                        cap_Dict["C"+str(i+1)+str(j+1)] = abs(float(maxCap_Dict["CM"+str(i+1)+str(j+1)])/scale)
                        cond_Dict["G"+str(i+1)+str(j+1)] = abs(float(maxCond_Dict["GC"+str(i+1)+str(j+1)])/scale)
                    sumCM += float(maxCap_Dict["CM"+str(i+1)+str(j+1)])
                    sumGC += float(maxCond_Dict["GC"+str(i+1)+str(j+1)])
                #Check dominanace of Matrix
                if(sumCM < 0 or sumGC > 0):
                    warningCode = -3
                    #print("DEBUG: SUM GC="+str(sumGC))
                #print("C"+str(i+1)+str(i+1)+ " =" + str(sumCM))
                cap_Dict["C"+str(i+1)+str(i+1)] = str(sumCM/scale)
                cond_Dict["G"+str(i+1)+str(i+1)] = str(abs(sumGC/scale))
        else:
            return -12, None, None
    else:
        return -99, None, None
    print("Warning Code = " + str(warningCode))
    return warningCode, cap_Dict, cond_Dict

def averageFCMat(cap_Dict, cond_Dict):
    dimension = int(np.sqrt(len(cap_Dict)))
    avgCap_Dict = {}
    avgCond_Dict = {}
    for i in range(dimension):
        for j in range(i, int(dimension)):
            if(i == j):
                avgCap_Dict["C"+str(i+1)+str(j+1)] = cap_Dict["C"+str(i+1)+str(j+1)]
                avgCond_Dict["G"+str(i+1)+str(j+1)] = cond_Dict["G"+str(i+1)+str(j+1)]
            else:
                temp1 = cap_Dict["C"+str(i+1)+str(j+1)]
                temp2 = cap_Dict["C"+str(j+1)+str(i+1)]
                avgCap_Dict["C"+str(i+1)+str(j+1)] = (temp1+temp2)/2
                temp1 = cond_Dict["G"+str(i+1)+str(j+1)]
                temp2 = cond_Dict["G"+str(j+1)+str(i+1)]
                avgCond_Dict["G"+str(i+1)+str(j+1)] = (temp1+temp2)/2
    return avgCap_Dict, avgCond_Dict

#Universal External App exit code handler.
#Replaces both fcCodeReportConsole and fhCodeReportConsole
def extAppCodeReport(appID, exitCode):
    externalAppList = ["FasterCap", "FastHenry2", "ngSPICE", "LTSpice", "External App"]
    errorTextList = ["Success", "Warning", "Error"]
    errorColorList = ["green","yellow","red"]
    errorTextColorIndex = 0
    errorMsg = ""
    if(appID > 4):
        appID = 4
    if(appID == 0):
        #FasterCap specific error messages
        #OK
        if(exitCode == 0):
            errorMsg = "normally"
        #Warnings
        elif(exitCode == -1):
            errorMsg = "with positive off-diagonal values"
        elif(exitCode == -2):
            errorMsg = "with negative diagonal values"
        elif(exitCode == -3):
            errorMsg = "with non-diagonally dominant matrix"
        #Errors
        elif(exitCode == -11):
            errorMsg = "with a timeout"
        elif(exitCode == -12):
            errorMsg = "and returned zero-size matrices"
        else:
            errorMsg = "with an error"
        
    elif(appID == 1):
        #FastHenry2 specific error messages
        #OK
        if(exitCode == 0):
            errorMsg = "normally"
        #Warnings
        elif(exitCode == -1):
           errorMsg = "with negative inductances"
        elif(exitCode == -2):
           errorMsg = "with negative resistances"
        elif(exitCode == -3):
           errorMsg = "with negative inductances and resistances"
        #Errors
        elif(exitCode == -11):
           errorMsg = "with a timeout"
        elif(exitCode == -12):
            errorMsg = "and returned zero-size matrices"
        else:
            errorMsg = "with an error"
    else:
        if(exitCode == 0):
            errorMsg = "normally"
        elif(exitCode < 0 and exitCode < -11):
            errorMsg = "with a warning"
        else:
            errorMsg = "with an error"
    #Error code should move from 0 at green to red for more negative.
    #For simplicity, 0 will be green
    #-1 to -10 will be yellow
    #<-10 will be red
    if(exitCode == 0):
        errorTextColorIndex = 0
    elif(exitCode < 0 and exitCode > -11):
        errorTextColorIndex = 1
    else:
        errorTextColorIndex = 2
    #Write Result to console
    writeConsole(errorTextList[errorTextColorIndex] + ":" + externalAppList[appID] + " ended " + errorMsg, errorColorList[errorTextColorIndex])
    #Finally, may need a way to communicate this to Auto Mode so the app can exit
            

def terminateApplication(appID):
    if(config.threadActiveApp_ID > -1):
        externalAppList_EXE = ["fastercap.exe", "fasthenry2.exe", "ngSPICE", "LTSpice"]
        optsList = ['taskkill', '/im', externalAppList_EXE[appID], '/f']
        writeConsole("Terminating " + externalAppList_EXE[appID] + "...", "yellow")
        killThreadProc = subprocess.Popen(optsList, shell=False, creationflags=DETACHED_PROCESS, stdout=subprocess.PIPE)
        consoleActiveAnimation(killThreadProc)
        if(appID == 0 or appID == 1):
            #Cscriptwill be running!
            #Killing cscript may be dangerous to other apps!
            optsList = ['taskkill', '/im', 'cscript.exe', '/f']
            writeConsole("Terminating cscript.exe...", "yellow")
            killThreadProc = subprocess.Popen(optsList, shell=False, creationflags=DETACHED_PROCESS, stdout=subprocess.PIPE)
            consoleActiveAnimation(killThreadProc)
        config.threadActiveApp_ID = -1

#Produces a simple rotating line to let the user
#know Python hasn't locked up.
'''
WARNING THIS NEEDS TO BE FIXED
Suggest a multi-threaded approach:
https://stackoverflow.com/questions/2581817/python-subprocess-callback-when-cmd-exits
'''
def consoleActiveAnimation(pid):
    spinChars = ['|','/','-','\\','|','/','-','\\']
    retCode = None
    while retCode is None:
        if(config.threadActiveApp_ID == -1 and config.killAutoThread_bool):
            break
        for spinC in spinChars:
            retCode = pid.poll()
            if retCode is not None:
                break
            stdout.write("\rWorking %c" % spinC)
            time.sleep(1)
    stdout.write("\rDone           \n\n")
    return retCode
    
def progressWrite(progStr, completedProc, totalNumOfProcess):
    perComp = completedProc/float(totalNumOfProcess) * 100.0
    perComp = round(perComp, 2)
    writeConsole(progStr + ':' + str(perComp) + '%','cyan')
#We are expecting two sets mergered into one: LR and CG
def parseIFCfile(fileName):
    paramDict1 = {}
    paramDict2 = {}
    frequency = -1
    try:
        with open(fileName, "r") as paramFile:
            splitLine = []
            for line in paramFile:
                splitLine = line.split()
                if(splitLine[0][:1] == 'L' or splitLine[0][:1] == 'C'):
                    paramDict1[splitLine[0]] = splitLine[1]
                elif(splitLine[0][:4] == 'FREQ'):
                    frequency = splitLine[1]
                else:
                    paramDict2[splitLine[0]] = splitLine[1]
            paramFile.close()
    except EnvironmentError:
        writeConsole('Aborting Parse: Failed to open ' + fileName + '!','red')
        return
    #print(paramDict1)
    #print(paramDict2)
    if(float(frequency) > -1):
        return paramDict1, paramDict2, frequency
    else:
        return paramDict1, paramDict2

#Assuming that both Skin and Proximity effect of RAC is proportional to 
#sqrt(f), we will use a data point at fop to calculate the unknown constant
#of below formula. The unknown constant is the Rskin + Rprox component.
def extractTwoPointData(numLayers, R_dict, R_dict_DC, frequencyOP):
    Rconst_dict = {}
    #fop is some arbitrary frequency point
    #Formula-> Rtot = RDC + (c1+c2)*sqrt(freq)
    #Formula-> Rtot = RDC + ((Rfop - RDC)/sqrt(fop)) * sqrt(freq)
    #Formula->(c1+c2) = (Rtot(@fop) - RDC)/sqrt(fop)
    #Return dictionary of RDC values for each R
    #dimension = int(np.sqrt(len(R_dict)))
    dimension = numLayers - 1
    for i in range(dimension + 1):
        for j in range(i, int(dimension + 1)):
            Rconst_dict["R"+str(i+1)+str(j+1)] = (float(R_dict["R"+str(i+1)+str(j+1)]) - float(R_dict_DC["R"+str(i+1)+str(j+1)]))/np.sqrt(float(frequencyOP)) 
    return Rconst_dict
    
#Here we assume that skin effect causes a reduction in inductance.
#We know that Ltot = Lexternal at very high frequency
#At DC Ltot is comprises inductances across entire inductor.
#We will solve for the unknown constant, c3, which is Lint with a fop
#data point.
def extractTriplePointData(numLayers, L_dict, L_dict_HIGH, frequencyOP):
    #Formula -> Ltot = Lfhigh + c3 * sqrt(freq)/freq
    #Formula -> c3 = (Ltot(@fop)-Lhf(@fhigh))*sqrt(fop)
    Lint_const_dict = {}
    #dimension = int(np.sqrt(len(L_dict)))
    dimension = numLayers - 1
    for i in range(dimension + 1):
        for j in range(i, int(dimension + 1)):
            Lint_const_dict["L"+str(i+1)+str(j+1)] = (float(L_dict["L"+str(i+1)+str(j+1)]) - float(L_dict_HIGH["L"+str(i+1)+str(j+1)]))*np.sqrt(float(frequencyOP)) 
    return Lint_const_dict
'''
************************
* FastHenry2 Functions *
************************
'''
def buildFastHenryHeader(fileName,pcbDict):
    headerStr = ''
    pcbData = ''
    keyListPCB = []
    for key in pcbDict.keys():
        keyListPCB.append(key)
    for key in keyListPCB:
        pcbData += "*    " + key + " : " + str(pcbDict[key]) + "\n"
    headerStr += "* "+fileName+" generated "+genDateStamp()+"\n"
    headerStr += "* Generated by Appliqué\n"
    headerStr += "* Contact: brodym@uw.edu\n"
    headerStr += "* PCB Data (mil):\n"
    headerStr += pcbData
    return headerStr

#Need to investigate note. Coil counterwinds, but I 
#think we fixed it by simply swapping port connections
def merge_layers_FH(xOrig,yOrig,zOrig,xa,ya,xb,yb,nLayers,oddLayer_th,evenLayer_th,copperWeight):
    i=0
    x=xa #Start first layer with forward spiral
    y=ya
    cuThickness=ozPerSqft_to_mil(copperWeight)
    zArrayList=generate_z_arrays(len(xa),oddLayer_th,evenLayer_th,nLayers,cuThickness,0,'EXPORT',1)
    z=zArrayList[0]
    i+=1
    while i < nLayers:
        if i % 2 == 0:
            #Even Layer
            x=np.hstack((x,xa))
            y=np.hstack((y,ya))
            z=np.hstack((z,zArrayList[i]))
        else:
            #Odd Layer
            x=np.hstack((x,np.fliplr([xb])[0]))
            y=np.hstack((y,np.fliplr([yb])[0]))
            z=np.hstack((z,zArrayList[i]))
        i+=1
    return x,y,z

'''
************************
* FasterCap Functions  *
************************
'''
#Generate Panel Vertices that define Hexahedron
def segs_to_hexa(xAout,yAout,xAin,yAin,xBout,yBout,xBin,yBin,scale):
    #for each new segment: 6 faces, 8 verts, 12 edges:Hexahedron
    #1: Generate Coil one Hexahedron List
    hexaListA = []
    hexaListB = []
    #List's first def is a pos n+1.
    i = 0
    #Scale incoming geometry
    xAout = scale * xAout
    yAout = scale * yAout
    xAin = scale * xAin
    yAin = scale * yAin
    xBout = scale * xBout
    yBout = scale * yBout
    xBin = scale * xBin
    yBin = scale * yBin
    while i < len(xAout)-1:
        aPoint = [xAin[i],yAin[i]]
        bPoint = [xAout[i],yAout[i]]
        cPoint = [xAout[i+1],yAout[i+1]]
        dPoint = [xAin[i+1],yAin[i+1]]
        hexaListA.append({'a':aPoint,'b':bPoint,'c':cPoint,'d':dPoint})
        i+=1
    i = 0
    while i < len(xBout)-1:
        aPoint = [xBin[i],yBin[i]]
        bPoint = [xBout[i],yBout[i]]
        cPoint = [xBout[i+1],yBout[i+1]]
        dPoint = [xBin[i+1],yBin[i+1]]
        hexaListB.append({'a':aPoint,'b':bPoint,'c':cPoint,'d':dPoint})
        i+=1
    return hexaListA,hexaListB

#Build up Panel Strings for QUI Files.
#Units in METERS
def buildPanel(xy_coords,z_elev,cu_Thickness,scale):
    #Scale incoming geometry
    #xy_coords = xy_coords * scale
    z_elev = z_elev * scale
    cu_Thickness = cu_Thickness * scale
    zTop = z_elev + cu_Thickness/2
    zBot = z_elev - cu_Thickness/2
    panelString = {}
    panelString['top'] = str(xy_coords['a'][0]) + " " + str(xy_coords['a'][1]) + " " + str(zTop) + "  " +str(xy_coords['b'][0]) + " " + str(xy_coords['b'][1]) + " " + str(zTop) + "  " + str(xy_coords['c'][0]) + " " + str(xy_coords['c'][1]) + " " + str(zTop) + "  " + str(xy_coords['d'][0]) + " " + str(xy_coords['d'][1]) + " " + str(zTop)
    panelString['bot'] = str(xy_coords['a'][0]) + " " + str(xy_coords['a'][1]) + " " + str(zBot) + "  " +str(xy_coords['b'][0]) + " " + str(xy_coords['b'][1]) + " " + str(zBot) + "  " + str(xy_coords['c'][0]) + " " + str(xy_coords['c'][1]) + " " + str(zBot) + "  " + str(xy_coords['d'][0]) + " " + str(xy_coords['d'][1]) + " " + str(zBot)
    panelString['out'] = str(xy_coords['c'][0]) + " " + str(xy_coords['c'][1]) + " " + str(zBot) + "  " +str(xy_coords['b'][0]) + " " + str(xy_coords['b'][1]) + " " + str(zBot) + "  " + str(xy_coords['b'][0]) + " " + str(xy_coords['b'][1]) + " " + str(zTop) + "  " + str(xy_coords['c'][0]) + " " + str(xy_coords['c'][1]) + " " + str(zTop)
    panelString['in'] = str(xy_coords['d'][0]) + " " + str(xy_coords['d'][1]) + " " + str(zBot) + "  " +str(xy_coords['a'][0]) + " " + str(xy_coords['a'][1]) + " " + str(zBot) +  "  " + str(xy_coords['a'][0]) + " " + str(xy_coords['a'][1]) + " " + str(zTop) + "  " + str(xy_coords['d'][0]) + " " + str(xy_coords['d'][1]) + " " + str(zTop)
    return panelString

#Generate the start Panel of Surface
def starterEnderPanel(xy_coords_list,z_elev,cu_Thickness,scale):
        #Scale incoming geometry
        #xy_coords_list = xy_coords_list * scale
        z_elev = z_elev * scale
        cu_Thickness = cu_Thickness * scale
        zTop = z_elev + cu_Thickness/2
        zBot = z_elev - cu_Thickness/2
        panel = {}
        panel['starter'] = str(xy_coords_list[0]['b'][0]) + " " + str(xy_coords_list[0]['b'][1]) + " " + str(zBot) + "  " + str(xy_coords_list[0]['b'][0]) + " " + str(xy_coords_list[0]['b'][1]) + " " + str(zTop) + "  " + str(xy_coords_list[0]['a'][0]) + " " + str(xy_coords_list[0]['a'][1]) + " " + str(zTop) + "  " + str(xy_coords_list[0]['a'][0]) + " " + str(xy_coords_list[0]['a'][1]) + " " + str(zBot)
        panel['ender'] = str(xy_coords_list[-1]['c'][0]) + " " + str(xy_coords_list[-1]['c'][1]) + " " + str(zBot) + "  " + str(xy_coords_list[-1]['c'][0]) + " " + str(xy_coords_list[-1]['c'][1]) + " " + str(zTop) + "  " + str(xy_coords_list[-1]['d'][0]) + " " + str(xy_coords_list[-1]['d'][1]) + " " + str(zTop) + "  " + str(xy_coords_list[-1]['d'][0]) + " " + str(xy_coords_list[-1]['d'][1]) + " " + str(zBot)
        return panel

#Build QUI Header Data
def quiHeader(fileBaseName,keyListMain, fileTag, pcbDict):
    keyListPCB = []
    pcbData = ''
    for key in pcbDict.keys():
            keyListPCB.append(key)
    for key in keyListPCB:
            pcbData += "*    " + key + " : " + str(pcbDict[key]) + "\n"
    fileStrDict = {}
    for key in keyListMain:
            fileStrDict[key] = ''
    for key in keyListMain:
        if fileTag == 'LST':
            fileStrDict[key] += "* MAIN LST FILE\n"
            fileStrDict[key] += "* "+ fileBaseName + ".lst generated "+genDateStamp()+"\n"
        else:
            fileStrDict[key] += "0 Comp " + key +" Surface Model\n"
            fileStrDict[key] += "* "+ fileBaseName +"_"+ key + "_" + fileTag + ".qui" + " generated "+genDateStamp()+"\n"
        fileStrDict[key] += "* Generated by Appliqué\n"
        fileStrDict[key] += "* Contact: brodym@uw.edu\n"
        fileStrDict[key] += "* PCB Data (mil):\n"
        fileStrDict[key] += pcbData
        if fileTag == 'LST':
            fileStrDict[key] += "\n\n* PCB Stackup Model:\n"
        else:
            fileStrDict[key] += "\n\n* Geometry Data:\n"
    return fileStrDict
        

#Build the Qui file contects for Top, Bottom, Outer and Inner Panels
def buildTBIOfileDict(panelGEO_list,fileBaseName,fileTag,pcbDict,copperThickness_m, scale):
    keyListPanel = []
    for key in (buildPanel(panelGEO_list[0],0,copperThickness_m, scale)).keys():
        keyListPanel.append(key)
    #Build each file contents simultaneously.
    fileStrDict = quiHeader(fileBaseName,keyListPanel, fileTag, pcbDict)
    i=0
    while i < len(panelGEO_list):
        #Build Surfaces
        panelString = buildPanel(panelGEO_list[i],0,copperThickness_m, scale)
        for key in keyListPanel:
            fileStrDict[key] += "\n\n* Index " + str(i) + "\n"
            fileStrDict[key] += "Q Ring" + fileTag + " " + str(panelString[key])+ "\n"
        i+=1
    return fileStrDict


def writeTBIOquiFiles(stringDict, fileWriteLoc, fileBaseName, fileTag):
    #Write Files
    keyList = []
    for key in stringDict.keys():
        keyList.append(key)
    for key in keyList:
        fullFileName = fileWriteLoc+"/"+fileBaseName+"_"+ key +"_" + fileTag + ".qui"
        try:
            with open(fullFileName,"w") as newFCQ:
                writeConsole('Writing File: ' + fullFileName, 'green')
                newFCQ.write(stringDict[key])
                newFCQ.close()
        except EnvironmentError:
            writeConsole('Aborting Export: Failed to open ' + fullFileName + ' for writing.','red')
            return
    return

def writeStarterEnderPanel(startEndPanel,fileWriteLoc, fileBaseName, fileTag, pcbDict):
    keyList = []
    for key in startEndPanel.keys():
        keyList.append(key)
    stringDict = quiHeader(fileBaseName,keyList, fileTag, pcbDict)
    for key in keyList:
        fullFileName = fileWriteLoc+"/"+fileBaseName+"_"+ key +"_" + fileTag + ".qui"
        try:
            with open(fullFileName,"w") as newFCQ:
                writeConsole('Writing File: ' + fullFileName, 'green')
                newFCQ.write(stringDict[key] + "\n")
                newFCQ.write("\n* " + key +" Panel\n")
                newFCQ.write("\nQ Ring" + fileTag + " " + startEndPanel[key] + "\n")
                newFCQ.close()
        except EnvironmentError:
                        writeConsole('Aborting Export: Failed to open ' + fullFileName + ' for writing.','red')
                        return       
    return

def writePerimPanels(panelDict, fileWriteLoc, fileBaseName, fileTag, pcbDict):
    stringDict = quiHeader(fileBaseName,['D'], fileTag, pcbDict)
    fullFileName = fileWriteLoc+"/"+fileBaseName+"_D_" + fileTag + ".qui"
    try:
        with open(fullFileName,"w") as newFCQ:
            writeConsole('Writing File: ' + fullFileName, 'green')
            newFCQ.write(stringDict['D'] + "\n")
            newFCQ.write("\n* " + fileTag +" Panel\n")
            newFCQ.write("\n\n* Perimeter Panels\n")
            for item in panelDict['Q1Q2']:
                newFCQ.write("Q OBJ" + fileTag + " " + str(item)+ "\n")
            for item in panelDict['Q2Q3']:
                newFCQ.write("Q OBJ" + fileTag + " " + str(item)+ "\n")
            for item in panelDict['Q3Q4']:
                newFCQ.write("Q OBJ" + fileTag + " " + str(item)+ "\n")
            for item in panelDict['Q4Q1']:
                newFCQ.write("Q OBJ" + fileTag + " " + str(item)+ "\n")
            newFCQ.close()
    except EnvironmentError:
        writeConsole('Aborting Export: Failed to open ' + fullFileName + ' for writing.','red')
        return
    return
    

'''
The following function needs editting to purge old realistic mode stuff'''
#Note this doesn't seem to work well in fasterCap
#Suggest making 3 Panels, each discretized well!
#Should we define discretization size? These can be reconfigured in LST file
#Adjust height up and down, including the copper and exluding.
#Beware of Top layer Z-axis ref point as it is outside the dielectric!!!!
#Use B layer if necessary.

#Option 1: Bury the upper and lower copper in dielectric.
#Option 2: Expose 3 sides to air, one to dielectric. <---Most accurate if we can get it working.
#Option 3: Expose 4 sides to air, with gap between copper ad dielectric

def genWritePCBpanels(pcb_Width, pcb_Height, odd_layer_separation, even_layer_separation, copper_thickness, number_layers, disc_X, disc_Y, disc_Z,
                        fileWriteLoc, fileBaseName, fileTag, pcbDict, scale, hexaFOR, hexaREV, zPCBBuffer):
    zOrigin = 0 
    zHeight = 0 
    odd_layer_separation = odd_layer_separation * scale
    even_layer_separation = even_layer_separation * scale
    copper_thickness = copper_thickness * scale
    evenNumLayers = False
    #For this model, we are simply summing the total height for all layers stacked.
    #Total height depends on if even or odd
    #If one layer, we will just assume it is a normal 2 layer build.
    if(number_layers == 1):
        number_layers = 2
    if(number_layers % 2 == 0):
        evenNumLayers = True
        zHeight = number_layers * copper_thickness + (number_layers/2) * odd_layer_separation + (number_layers/2 - 1) * even_layer_separation
    else:
        zHeight = number_layers * copper_thickness + ((number_layers-1)/2) * odd_layer_separation + ((number_layers - 1)/2) * even_layer_separation
    #IF WE ARE IN SIMPLE MODE, WE DO NOT NEED TO PERFORM TRIANGULATION
    #SIMPLY PRODUCE A SIMPLE XY PLANE!
    pcb_Width = pcb_Width * scale
    pcb_Height = pcb_Height * scale
    #Calculate the main corners of the large cuboid.
    a_X = pcb_Width/2
    a_Y = pcb_Height/2
    b_X = -pcb_Width/2
    b_Y = pcb_Height/2
    c_X = -pcb_Width/2
    c_Y = -pcb_Height/2
    d_X = pcb_Width/2
    d_Y = -pcb_Height/2
    z_Height = zHeight
    panelString = {}
    #Gen Top and Bottom and face first
    #Start in quad 1
    #Move in -x direction. Then proceed down -y
    xyZplusPlanePanel = [] #XY PLANE is Top and Bottom
    xyZminusPlanePanel = [] #XY PLANE is Top and Bottom
    yzXplusPlanePanel = []
    yzXminusPlanePanel = []
    xzYplusPlanePanel = []
    xzYminusPlanePanel = []
    deltaX = pcb_Width/disc_X
    deltaY = pcb_Height/disc_Y
    z_PCBBuffer = copper_thickness * zPCBBuffer
    a_Z = z_Height + z_PCBBuffer
    a_Zo = -z_PCBBuffer

    deltaZ = (a_Z-a_Zo)/disc_Z
    #XY
    for y_Index in range(disc_Y):
        a_Yprime = a_Y - deltaY * y_Index
        for x_Index in range(disc_X):
            #Start in most +x, +y corner.
            Q1 = (a_X - deltaX * (x_Index), a_Yprime)
            Q2 = (a_X - deltaX * (x_Index + 1), a_Yprime)
            Q3 = (a_X - deltaX * (x_Index + 1), a_Yprime - deltaY)
            Q4 = (a_X - deltaX * (x_Index), a_Yprime - deltaY)
            panelString_plus = str(Q1[0]) + " " + str(Q1[1]) + " " + str(a_Z) + "  " +str(Q2[0]) + " " + str(Q2[1]) + " " + str(a_Z) + "  " + str(Q3[0]) + " " + str(Q3[1]) + " " + str(a_Z) + "  " + str(Q4[0]) + " " + str(Q4[1]) + " " + str(a_Z)
            panelString_minus = str(Q1[0]) + " " + str(Q1[1]) + " " + str(a_Zo) + "  " +str(Q2[0]) + " " + str(Q2[1]) + " " + str(a_Zo) + "  " + str(Q3[0]) + " " + str(Q3[1]) + " " + str(a_Zo) + "  " + str(Q4[0]) + " " + str(Q4[1]) + " " + str(a_Zo)
            xyZplusPlanePanel.append(panelString_plus)
            xyZminusPlanePanel.append(panelString_minus)

    #YZ
    for y_Index in range(disc_Y):
        a_Yprime = a_Y - deltaY * y_Index
        for z_Index in range(disc_Z):
            #Start in most +x, +y corner.
            Q1 = (a_Yprime, a_Z - deltaZ * (z_Index))
            Q2 = (a_Yprime, a_Z - deltaZ * (z_Index+1))
            Q3 = (a_Yprime - deltaY, a_Z - deltaZ * (z_Index+1))
            Q4 = (a_Yprime - deltaY, a_Z - deltaZ * (z_Index))
            panelString_plus = str(a_X) + " " + str(Q1[0]) + " " + str(Q1[1]) + "  " +str(a_X) + " " + str(Q2[0]) + " " + str(Q2[1]) + "  " + str(a_X) + " " + str(Q3[0]) + " " + str(Q3[1]) + "  " + str(a_X) + " " + str(Q4[0]) + " " + str(Q4[1])
            panelString_minus = str(b_X) + " " + str(Q1[0]) + " " + str(Q1[1]) + "  " +str(b_X) + " " + str(Q2[0]) + " " + str(Q2[1]) + "  " + str(b_X) + " " + str(Q3[0]) + " " + str(Q3[1]) + "  " + str(b_X) + " " + str(Q4[0]) + " " + str(Q4[1])
            yzXplusPlanePanel.append(panelString_plus)
            yzXminusPlanePanel.append(panelString_minus)
    #XZ
    for x_Index in range(disc_X):
        a_Xprime = a_X - deltaX * x_Index
        for z_Index in range(disc_Z):
            #Start in most +x, +y corner.
            Q1 = (a_Xprime, a_Z - deltaZ * (z_Index))
            Q2 = (a_Xprime, a_Z - deltaZ * (z_Index + 1))
            Q3 = (a_Xprime - deltaX, a_Z - deltaZ * (z_Index + 1))
            Q4 = (a_Xprime - deltaX, a_Z - deltaZ * (z_Index))
            panelString_plus = str(Q1[0]) + " " + str(a_Y) + " " + str(Q1[1]) + "  " +str(Q2[0]) + " " + str(a_Y) + " " + str(Q2[1]) + "  " + str(Q3[0]) + " " + str(a_Y) + " " + str(Q3[1]) + "  " + str(Q4[0]) + " " + str(a_Y) + " " + str(Q4[1])
            panelString_minus = str(Q1[0]) + " " + str(c_Y) + " " + str(Q1[1]) + "  " +str(Q2[0]) + " " + str(c_Y) + " " + str(Q2[1]) + "  " + str(Q3[0]) + " " + str(c_Y) + " " + str(Q3[1]) + "  " + str(Q4[0]) + " " + str(c_Y) + " " + str(Q4[1])
            xzYplusPlanePanel.append(panelString_plus)
            xzYminusPlanePanel.append(panelString_minus)
    stringDict = quiHeader(fileBaseName,['PCB'], fileTag, pcbDict)
    fullFileName = fileWriteLoc+"/"+fileBaseName+"_PCB_" + fileTag + ".qui"
    try:
        with open(fullFileName,"w") as newFCPCB:
            writeConsole('Writing File: ' + fullFileName, 'green')
            newFCPCB.write(stringDict['PCB'] + "\n")
            newFCPCB.write("\n* " + fileTag +" Panel\n")
            newFCPCB.write("\n\n* PCB Dielectric\n")
            newFCPCB.write("\n\n* XY-Surfaces \n")
            for i, panelString in enumerate(xyZplusPlanePanel):
                newFCPCB.write("T PCB " + panelString + "\n")
            for i, panelString in enumerate(xyZminusPlanePanel):
                newFCPCB.write("T PCB " + panelString + "\n")
            newFCPCB.write("\n\n* YZ-Surfaces \n")
            for i, panelString in enumerate(yzXplusPlanePanel):
                newFCPCB.write("Q PCB " + panelString + "\n")
            for i, panelString in enumerate(yzXminusPlanePanel):
                newFCPCB.write("Q PCB " + panelString + "\n")
            newFCPCB.write("\n\n* XZ-Surfaces \n")
            for i, panelString in enumerate(xzYplusPlanePanel):
                newFCPCB.write("Q PCB " + panelString + "\n")
            for i, panelString in enumerate(xzYminusPlanePanel):
                newFCPCB.write("Q PCB " + panelString + "\n")
            newFCPCB.close()
    except EnvironmentError:
        writeConsole('Aborting Export: Failed to open ' + fullFileName + ' for writing.','red')
        return
    return

def genTraceNegative(tracePolyList, pcbXWidth, pcbYHeight, xDesc, yDesc):
    pcbXdelta = pcbXWidth/xDesc 
    pcbYdelta = pcbYHeight/yDesc
    xPCB = []
    yPCB = []
    pcbList = []
    for i in range(xDesc + 1):
        for j in range(yDesc + 1):
            xPCB.append(-pcbXWidth/2.0 + i*pcbXdelta)
            yPCB.append(-pcbYHeight/2.0 + j*pcbYdelta)
            pcbList.append((-pcbXWidth/2.0 + i*pcbXdelta, -pcbYHeight/2.0 + j*pcbYdelta))
    polyObj = Polygon(tracePolyList)
    scrubbedPCBList = []
    for coord in pcbList:
        point = Point(coord)
        if(not polyObj.contains(point)):
            scrubbedPCBList.append(coord)
    pointsCombined = MultiPoint(scrubbedPCBList + tracePolyList)
    combinedTriangles = triangulate(pointsCombined)
    x,y = polyObj.exterior.xy
    traceTriangles = MultiPolygon(triangulate(polyObj))
    negTriangles = []
    for poly in combinedTriangles:
        if not poly.centroid.intersects(polyObj):
            negTriangles.append(poly)
    negPCB_retList = []
    for polygon in negTriangles:
        nx, ny = polygon.exterior.xy
        nx = nx.tolist()[:-1]
        ny = ny.tolist()[:-1]
        negPCB_retList.append(list(zip(nx,ny)))
    return negPCB_retList

#Increase discretization
#This function adds additional points to the straight regions.
#a minimum dx and dy are specified. If the delta exceeds, the section has points added
def refineStraightSections(polyList):
    print("Refine")



if __name__== "__main__":
    print("This is a plug-in module for SSL PCB Coil Designer.\n")
    exit()
