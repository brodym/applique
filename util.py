'''
################################################################################################################
#                                                                                                              #
#  **********************************************                                                              #
#  * Appliqué: util.py                          *                                                              #
#  * By: Brody Mahoney                          *                                                              #
#  **********************************************                                                              #
#                                                                                                              #
#                                                                                                              #
#  Common functions shared between modules.                                                                    #
################################################################################################################
'''
import termcolor
import time
import datetime
import pickle
import numpy as np
import os
from svgpathtools import svg2paths

#This is currenty necesssry on for the polygonal loop generation
import config

'''
***************************
* Simple Helper Functions *
***************************
'''
        
def genDateStamp():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

def writeConsole(text,color):
    print(termcolor.colored("["+genDateStamp()+"]",'white')+termcolor.colored(text+"\n",color))


def text2ASCII(text):
    asciiText = ''
    for i, ch in enumerate(text):
        asciiText += str(ord(ch))
        if(i<len(text)-1):
            asciiText += ", "
    return asciiText

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

#Perform simple linear interpolation
def interpolate(x, xa, xb, ya, yb):
    x = float(x)
    xa = float(xa)
    xb = float(xb)
    ya = float(ya)
    yb = float(yb)
    y = ya + (x-xa)*(yb-ya)/(xb-xa)
    return y

def printResultsConsole(param_Dict):
    #This is a general purpose function.
    #It simply prints key and value, that's it.
    for key, value in param_Dict.items():
        print(key + " = " + str(value) + "\n")

def writeDictsToFile(dict_1, dict_2, freq, fileName):
    try:
        with open(fileName, "w") as dictsFile:
            for key, value in dict_1.items():
                dictsFile.write(key + " " + str(value) + "\n")
            for key, value in dict_2.items():
                dictsFile.write(key + " " + str(value) + "\n")
            if(freq > -1):
                dictsFile.write("FREQ " + str(freq) + "\n")
            dictsFile.close()
        writeConsole('Data written to ' + fileName,'green')
    except EnvironmentError:
        writeConsole('Error writing file!','red')
        return

def floatToWholeFracPart(inputNumber):
    wholePart = int(inputNumber)
    fracPart = inputNumber - wholePart
    return wholePart, fracPart
    
'''
******************
* File Functions *
******************
'''

#Save User Settings, in Pickle Dictionary, to file
def save_settings_to_file(settingsDict,fileSavLoc,filename):
    fullFileName = fileSavLoc + "/" + filename
    pickle.dump( settingsDict, open( fullFileName, "wb" ) )
    return
        
#Load Pickle file and return dictionary
def load_settings_from_file(filename):
    fullFileName = filename
    try:
        settingsDict = pickle.load( open( fullFileName, "rb" ) )
    except FileNotFoundError:
        writeConsole("Could not open file","red")
        settingsDict = None
    return settingsDict

def cleanDirectory(directoryPath,removeDir_Bool):
    writeConsole('Cleaning ' + directoryPath,'green')
    try:
        for file in os.listdir(directoryPath): 
            file_path = os.path.join(directoryPath, file)
            if os.path.isfile(file_path):
                writeConsole('Deleting: ' + file_path ,'green')
                try:
                    os.remove(file_path)
                except PermissionError:
                    writeConsole('Could not delete ' + file_path ,'yellow')
    except FileNotFoundError:
        writeConsole('Working directory not found.' ,'yellow')
        return
    if(removeDir_Bool):
        os.rmdir(directoryPath)
        writeConsole('Removing directory: ' + directoryPath ,'green')

def moveFile(oldFileName, newFileName):
    if(os.path.exists(newFileName)):
        ###Delete file first
        os.remove(newFileName)
    os.rename(oldFileName, newFileName)

def fileExists(fileName):
    if(os.path.exists(fileName)):
        return True
    else:
        return False

'''
*******************************
* Conversion Helper Functions *
*******************************
'''

def mil_to_m(num_mils):
    num_m = num_mils * 2.54e-5
    return num_m

def mil_to_mm(num_mils):
    num_mm = num_mils * 0.0254
    return num_mm

def ozPerSqft_to_mm(num_ozPerSqft):
    num_mm = num_ozPerSqft * 0.0347
    return num_mm

def ozPerSqft_to_mil(num_ozPerSqft):
    num_mil = num_ozPerSqft * 1.37
    return num_mil

'''
****************************
* GFX Generation Functions *
****************************
We should be able to easily expand SVG to panel or outline mode!
In mode == 1 we have double path. From these generated points we should
be able to call in_out_2_panels_GFX and in_out_2_outline_GFX. Note that those functions do both FWD and REV
at same time. importSVG does FWD and REV separately.
'''
#General purpose SVG import
#Based on https://stackoverflow.com/questions/43383249/how-do-i-get-a-coordinates-list-from-an-svgpathtools-bezier-curve answer
def importSVG(svgFileName,transXSVG,transYSVG,scaleSVG,flipHorzSVG,flipVertSVG,autoCenterSVG, sampPoints, traceWidth, mode):
    #mode 0: Single Path. For FastHenry2 and simple GFX
    #mode 1: Double Path. For FasterCap. Generate panels from single path with width equal to trace width.
    try:
        fullFileName = os.path.abspath(svgFileName)
        paths, attributes = svg2paths(fullFileName)
    except FileNotFoundError:
        print("File Not found")
        return [], []
    xCoords = np.empty(len(paths) * sampPoints - len(paths)+1)
    yCoords = np.empty(len(paths) * sampPoints - len(paths)+1)
    svgPath = []
    position = 0
    #Note, points will repeat every sampPoint.
    #Remove last entry if not last path?
    for pathLine in paths:
        for index in range(sampPoints):
            pathPoints = pathLine.point(index/(float(sampPoints)-1))
            #Check to make sure we are not repeating points
            if((position == 0) or (position > 0 and xCoords[position-1] != np.real(pathPoints) and yCoords[position-1] != np.imag(pathPoints))):
                np.put(xCoords, [position], np.real(pathPoints))
                np.put(yCoords, [position], np.imag(pathPoints))
                position += 1
    xCoords = xCoords * scaleSVG
    yCoords = yCoords * scaleSVG
    xCenter = 0.0
    yCenter = 0.0
    if(autoCenterSVG):
        xCenter = (np.amax(xCoords)+np.amin(xCoords))/2.0
        yCenter = (np.amax(yCoords)+np.amin(yCoords))/2.0
    xCoords = xCoords + transXSVG - xCenter
    yCoords = yCoords + transYSVG - yCenter
    if flipHorzSVG:
        xCoords = xCoords * -1
    if flipVertSVG:
        yCoords = yCoords * -1
    if(mode == 1):
        a = xCoords**2+yCoords**2
        b = -2*(xCoords**2+yCoords**2)
        c = xCoords**2+yCoords**2 - (traceWidth/2.0)**2
        xOutCoords = np.empty_like(xCoords)
        yOutCoords = np.empty_like(yCoords)
        xInCoords = np.empty_like(xCoords)
        yInCoords = np.empty_like(yCoords)
        for index in range(xCoords.size):
            solutions = np.roots([a[index],b[index],c[index]])
            np.put(xOutCoords, [index], xCoords[index] * solutions[0])
            np.put(yOutCoords, [index], yCoords[index] * solutions[0])
            np.put(xInCoords, [index], xCoords[index] * solutions[1])
            np.put(yInCoords, [index], yCoords[index] * solutions[1])
        return xOutCoords, xInCoords, yOutCoords, yInCoords
    return xCoords, yCoords

'''
Future Mods:
1. Consider renaming this function. single_tube_master is from the original version of this app where single_tube mode was the only
available graphic. Now we can do outline and panels.
2. FasterCap requires panelization. It would be great if we could consolidate this functionality
3. Expand for panel and outline view of SVG
'''
'''
n_hexaGen: Type of graphic generation.
0 = Single tube.
1 = Outline
2 = panel

4 = Used by FasterCap to create panels.
'''
def single_tube_master(n_tinnerwidth, n_tinnerheight, n_tinnercornerrad, n_twidth, n_tspacing, n_turns,
                n_dout, n_axialxo, n_axialyo, n_lineseg, n_straightLineSegs_X, n_straightLineSegs_Y, n_midSegs, b_export, 
                n_numlayers, n_coiltype, n_hexaGen, n_portLocation, n_portGap):
    #n_tinnerwidth, n_tinnerheight, n_tinnercornerrad are specific to rectangles
    #remaining args are for both types of coils.
    #Convert user dout to centerline dout. 
    n_dout = n_dout - (n_twidth - n_tspacing)/float(2)
    if n_coiltype == 'Circular':
        #Coil Standard
        #spaceRate needs to be modified to avoid div by zero error
        spaceRate = (n_twidth + n_tspacing)/float(2*np.pi)
        #Get arbitrarily close to zero, but avoid error!
        #This is important for single loop.
        #IN FUTURE CORRECT THIS WITH MODIFYING BEGINTURNS, ETC.
        if(abs(spaceRate) < 1e-12):
            #print("Too small")
            spaceRate = 5e-12
        beginTurns = ((n_dout/2)-(spaceRate*2*np.pi*n_turns))/(spaceRate*2*np.pi)
        rads = np.linspace(beginTurns*2*np.pi, (n_turns+beginTurns)*2*np.pi, n_lineseg)
        radsREV = rads[::-1]
        r = rads*spaceRate
        xOrigin = 0
        yOrigin = 0
        #Outline or Panel Mode
        if(n_hexaGen == 1 or n_hexaGen == 2):
            #Return combined inner outer X and Y
            #Xinner 0 to End. Append Xouter End to 0
            offset =n_twidth/2
            XA1,XA2,YA1,YA2 = coil_round_hexaGen(rads, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo,n_midSegs)
            XB1,XB2,YB1,YB2 = coil_round_hexaGen(radsREV, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo,n_midSegs)
            XB1 = XB1[::-1]
            XB2 = XB2[::-1]
            YB1 = YB1[::-1]
            YB2 = YB2[::-1]
            if(n_hexaGen == 2):
                XA, YA, XB, YB = in_out_2_panels_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2)
            if(n_hexaGen == 1):
                XA, YA, XB, YB = in_out_2_outline_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2)
            return XA, YA, XB, YB
            #This is for FasterCap Export. May be moved later
        elif(n_hexaGen == 4):
            offset =n_twidth/2
            XA1,XA2,YA1,YA2 = coil_round_hexaGen(rads, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo,n_midSegs)
            XB1,XB2,YB1,YB2 = coil_round_hexaGen(radsREV, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo,n_midSegs)
            XB1 = XB1[::-1]
            XB2 = XB2[::-1]
            YB1 = YB1[::-1]
            YB2 = YB2[::-1]
            return XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2
            #Note these points should be flipped since th
        else:
            offset = 0
            XA, YA = coil_round_segs(rads, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo)
            XB, YB = coil_round_segs(radsREV, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo)
            XB = XB[::-1]
            YB = YB[::-1]
    elif n_coiltype == 'Polygonal Loop':
        offset = n_twidth/2
        xOrigin = 0
        yOrigin = 0
        delTheta = 2*np.pi/config.line_segs_per_turn
        stTheta = -1*delTheta/2
        radsPoly = np.linspace(stTheta, 2*np.pi+stTheta, config.line_segs_per_turn+1)
        print(radsPoly)
        print("Port Location:" + str(n_portLocation))
        print("Port Gap:" + str(n_portGap))
        print("Radial to corner:" + str(n_dout))
        print("Delta Theta" + str(delTheta))
        print("Start Delta" + str(stTheta))
        XA1, XA2, YA1, YA2 = poly_loop_segs(radsPoly, n_dout, n_portLocation, n_portGap, xOrigin, yOrigin, offset)
        #XB1, XB2, YB1, YB2 = poly_loop_segs(radsPoly[::-1], n_dout, n_portLocation, n_portGap, xOrigin, yOrigin, offset)
        #XB1 = XB1[::-1]
        #XB2 = XB2[::-1]
        #YB1 = YB1[::-1]
        #YB2 = YB2[::-1]
        #The following line allows the program to be used until this function is complete
        if(n_hexaGen == 2):
            XA, YA, XB, YB = in_out_2_panels_GFX(XA1, XA2, YA1, YA2, XA1, XA2, YA1, YA2)
        if(n_hexaGen == 1):
            XA, YA, XB, YB = in_out_2_outline_GFX(XA1, XA2, YA1, YA2, XA1, XA2, YA1, YA2)
        return XA, YA, XA, YA
    elif n_coiltype == 'Rounded Rectangle':
        #debug
        if(n_hexaGen == 1 or n_hexaGen == 2 or n_hexaGen == 4):
            #Add n_straightLineSegs to generation.
            XA1,YA1,XB1,YB1=round_rect(n_tinnerwidth+n_twidth, n_tinnerheight+n_twidth, n_tinnercornerrad+n_twidth*.5, n_twidth, n_tspacing, n_turns, n_dout, n_axialxo, n_axialyo, n_lineseg, n_straightLineSegs_X, n_straightLineSegs_Y,n_midSegs)
            XA2,YA2,XB2,YB2=round_rect(n_tinnerwidth-n_twidth, n_tinnerheight-n_twidth, n_tinnercornerrad-n_twidth*.5, n_twidth, n_tspacing, n_turns, n_dout, n_axialxo, n_axialyo, n_lineseg, n_straightLineSegs_X, n_straightLineSegs_Y,n_midSegs)
            if(n_hexaGen == 4):
                return XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2
            elif(n_hexaGen == 2):
                return in_out_2_panels_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2)
            elif(n_hexaGen == 1):
                return in_out_2_outline_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2)
        else:
            XA, YA, XB, YB = round_rect(n_tinnerwidth, n_tinnerheight, n_tinnercornerrad, n_twidth, n_tspacing, n_turns, n_dout, n_axialxo, n_axialyo, n_lineseg, n_straightLineSegs_X, n_straightLineSegs_Y,n_midSegs)
    elif n_coiltype == 'Import SVG':
        #This will only happen when user switches to Import SVG and has not imported yet.
        dummyArray = np.zeros(1)
        return dummyArray, dummyArray, dummyArray, dummyArray
    return XA, YA, XB, YB

'''
This helper function is for generating the GFX panels from inner and outer arrays.
8 arrays in, 4 out.
'''

def in_out_2_panels_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2):
    XATemp = []
    YATemp = []
    XBTemp = []
    YBTemp = []
    for index, coord in enumerate(XA1):
        XATemp.append(XA1.item(index))
        XATemp.append(XA2.item(index))
        XATemp.append(XA1.item(index))
        YATemp.append(YA1.item(index))
        YATemp.append(YA2.item(index))
        YATemp.append(YA1.item(index))
        XBTemp.append(XB1.item(index))
        XBTemp.append(XB2.item(index))
        XBTemp.append(XB1.item(index))
        YBTemp.append(YB1.item(index))
        YBTemp.append(YB2.item(index))
        YBTemp.append(YB1.item(index))
    XA1 = np.array(XATemp)
    YA1 = np.array(YATemp)
    XB1 = np.array(XBTemp)
    YB1 = np.array(YBTemp)
    XA = np.concatenate((XA1,XA2[::-1]), axis=None)
    YA = np.concatenate((YA1,YA2[::-1]), axis=None)
    XB = np.concatenate((XB1,XB2[::-1]), axis=None)
    YB = np.concatenate((YB1,YB2[::-1]), axis=None)
    return XA, YA, XB, YB

'''
This helper function is for generating the GFX outline from inner and outer arrays.
8 arrays in, 4 out.
'''

def in_out_2_outline_GFX(XA1, XA2, YA1, YA2, XB1, XB2, YB1, YB2):
    XA = np.concatenate((XA1,XA2[::-1]), axis=None)
    YA = np.concatenate((YA1,YA2[::-1]), axis=None)
    XB = np.concatenate((XB1,XB2[::-1]), axis=None)
    YB = np.concatenate((YB1,YB2[::-1]), axis=None)
    #Lastly append XA[0], YA[0], etc. to end to close the outline
    XA = np.append(XA, XA.item(0))
    YA = np.append(YA, YA.item(0))
    XB = np.append(XB, XB.item(0))
    YB = np.append(YB, YB.item(0))
    return XA, YA, XB, YB

#Generate Arrays of line segments for polygonal loops
def poly_loop_segs(radsPoly, n_dout, n_portLocation, n_portGap, xOrigin, yOrigin, offset):
    XATemp = []
    YATemp = []
    XBTemp = []
    YBTemp = []
    for index,delTheta in enumerate(radsPoly):
        XATemp.append((n_dout/2 + offset)*np.cos(delTheta))
        YATemp.append((n_dout/2 + offset)*np.sin(delTheta))
        XBTemp.append((n_dout/2 - offset)*np.cos(delTheta))
        YBTemp.append((n_dout/2 - offset)*np.sin(delTheta))
    XA = np.asarray(XATemp)
    YA = np.asarray(YATemp)
    XB = np.asarray(XBTemp)
    YB = np.asarray(YBTemp)
    return XA, XB, YA, YB

#Generate Arrays of line Segments to produce Coils
#Make offset variable to include graphics for trace width!
def coil_round_hexaGen(rads, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo,n_midSegs):
    if(n_midSegs > 0):
        thisX1 = []
        thisY1 = []
        thisX2 = []
        thisY2 = []
        for index,delRad in enumerate(rads):
            thisX1.append((r[index] + offset)*np.cos(delRad-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin)
            thisY1.append((r[index] + offset)*np.sin(delRad-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin)
            thisX2.append((r[index] - offset)*np.cos(delRad-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin)
            thisY2.append((r[index] - offset)*np.sin(delRad-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin)
            #Now calculate all midpoints and append, unless this is the last iteration
            if(index < len(rads) - 1):
                lastX1 = thisX1[-1]
                lastY1 = thisY1[-1]
                lastX2 = thisX2[-1]
                lastY2 = thisY2[-1]
                nextX1 = (r[index+1] + offset)*np.cos(rads[index+1]-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin
                nextY1 = (r[index+1] + offset)*np.sin(rads[index+1]-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin
                nextX2 = (r[index+1] - offset)*np.cos(rads[index+1]-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin
                nextY2 = (r[index+1] - offset)*np.sin(rads[index+1]-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin
                delX1 = (nextX1 - lastX1)/(n_midSegs + 1)
                delY1 = (nextY1 - lastY1)/(n_midSegs + 1)
                delX2 = (nextX2 - lastX2)/(n_midSegs + 1)
                delY2 = (nextY2 - lastY2)/(n_midSegs + 1)
                for indexMid in range(n_midSegs):
                    midX1 = delX1 * (indexMid + 1) + lastX1
                    midY1 = delY1 * (indexMid + 1) + lastY1
                    midX2 = delX2 * (indexMid + 1) + lastX2
                    midY2 = delY2 * (indexMid + 1) + lastY2
                    thisX1.append(midX1)
                    thisY1.append(midY1)
                    thisX2.append(midX2)
                    thisY2.append(midY2)
        X1 = np.asarray(thisX1)
        Y1 = np.asarray(thisY1)
        X2 = np.asarray(thisX2)
        Y2 = np.asarray(thisY2)
    else:
        X1 = (r + offset)*np.cos(rads-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin
        Y1 = (r + offset)*np.sin(rads-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin
        X2 = (r - offset)*np.cos(rads-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin
        Y2 = (r - offset)*np.sin(rads-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin
    return X1,X2,Y1,Y2

def rads_inject_midpoints_circ(rads_array,n_midSegs,Xarr,Yarr):
    for delRad in rads_array:
        lastX = Xarr[-1]
        lastY = Yarr[-1]
        nextX = currRad*np.cos(delRad)+A_x
        nextY = currRad*np.sin(delRad)+A_y
        delX = (nextX - lastX)/(n_midSegs + 1)
        delY = (nextY - lastY)/(n_midSegs + 1)
        for index in range(n_midSegs):
                    midX = delX * (index + 1) + lastX
                    midY = delY * (index + 1) + lastY
                    Xarr = np.append(Xarr,midX)
                    Yarr = np.append(Yarr,midY)
        Xarr = np.append(Xarr,nextX)
        Yarr = np.append(Yarr,nextY)
    return Xarr, Yarr

#Generate Arrays of line Segments to produce Coils
#Make offset variable to include graphics for trace width!
def coil_round_segs(rads, r, beginTurns, spaceRate, xOrigin, yOrigin, offset, n_axialxo, n_axialyo):
    X1 = (r)*np.cos(rads-beginTurns*2*np.pi)+n_axialxo/2.0+xOrigin
    Y1 = (r)*np.sin(rads-beginTurns*2*np.pi)+n_axialyo/2.0+yOrigin
    return X1, Y1

#CLEAN UP SPACING/INDENTS on this
def round_rect(n_tinnerwidth, n_tinnerheight, n_tinnercornerrad, n_twidth, n_tspacing, n_turns, n_dout, n_axialxo, n_axialyo, n_lineseg, n_straightLineSegs_X, n_straightLineSegs_Y,n_midSegs):
    #Straight line segs will add more intermediate panels in the straight sections. 
    #Default is one long section!
    n_tspacing += n_twidth
    #!!Note linesegs for Rounded Rectangle will be fractional if not div by 4.
    n_lineseg = n_lineseg/4
    if(floatToWholeFracPart(n_lineseg)[1] > 0.0):
        writeConsole('Fractional Line Segs. Converting to int!' ,'yellow')
    n_lineseg = int(n_lineseg)
    #converting to int seems to work. Is it correct??
    #check
    anchor_X = n_tinnerwidth/2.0-n_tinnercornerrad
    anchor_Y = n_tinnerheight/2.0-n_tinnercornerrad
    A_x = -1.0*anchor_X
    A_y = -1.0*anchor_Y
    B_x = 1.0*anchor_X
    B_y = -1.0*anchor_Y
    C_x = 1.0*anchor_X
    C_y = 1.0*anchor_Y
    D_x = -1.0*(n_tinnerwidth/2.0+n_tspacing-n_tinnercornerrad)
    D_y = 1.0*anchor_Y
    currTurn = 0
    radsA = np.linspace(1.0*np.pi, 1.5*np.pi, n_lineseg)[1:]
    radsB = np.linspace(1.5*np.pi, 2.0*np.pi, n_lineseg)[1:]
    radsC = np.linspace(2.0*np.pi, 2.5*np.pi, n_lineseg)[1:]
    radsD = np.linspace(2.5*np.pi, 3.0*np.pi, n_lineseg)[1:]
    #Start at bottom of corner D
    XA1 = np.array([-1.0*n_tinnerwidth/2.0])
    YA1 = np.array([D_y])
    '''
    !!!!
    Improve speed.
    No need to calculate deltaAB etc on each iteration. The value never changes!
    Calculate it once here.
    '''
    while((n_turns-currTurn)>0):
        #Calculate radius of corner for current turn
        currRad = n_tinnercornerrad + currTurn * n_tspacing
        
        #Calculate Straight line points
        Aline_x = -1.0*n_tinnerwidth/2.0 - currTurn * n_tspacing
        Aline_y = A_y
        Bline_x = B_x
        Bline_y = -1.0*n_tinnerheight/2.0 - currTurn * n_tspacing
        Cline_x = 1.0*n_tinnerwidth/2.0 + currTurn * n_tspacing
        Cline_y = C_y
        Dline_x = D_x
        Dline_y = 1.0*n_tinnerheight/2.0 + currTurn * n_tspacing
        
        #Straight line to point A
        if(n_straightLineSegs_Y>1):
            deltaDA = (D_y - A_y)/n_straightLineSegs_Y
            for index in range(n_straightLineSegs_Y):
                if(index>0):
                    XA1=np.append(XA1, Aline_x)
                    YA1=np.append(YA1, D_y - index * deltaDA)
        XA1=np.append(XA1, Aline_x)
        YA1=np.append(YA1, Aline_y)

        #Corner A
        if(n_midSegs > 0):
            XA1, YA1 = rads_inject_midpoints_rr(radsA,n_midSegs,XA1,YA1,A_x,A_y,currRad)
        else:
            cornerXA1 = currRad*np.cos(radsA)+A_x
            cornerYA1 = currRad*np.sin(radsA)+A_y
            XA1=np.append(XA1,cornerXA1)
            YA1=np.append(YA1,cornerYA1)
        #Straight line to point B
        if(n_straightLineSegs_X>1):
            deltaAB = (B_x - A_x)/n_straightLineSegs_X
            for index in range(n_straightLineSegs_X):
                if(index>0):
                    XA1=np.append(XA1, A_x + index * deltaAB)
                    YA1=np.append(YA1, Bline_y)
        XA1=np.append(XA1, Bline_x)
        YA1=np.append(YA1, Bline_y)
        
        #Corner B
        if(n_midSegs > 0):
            XA1, YA1 = rads_inject_midpoints_rr(radsB,n_midSegs,XA1,YA1,B_x,B_y,currRad)
        else:
            cornerXA1 = currRad*np.cos(radsB)+B_x
            cornerYA1 = currRad*np.sin(radsB)+B_y
            XA1=np.append(XA1,cornerXA1)
            YA1=np.append(YA1,cornerYA1)
        #Straight line to point C
        if(n_straightLineSegs_Y>1):
            deltaBC = (C_y - B_y)/n_straightLineSegs_Y
            for index in range(n_straightLineSegs_Y):
                if(index>0):
                    XA1=np.append(XA1, Cline_x)
                    YA1=np.append(YA1, B_y + index * deltaBC)
        XA1=np.append(XA1, Cline_x)
        YA1=np.append(YA1, Cline_y)
            
        #Corner C
        if(n_midSegs > 0):
            XA1, YA1 = rads_inject_midpoints_rr(radsC,n_midSegs,XA1,YA1,C_x,C_y,currRad)
        else:
            cornerXA1 = currRad*np.cos(radsC)+C_x
            cornerYA1 = currRad*np.sin(radsC)+C_y
            XA1=np.append(XA1,cornerXA1)
            YA1=np.append(YA1,cornerYA1)
        #Straight line to point D
        if(n_straightLineSegs_X>1):
            deltaCD = (C_x - D_x)/n_straightLineSegs_X
            for index in range(n_straightLineSegs_X):
                if(index>0):
                    XA1=np.append(XA1, C_x - index * deltaCD)
                    YA1=np.append(YA1, Dline_y)
        XA1=np.append(XA1, Dline_x)
        YA1=np.append(YA1, Dline_y)
        
        #Corner D
        if(n_midSegs > 0):
            XA1, YA1 = rads_inject_midpoints_rr(radsD,n_midSegs,XA1,YA1,D_x,D_y,currRad)
        else:
            cornerXA1 = currRad*np.cos(radsD)+D_x
            cornerYA1 = currRad*np.sin(radsD)+D_y
            XA1=np.append(XA1,cornerXA1)
            YA1=np.append(YA1,cornerYA1)
        
        currTurn=currTurn+1
    #debugCoords(XA1, YA1)
    #Perform inverse winding for layers 2,4
    #If simple invert make 1=2 and 3=4 for wound in opposite direction
    XB1=XA1
    YB1=YA1
    currTurn=n_turns
    if(config.coil_RR_invert_Bool):
        print("Simple Invert")
    D_x = -1.0*(n_tinnerwidth/2.0-n_tspacing-n_tinnercornerrad)
    #Start at bottom of corner D
    XB1 = XA1[-1]
    YB1 = YA1[-1]
                
    while(currTurn>0):
        #Calculate radius of corner for current turn
        currRad = n_tinnercornerrad + currTurn * n_tspacing
        #Calculate Straight line points
        Aline_x = -1.0*n_tinnerwidth/2.0 - currTurn * n_tspacing
        Aline_y = A_y
        Bline_x = B_x
        Bline_y = -1.0*n_tinnerheight/2.0 - currTurn * n_tspacing
        Cline_x = 1.0*n_tinnerwidth/2.0 + currTurn * n_tspacing
        Cline_y = C_y
        Dline_x = D_x
        Dline_y = 1.0*n_tinnerheight/2.0 + currTurn * n_tspacing
        
        #Straight line to point A
        if(n_straightLineSegs_Y>1):
            deltaDA = (D_y - A_y)/n_straightLineSegs_Y
            for index in range(n_straightLineSegs_Y):
                if(index>0):
                    XB1=np.append(XB1, Aline_x)
                    YB1=np.append(YB1, D_y - index * deltaDA)
        XB1=np.append(XB1, Aline_x)
        YB1=np.append(YB1, Aline_y)
        
        #Corner A
        if(n_midSegs > 0):
            XB1, YB1 = rads_inject_midpoints_rr(radsA,n_midSegs,XB1,YB1,A_x,A_y,currRad)
        else:
            cornerXB1 = currRad*np.cos(radsA)+A_x
            cornerYB1 = currRad*np.sin(radsA)+A_y
            XB1=np.append(XB1,cornerXB1)
            YB1=np.append(YB1,cornerYB1)
        
        #Straight line to point B
        if(n_straightLineSegs_X>1):
            deltaAB = (B_x - A_x)/n_straightLineSegs_X
            for index in range(n_straightLineSegs_X):
                if(index>0):
                    XB1=np.append(XB1, A_x + index * deltaAB)
                    YB1=np.append(YB1, Bline_y)
        XB1=np.append(XB1, Bline_x)
        YB1=np.append(YB1, Bline_y)
        
        #Corner B
        if(n_midSegs > 0):
            XB1, YB1 = rads_inject_midpoints_rr(radsB,n_midSegs,XB1,YB1,B_x,B_y,currRad)
        else:
            cornerXB1 = currRad*np.cos(radsB)+B_x
            cornerYB1 = currRad*np.sin(radsB)+B_y
            XB1=np.append(XB1,cornerXB1)
            YB1=np.append(YB1,cornerYB1)
        
        #Straight line to point C
        if(n_straightLineSegs_Y>1):
            deltaBC = (C_y - B_y)/n_straightLineSegs_Y
            for index in range(n_straightLineSegs_Y):
                if(index>0):
                    XB1=np.append(XB1, Cline_x)
                    YB1=np.append(YB1, B_y + index * deltaBC)
        XB1=np.append(XB1, Cline_x)
        YB1=np.append(YB1, Cline_y)
        
        #Corner C
        if(n_midSegs > 0):
            XB1, YB1 = rads_inject_midpoints_rr(radsC,n_midSegs,XB1,YB1,C_x,C_y,currRad)
        else:
            cornerXB1 = currRad*np.cos(radsC)+C_x
            cornerYB1 = currRad*np.sin(radsC)+C_y
            XB1=np.append(XB1,cornerXB1)
            YB1=np.append(YB1,cornerYB1)
        
        #Straight line to point D
        if(n_straightLineSegs_X>1):
            deltaCD = (C_x - D_x)/n_straightLineSegs_X
            for index in range(n_straightLineSegs_X):
                if(index>0):
                    XB1=np.append(XB1, C_x - index * deltaCD)
                    YB1=np.append(YB1, Dline_y)
        XB1=np.append(XB1, Dline_x)
        YB1=np.append(YB1, Dline_y)
        
        #Corner D
        if(n_midSegs > 0):
            XB1, YB1 = rads_inject_midpoints_rr(radsD,n_midSegs,XB1,YB1,D_x,D_y,currRad)
        else:
            cornerXB1 = currRad*np.cos(radsD)+D_x
            cornerYB1 = currRad*np.sin(radsD)+D_y
            XB1=np.append(XB1,cornerXB1)
            YB1=np.append(YB1,cornerYB1)

        currTurn=currTurn-1
    #debugCoords(XB1, YB1)
    XA1 = XA1 + n_axialxo/2.0
    YA1 = YA1 + n_axialyo/2.0
    XB1 = XB1 - n_axialxo/2.0
    YB1 = YB1 - n_axialyo/2.0
    return XA1, YA1, XB1, YB1
    #return XA1, YA1, np.fliplr([XB1])[0], np.fliplr([YB1])[0]

def debugCoords(x_array, y_array):
    writeConsole("Coord Check","yellow")
    for i, x_coord in enumerate(x_array):
        xCoordRound_I = round(x_array[i], 4)
        yCoordRound_I = round(y_array[i], 4)
        #writeConsole("Check: (" + str(xCoordRound_I) + "," + str(yCoordRound_I) + ")","blue")
        for j, x_coord in enumerate(x_array):
            #SKIP i = j
            xCoordRound_J = round(x_array[j], 4)
            yCoordRound_J = round(y_array[j], 4)
            if(j > i):
                if(xCoordRound_I == xCoordRound_J and yCoordRound_I == yCoordRound_J):
                    writeConsole("Match: (" + str(xCoordRound_J) + "," + str(yCoordRound_J) + ")","red")
                    
def rads_inject_midpoints_rr(rads_array,n_midSegs,Xarr,Yarr,A_x,A_y,currRad):
    for delRad in rads_array:
        lastX = Xarr[-1]
        lastY = Yarr[-1]
        nextX = currRad*np.cos(delRad)+A_x
        nextY = currRad*np.sin(delRad)+A_y
        delX = (nextX - lastX)/(n_midSegs + 1)
        delY = (nextY - lastY)/(n_midSegs + 1)
        for index in range(n_midSegs):
                    midX = delX * (index + 1) + lastX
                    midY = delY * (index + 1) + lastY
                    Xarr = np.append(Xarr,midX)
                    Yarr = np.append(Yarr,midY)
        Xarr = np.append(Xarr,nextX)
        Yarr = np.append(Yarr,nextY)
    return Xarr, Yarr

def generate_z_arrays(array_len,odd_layer_sep,even_layer_sep,num_layers,copper_thickness,min_tube_radius,mode,scale):
    zArrayList = []
    if mode=='GFX':
        zOrigin=max(min_tube_radius,copper_thickness/2)
    else:
        zOrigin = 0
    #scale data
    odd_layer_sep = odd_layer_sep * scale
    even_layer_sep = even_layer_sep * scale
    copper_thickness = copper_thickness * scale
    i=0
    while i < num_layers:
        if(i==0):
            zArrayList.append(np.full(array_len,zOrigin))
        elif (num_layers>1 and (i % 2 == 0)):
            zArrayList.append(np.full(array_len,zOrigin+copper_thickness*i+(i/2*odd_layer_sep+i/2*even_layer_sep)))
        elif (num_layers>1 and (i % 2 != 0)):
            zArrayList.append(np.full(array_len,zOrigin+copper_thickness*i+((i+1)/2*odd_layer_sep+(i-1)/2*even_layer_sep)))
        i+=1
    return zArrayList

def getTotalPCBThickness(odd_layer_separation, even_layer_separation, copper_thickness, number_layers):
    thickness = -1
    #Total height depends on if even or odd
    #print(number_layers, copper_thickness, odd_layer_separation, even_layer_separation)
    if(number_layers % 2 == 0):
        thickness = number_layers * copper_thickness + (number_layers/2) * odd_layer_separation + (number_layers/2 - 1) * even_layer_separation
    else:
        thickness = number_layers * copper_thickness + ((number_layers-1)/2) * odd_layer_separation + ((number_layers - 1)/2) * even_layer_separation
    return thickness

def gen_PCBcuboid(pcb_Width, pcb_Height, odd_layer_separation, even_layer_separation, copper_thickness, number_layers):
    zOrigin = 0 
    zHeight = 0 
    #Treat single Layer like two layer
    if number_layers == 1:
        number_layers = 2
    #For this model, we are simply summing the total height for all layers stacked.
    #Total height depends on if even or odd
    if(number_layers % 2 == 0):
        zHeight = number_layers * copper_thickness + (number_layers/2) * odd_layer_separation + (number_layers/2 - 1) * even_layer_separation
    else:
        zHeight = number_layers * copper_thickness + ((number_layers-1)/2) * odd_layer_separation + ((number_layers - 1)/2) * even_layer_separation
    if(is_number(pcb_Width) and is_number(pcb_Height)):
        pcb_Width = float(pcb_Width)
        pcb_Height = float(pcb_Height)
    else:
        writeConsole("PCB Dimensions are not numerical","red")
        return None, None, None
    a_X = pcb_Width/2
    a_Y = pcb_Height/2
    b_X = -pcb_Width/2
    b_Y = pcb_Height/2
    c_X = -pcb_Width/2
    c_Y = -pcb_Height/2
    d_X = pcb_Width/2
    d_Y = -pcb_Height/2
    x_PCB = np.asarray([a_X,b_X,c_X,d_X,a_X,a_X,b_X,b_X,b_X,c_X,c_X,c_X,d_X,d_X,d_X,a_X])
    y_PCB = np.asarray([a_Y,b_Y,c_Y,d_Y,a_Y,a_Y,b_Y,b_Y,b_Y,c_Y,c_Y,c_Y,d_Y,d_Y,d_Y,a_Y])
    z_PCB = np.asarray([zOrigin,zOrigin,zOrigin,zOrigin,zOrigin,zHeight,zHeight,zOrigin,zHeight,zHeight,zOrigin,zHeight,zHeight,zOrigin,zHeight,zHeight])
    return x_PCB, y_PCB, z_PCB
