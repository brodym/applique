'''
config.py

Common global variables shared between modules
'''
#Const Globals
#PCB Manufacturer Specs Inits (units in mils)
copperWeight_init = '0.5'
pcbDielDF_init = '0.016'
pcbDielPerm_init = '4.4'
pcbWidth_init = '800'
pcbHeight_init = '800'
pcbConstruction_init = 'PrePreg-Core-PrePreg'
prepreg_thickness_init = 14.1 #Sunstone standard 4-layer Construction
core_thickness_init = 28.0 #Sunstone standard 4-layer Construction

#Coil Generation Inits (units in mils)
traceWidth_init = 40
traceSpacing_init = 41
diamOuter_init = 500
turns_init = 2
xOrigin=0
yOrigin=0
innerLayer=1.4
outerLayer=1.7
axialOffsetX_init=0
axialOffsetY_init=0
lineSegPerTurn_init = 10
midPointsSegs_init = 0
init_num_layers = 4
polyPortLocation_init = 0.0
polyPortGap_init = 0.0
lineSeg_init = lineSegPerTurn_init * turns_init + 1
coil_RR_invert_init = False

#Graphics Inits
min_tube_radius = 1.0
plotType_init = 'Panel'

#Rounded rectangle coil starting points (units in mils)
rectRadInnerCorn_init = 100
rectWidthInner_init = 400
rectHeightInner_init = 400
rectCoilStraightSegsX_init = 25
rectCoilStraightSegsY_init = 25


#Import SVG init
evenLayerSVG_init='even.svg'
evenTransXSVG_init = 0.0
evenTransYSVG_init = 0.0
evenScaleSVG_init = 1.0
evenFlipHorzSVG_init = False
evenFlipVertSVG_init = False
evenAutoCenterSVG_init = False

oddLayerSVG_init='odd.svg'
oddTransXSVG_init = 0.0
oddTransYSVG_init = 0.0
oddScaleSVG_init = 1.0
oddFlipHorzSVG_init = False
oddFlipVertSVG_init = False
oddAutoCenterSVG_init = False

lockEvenOddSVG_init=False
sampPointsSVG_init=10

#Altium Specific Export Parameters
exportFileName="PCB_altium_export.PcbDoc"
sheetWidth=1000
sheetHeight=1000
sheetX=1000
sheetY=1000

#Runtime Globals.
coil_type = 'Circular'
polyPortLocation = polyPortLocation_init
polyPortGap = polyPortGap_init
turns = turns_init
coil_RR_invert_Bool = coil_RR_invert_init
inner_corner_rad = rectRadInnerCorn_init
inner_coil_width = rectWidthInner_init
inner_coil_height = rectHeightInner_init
rectCoilStraightSegsX = rectCoilStraightSegsX_init
rectCoilStraightSegsY = rectCoilStraightSegsY_init
outer_diam = diamOuter_init
trace_spacing = traceSpacing_init
axial_offset_x = axialOffsetX_init
axial_offset_y = axialOffsetY_init
line_segments = lineSeg_init
line_segs_per_turn = lineSegPerTurn_init
midPointsSegs = midPointsSegs_init
numLayers = init_num_layers

copper_weight=copperWeight_init
trace_width=traceWidth_init
pcb_width=pcbWidth_init
pcb_height=pcbHeight_init
pcb_material='Sunstone FR-4'
pcb_diel_DF=pcbDielDF_init
pcb_diel_perm=pcbDielPerm_init
pcb_construction=pcbConstruction_init
prepreg_thickness=prepreg_thickness_init
core_thickness=core_thickness_init

evenLayerSVG=evenLayerSVG_init
oddLayerSVG=oddLayerSVG_init
sampPointsSVG=sampPointsSVG_init
lockEvenOddSVG=lockEvenOddSVG_init

min_tube_radius=1

odd_layer_separation = 0.0
even_layer_separation = 0.0
copper_thickness = 0.0


evenTransXSVG = evenTransXSVG_init
evenTransYSVG = evenTransYSVG_init
evenScaleSVG = evenScaleSVG_init
evenFlipHorzSVG = evenFlipHorzSVG_init
evenFlipVertSVG = evenFlipVertSVG_init
evenAutoCenterSVG = evenAutoCenterSVG_init
oddTransXSVG = oddTransXSVG_init
oddTransYSVG = oddTransYSVG_init
oddScaleSVG = oddScaleSVG_init
oddFlipHorzSVG = oddFlipHorzSVG_init
oddFlipVertSVG = oddFlipVertSVG_init
oddAutoCenterSVG = oddAutoCenterSVG_init

#autoModel
abort_flag = False
threadActiveApp_ID = -1
killAutoThread_bool = False
