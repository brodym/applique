'**********************************************************************
'* FasterCap Windows COM Interface                                    *
'* Brody Mahoney 7/2019                                               *
'*                                                                    *
'* This script provides an interface between the python GUI frontend  *
'* and FasterCap. FasterCap uses a COM interface for automation and   *
'* should function directly from Python with win32com module. While   *
'* FastHenry2 functions well by this manner, FasterCap seems to lock  *
'* up after being executed. Therefore this script serves as a kind of *
'* "patch" to allow usage of the FasterCap COM interface. Output is   *
'* simple text in a format that is easy for Python to parse into a    *
'* dictionary.                                                        *
'*                                                                    *
'* Windows command prompt:                                            *
'*        cscript //nologo FasterCap_COM_ifc.vbs <options>*           *
'*                                                                    *
'*  Options:                                                          *
'*      -fc FasterCap Input FileName expected as next arg.            *
'*      -s  FasterCap scaling factor. Cap will be divided by value.   *
'*      -p  Outputfile Prefix                                         *
'*      -ap Absolute path mode.                                       *
'*      -w  Show FasterCap window.                                    *
'*      -to Timeout limit.                                            *
'*                                                                    *
'**********************************************************************


Set args = Wscript.Arguments
Dim capInFile
Dim filePrefix
Dim apBool
Dim showWindowBool
Dim autoError
Dim timeOutBool
Dim timeOutTime
autoError = "0.01"
apBool = False
showWindowBool = False
timeOutBool = False
capScale = 1
filePrefix = ""

If args.count > 0 Then
    Dim i
    For i = 0 To args.count-1 Step 1
        'Wscript.Echo "Arg(" + CStr(i) + ")=" + args.Item(i)
        If args.Item(i) = "-fc" Then
            capBool = True
            On Error Resume Next
            capInFile = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: Missing Input File"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-a" Then
            On Error Resume Next
            autoError = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: Missing auto value"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-p" Then
            On Error Resume Next
            filePrefix = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: missing prefix"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-to" Then
            timeOutBool = True
            On Error Resume Next
            timeOutTime = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: Missing TimeOut Value"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-ap" Then
            apBool = True
        ElseIf args.Item(i) = "-w" Then
            showWindowBool = True
        ElseIf args.Item(i) = "-h" Then
            Call help
            Wscript.Quit
        End If
    Next
Else
    Wscript.Echo "!!Error: No options selected. Try -h option"
    Wscript.Quit
End If

'Set Path info. We will work in same directory
path = ""
If Not apBool Then
    pathPos = InstrRev(Wscript.ScriptFullName, Wscript.ScriptName)
    path = left(Wscript.ScriptFullName, pathPos-1)
End If
If filePrefix <> "" Then
   filePrefix = filePrefix + "_"
End If

Dim execTime
execTime = 0
Dim capSleepTime
capSleepTime = 1000
Dim FasterCap
Set FasterCap = CreateObject("FasterCap.Document")
If showWindowBool Then
    FasterCap.ShowWindow()
End If
Dim checkObj
checkObj = IsObject(FasterCap)
fasterCapProc = FasterCap.Run("""" + path + capInFile + """ -a" + autoError)
Do While FasterCap.IsRunning = True
    Wscript.Sleep capSleepTime
    execTime = execTime + capSleepTime
    If timeOutBool And execTime > (timeOutTime*1000) Then
        Set wshShell = CreateObject( "WScript.Shell" )
        WScript.Echo "Timeout at " + Cstr(execTime) + "ms"
        wshShell.Run("taskkill /im fastercap.exe /f")
        Set FasterCap = Nothing
        Set wshShell = Nothing
        Wscript.Quit
    End If
Loop
Dim maxCapName
Dim maxCap
Dim maxCondName
Dim maxCond 
Dim retStatus
maxCapName = "CM"
maxCap = FasterCap.GetCapacitance()
maxCondName = "GC"
maxCond = FasterCap.GetConductance()
retStatus = FasterCap.GetReturnStatus()
Call printParamDict(maxCapName, maxCap, maxCondName, maxCond, capScale, retStatus, execTime)
FasterCap.Quit
Set FasterCap = Nothing
Wscript.Quit
  

Sub printParamDict(aDictName, aDictMat, bDictName, bDictMat, scale, retStatus, execTime)
    Dim matDim, i , j
    matDim = uBound(aDictMat)
    If matDim > -1 Then
        Wscript.Echo vbCrLf
        'Print out Mats
        For i = 0 To matDim Step 1
            For j = 0 To matDim Step 1
                Wscript.Echo Cstr(aDictName) + Cstr(i+1) + Cstr(j+1) + " " + Cstr(aDictMat(i,j))
                Wscript.Echo Cstr(bDictName) + Cstr(i+1) + Cstr(j+1) + " " + Cstr(bDictMat(i,j))
            Next
        Next
    End If
    Wscript.Echo "DIM " & Cstr(matDim) & vbCr
    Wscript.Echo "EXIT " & Cstr(retStatus) & vbCr
    Wscript.Echo "TIME " & Cstr(execTime/1000) & vbCr
End Sub

Sub help()
    WScript.Echo "##FasterCap COM Interface Help##"
    WScript.Echo vbCrLf & "Options:"
    WScript.Echo VBTab & "-fc <""xxxx.lst""> | FasterCap enabled. Input FileName expected as next arg."
    WScript.Echo VBTab & "-ap              | Absolute Path Mode."
    WScript.Echo VBTab & "-a <Err Value>    | Auto iterate Error Value."
    WScript.Echo VBTab & "-w                | Show FasterCap Window."
    Wscript.Echo VBTab & "FasterCap Example: cscript .\henrycap_ifc.vbs -fc ""inductor.lst"" -ap -a 0.05"
End Sub
