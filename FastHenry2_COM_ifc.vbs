'**********************************************************************
'* FastHenry2 Windows COM Interface                                    *
'* Brody Mahoney 7/2019                                               *
'*                                                                    *
'* This script provides an interface between the python GUI frontend  *
'* and FastHenry2. FastHenry2 uses a COM interface for automation and *
'* should function directly from Python with win32com module. While   *
'* FastHenry2 functions well some machines with win32com, it failed   *
'* function on others. For more reliable operation this script serves *
'* as a kind of "patch" to allow usage of FastHenry2 COM in Python.   *
'* The output is simple text, formatted to quickly parse into a       *
'* Python dictionary.
'*                                                                    *
'* Windows command prompt:                                            *
'*        cscript //nologo FastHenry2_COM_ifc.vbs <options>           *
'*                                                                    *
'*  Options:                                                          *
'*      -fh FastHenry2 input FileName expected as next arg.           *
'*      -p  Outputfile Prefix                                         *
'*      -ap Absolute path mode.                                       *
'*      -to Timeout limit.                                            *
'*                                                                    *
'**********************************************************************


Set args = Wscript.Arguments
Dim henryInFile
Dim filePrefix
Dim apBool
Dim autoError
Dim timeOutBool
Dim timeOutTime
filePrefix = ""

If args.count > 0 Then
    Dim i
    For i = 0 To args.count-1 Step 1
        'Wscript.Echo "Arg(" + CStr(i) + ")=" + args.Item(i)
        If args.Item(i) = "-fh" Then
            capBool = True
            On Error Resume Next
            henryInFile = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: Missing Input File"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-p" Then
            On Error Resume Next
            filePrefix = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: missing prefix"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-to" Then
            timeOutBool = True
            On Error Resume Next
            timeOutTime = args.Item(i+1)
            If Err.Number <> 0 Then
                If Err.Number = 9 Then
                    Wscript.Echo "!!Error: Missing TimeOut Value"
                Else
                    Wscript.Echo "!!Error"
                End If
                Wscript.Quit
            End If
        ElseIf args.Item(i) = "-ap" Then
            apBool = True
        ElseIf args.Item(i) = "-h" Then
            Call help
            Wscript.Quit
        End If
    Next
Else
    Wscript.Echo "!!Error: No options selected. Try -h option"
    Wscript.Quit
End If

'Set Path info. We will work in same directory
path = ""
If Not apBool Then
    pathPos = InstrRev(Wscript.ScriptFullName, Wscript.ScriptName)
    path = left(Wscript.ScriptFullName, pathPos-1)
End If
If filePrefix <> "" Then
   filePrefix = filePrefix + "_"
End If

Dim execTime
execTime = 0
Dim henrySleepTime
henrySleepTime = 1000
Dim FastHenry2
Set FastHenry2 = CreateObject("FastHenry2.Document")

'fasterCapProc = FasterCap.Run("""" + path + capInFile + """ -a0.01")
fastHenry2Proc = FastHenry2.Run("""" + path + henryInFile + """")
Do While FastHenry2.IsRunning = True
    Wscript.Sleep henrySleepTime
    execTime = execTime + henrySleepTime
    If timeOutBool And execTime > (timeOutTime*1000) Then
        Set wshShell = CreateObject( "WScript.Shell" )
        WScript.Echo "Timeout at " + Cstr(execTime) + "ms"
        wshShell.Run("taskkill /im fasthenry2.exe /f")
        Set FastHenry2 = Nothing
        Set wshShell = Nothing
        Wscript.Quit
    End If
Loop
Dim indMatName
Dim indMat
Dim resMatName
Dim resMat 
Dim expDim
Dim freq
matDim = 2
indMatName = "L"
indMat = FastHenry2.GetInductance()
resMatName = "R"
resMat = FastHenry2.GetResistance()
freq = FastHenry2.GetFrequencies()
Call printParamDict(indMatName, indMat, resMatName, resMat, 0, expDim, execTime, freq(0))
FastHenry2.Quit
Set FasterCap = Nothing
Wscript.Quit

Sub printParamDict(aDictName, aDictMat, bDictName, bDictMat, retStatus, expDim, execTime, freq)
    Dim matDim, i , j
    matDim = uBound(aDictMat,2)
    If matDim > -1 Then
        Wscript.Echo vbCrLf
        'Print out Mats
        For i = 0 To matDim Step 1
            For j = 0 To matDim Step 1
                Dim tempParamA
                Dim tempParamB
                If expDim = 1 Then
                    'FasterCap
                    tempParamA = aDictMat(i,j)
                    tempParamB = bDictMat(i,j)
                Else
                    'FastHenry2
                    tempParamA = aDictMat(0,i,j)
                    tempParamB = bDictMat(0,i,j)
                End If
                Wscript.Echo Cstr(aDictName) + Cstr(i+1) + Cstr(j+1) + " " + Cstr(tempParamA)
                Wscript.Echo Cstr(bDictName) + Cstr(i+1) + Cstr(j+1) + " " + Cstr(tempParamB)
            Next
        Next
    End If
    Wscript.Echo "DIM " & Cstr(matDim) & vbCr
    If expDim = 1 Then
        'FasterCap
        Wscript.Echo "EXIT " & Cstr(retStatus) & vbCr
    Else
        'Print frequency
        Wscript.Echo "FREQ " & Cstr(freq) & vbCr
    End If
    Wscript.Echo "TIME " & Cstr(execTime/1000) & vbCr
End Sub

Sub help()
    WScript.Echo "##FastHenry2 COM Interface Help##"
    WScript.Echo vbCrLf & "Options:"
    WScript.Echo VBTab & "-fh <""xxxx.inp""> | Fasthenry input FileName expected as next arg."
    WScript.Echo VBTab & "-ap              | Absolute Path Mode."
    Wscript.Echo VBTab & "FastHenry2 Example: cscript .\FastHenry2_COM_ifc.vbs -fh ""fasthenry.inp"""
End Sub


