'''
################################################################################################################
#                                                                                                              #
#  **********************************************                                                              #
#  * Appliqué                                   *                                                              #
#  * By: Brody Mahoney                          *                                                              #
#  **********************************************                                                              #
#                                                                                                              #
# Requirements:                                                                                                #
#  1. Python v3.6.x or higher                                                                                  #
#  2. FastFieldSolves from https://www.fastfieldsolvers.com/. Includes FastHenry2 and FasterCap                #
#  3. NgSpice and or LTSpice                                                                                   #
#  4. Windows 64-bit                                                                                           #
#                                                                                                              #
#  Required Python modules:                                                                                    #
#    1.  NumPy                                                                                                 #
#    2.  MatplotLib                                                                                            #
#    3.  Sys                                                                                                   #
#    4.  Subprocess                                                                                            #
#    5.  Time                                                                                                  #
#    6.  Traits                                                                                                #
#    7.  MayaVi                                                                                                #
#    8.  PyFace                                                                                                #
#    9.  Colorama                                                                                              #
#    10. OS                                                                                                    #
#    11. ImportLib                                                                                             #
#        Note: Plug-in modules may require additional Python modules                                           #
#                                                                                                              #
#  Associated Files:                                                                                           #
#    1.  applique.py                 Main application. Core graphics and front-end                             #
#    2.  config.py                   Shared variables between Python modules                                   #
#    3.  util.py                     Common/shared functions                                                   #
#    4.  css.py                      Application styling                                                       #
#    5.  FasterCap_COM_ifc.vbs       VBScript coupling this Python App to FasterCap                            #
#    6.  FastHenry2_COM_ifc.vbs      VBScript coupling this Python App to FastHenry2                           #
#    7.  module_init.py              Automatically loads external modules                                      #
#   External Modules:                                                                                          #
#    1. module_simulation.py         External plug-in module that runs                                         #
#    2. module_altium.py             External Altium PCB File export                                           #
################################################################################################################
'''
#Import minimal so console messages can be displayed
try:
    import colorama
    import termcolor
    colorama.init()
except ModuleNotFoundError:
    print("Colorama or termcolor not installed\n")
    exit()
titleStr = """
*******************************************
* Appliqué : PCB Planar Inductor Designer *
*******************************************"""
print(termcolor.colored(titleStr,'cyan'))
#import shared globals and common functions
print(termcolor.colored('Loading core modules...\n','green'))
try:
    import config
    from util import *
    writeConsole('Shared variables & functions [OK]','green')
except ModuleNotFoundError:
    writeConsole('Shared variables & functions [Failed]','red')
    exit()
#Load each core module
try:
    import numpy as np
    writeConsole('Numpy [OK]','green')
except ModuleNotFoundError:
    writeConsole('Numpy [Failed]','red')
    exit()
try:
    import os
    import sys
    writeConsole('OS & Sys [OK]','green')
except ModuleNotFoundError:
    writeConsole('OS & Sys [Failed]','red')
    exit()
try:
    from traits.api import HasTraits, Range, Instance, on_trait_change, String, Button, Enum, Bool, List, HTML
    from traitsui.api import View, Item, Group, Include, HSplit, DirectoryEditor, FileEditor, HTMLEditor
    writeConsole('Traits [OK]','green')
except ModuleNotFoundError:
    writeConsole('Traits [Failed]','red')
    exit()
try:
    from mayavi.core.api import PipelineBase
    from mayavi.core.ui.api import MayaviScene, SceneEditor, MlabSceneModel
    writeConsole('MayaVi [OK]','green')
except ModuleNotFoundError:
    writeConsole('MayaVi [Failed]','red')
    exit()
try:
    from pyface.image_resource import ImageResource
    writeConsole('PyFace [OK]','green')
except ModuleNotFoundError:
    writeConsole('PyFace [Failed]','red')
    exit()
try:
    import importlib
    writeConsole('ImportLib [OK]','green')
except ModuleNotFoundError:
    writeConsole('ImportLib [Failed]','red')
    exit()
#Import Styles
try:
    from css import *
    writeConsole('Styles [OK]','green')
except ModuleNotFoundError:
    writeConsole('Styles [Failed]','red')
    exit()
#Import Modules Dynamically
#The module_init.py contains a list of modules to be loaded
writeConsole('Loading plug-in modules...','green')
import module_init
moduleList = []
for i, mod_name in enumerate(module_init.moduleID):
    try:
        moduleList.append(importlib.import_module(mod_name))
        writeConsole('Plug-in: '+str(moduleList[i].sslLabel)+' [OK]','green')
    except ImportError as err:
        print("Error:", err)

'''
************************
*Constructors for tabs.*
************************
'''
class helpPanel(HasTraits):
    helpTextHTML = '''
<html>
<body>
    <h1>Appliqué</h1>
    <h2>PCB Planar Inductor Designer</h2>
    <hr/>
    <h3>Instructions may be found at bitbucket</h3>
    <i>Contact: <a href = "mailto: brodym@uw.edu">brodym@uw.edu</a></i>
    <br/>
    (Under Construction)
</body>
</html>
'''
    helpDisplayHTML = HTML(helpTextHTML)
    helpView = View(
                    Item('helpDisplayHTML', editor = HTMLEditor, show_label=False),
                    width=400
                    )

class GraphicsPanel(HasTraits):
    plotTypeValues = List(['Single Tube', 'Outline','Panel'])
    plotType = Enum(values='plotTypeValues')
    min_tube_radius = Range(1,10,1)
    showPCB_Bool = Bool(True)
    #Items in GraphicsList will be saved.
    GraphicsList = ['plotType','min_tube_radius','showPCB_Bool']
    graphicsView = View(
                        Item('plotType',label='Trace Section Representation'),
                        Item('min_tube_radius',label='3D Line Thickness'),
                        Item('showPCB_Bool', label='Show PCB Outline')
                        )
    @on_trait_change('min_tube_radius')
    def minTubeRadiusHandler(self):
        config.min_tube_radius=self.min_tube_radius

class filePanel(HasTraits):
        #Use pickle to save and load settings
        saveButton = Button('Save Settings')
        loadButton = Button('Load Settings')
        saveLoc = "."
        saveFileName = "settings.apq"
        loadFileName = "./" + saveFileName
        graphicsView = View(
                                Item('saveLoc',editor=DirectoryEditor(),label='Save File Location'),
                                Item('saveFileName', label='Load/Save File Name'),
                                Item('saveButton',label='Save all user settings'),
                                Item('loadFileName',editor=FileEditor(entries=10, filter=['Appliqué Settings File *.apq', 'All files *.*']),label='Load User Settings File'),
                                Item('loadButton',label='Load previous user settings')
                        )
        
class CoilPanel(HasTraits):
        #FIX_7_20 Fix Loop. Allow Triangle and Hexagonal and square single coil.
        #For the last three simply lock the side per turn to 3, 6 & 4.
        #Later add a tessalation function that allows a coil design to be tesselated!
        #Tessalation Function must allow spacing per coil.
        #Question: How to effective measure multi layer, tessalated coils?????? Inductance only easy. Could treat same way.
        #First iteration does not allow complicated SRF simulation with tessalation. INDUCTANCE measurements would be very beneficial.
        #Perform SINGLE LUMP analysis only.
        coil_type = Enum('Circular','Polygonal Loop','Rounded Rectangle','Import SVG')
        #Poly Loop settings
        ##Because loop ends onto itself, there must be a split in the loop for proper operation.
        ##The port defines the location of the port in terms of angular position. 0<=theta<360
        ##The port gap defines how wide the port is.
        poly_port_location = Range(0.0, 360.0, config.polyPortLocation_init)
        poly_port_gap = Range(0.0, 360.0, config.polyPortGap_init)
        turns = Range(1, 100, config.turns_init)
        inner_corner_rad = Range (0.0, 10000.0, config.rectRadInnerCorn_init)
        inner_coil_width = Range (0.0, 10000.0, config.rectWidthInner_init)
        inner_coil_height = Range (0.0, 10000.0, config.rectHeightInner_init)
        rect_coil_straight_section_segs_X = Range (1, 1000, config.rectCoilStraightSegsY_init)
        rect_coil_straight_section_segs_Y = Range (1, 1000, config.rectCoilStraightSegsY_init)
        outer_diam = Range(1.0, 100000.0, config.diamOuter_init)
        stadium_Coil_Bool = Bool(False)
        coil_RR_invert_Bool = Bool(False)
        trace_spacing = Range(0.01, 1000.0, config.traceSpacing_init)
        axial_offset_x = Range(-1000.0, 1000.0, config.axialOffsetX_init)
        axial_offset_y = Range(-1000.0, 1000.0, config.axialOffsetY_init)
        #line_segments = (segs/turn * turn) + 1
        line_segments = Range(1, 10000, config.lineSeg_init)
        line_segs_per_turn = Range(3, 10000, config.lineSegPerTurn_init)
        midPoints_segments = Range(0, 10000, config.midPointsSegs_init)
        number_layers = Range(1,20, config.init_num_layers)
        #Import SVG items
        even_transXSVG = Range(-1000.0, 1000.0, config.evenTransXSVG_init)
        even_transYSVG = Range(-1000.0, 1000.0, config.evenTransYSVG_init)
        odd_transXSVG = Range(-1000.0, 1000.0, config.oddTransXSVG_init)
        odd_transYSVG = Range(-1000.0, 1000.0, config.oddTransYSVG_init)
        even_scaleSVG = Range(0.01, 100.0, config.evenScaleSVG_init)
        odd_scaleSVG = Range(0.01, 100.0, config.oddScaleSVG_init)
        even_flipHorzSVG = Bool(config.evenFlipHorzSVG_init)
        even_flipVertSVG = Bool(config.evenFlipVertSVG_init)
        odd_flipHorzSVG = Bool(config.oddFlipHorzSVG_init)
        odd_flipVertSVG = Bool(config.oddFlipVertSVG_init)
        even_autoCenterSVG = Bool(config.evenAutoCenterSVG_init)
        odd_autoCenterSVG = Bool(config.oddAutoCenterSVG_init)
        lockEvenOddSVG = Bool(config.lockEvenOddSVG_init)
        evenLayerSVG = String("./even.svg")
        oddLayerSVG = String("./odd.svg")
        sampPointsSVG = Range(1, 1000, config.sampPointsSVG_init)
        importFilesSVG = Button("Import")
        svgImportOK = Bool(False)
        #Items in CoilList will be saved.
        CoilList = ['coil_type','poly_port_location','poly_port_gap','turns','inner_corner_rad','inner_coil_width','inner_coil_height','rect_coil_straight_section_segs_X','rect_coil_straight_section_segs_Y',
            'outer_diam','stadium_Coil_Bool','trace_spacing','axial_offset_x','axial_offset_y','line_segments','line_segs_per_turn','midPoints_segments','number_layers',
            'even_transXSVG','even_transYSVG','odd_transXSVG','odd_transYSVG','even_scaleSVG','odd_scaleSVG','even_flipHorzSVG','even_flipVertSVG','odd_flipHorzSVG',
            'odd_flipVertSVG','even_autoCenterSVG','odd_autoCenterSVG','lockEvenOddSVG','evenLayerSVG','oddLayerSVG','sampPointsSVG','coil_RR_invert_Bool']
        pcbInfoView = View(                     Item('coil_type', label='Shape of Coil'),
                                                Group(
                                                    Item('poly_port_location', label='Polygon Port Location (degrees)'),
                                                    Item('poly_port_gap', label='Polygon Port Gap (degrees)'),
                                                    visible_when='coil_type==\'Polygonal Loop\'',
                                                ),
                                                
                                                Group(
                                                    Item('outer_diam', label='Coil Outer Diameter'),
                                                    visible_when='coil_type == "Circular"',
                                                    ),
                                                Group(
                                                    Item('stadium_Coil_Bool', label='Stadium Coil', tooltip='Lock width to form a stadium-like coil'),
                                                    Item('coil_RR_invert_Bool', label='Simple Invert', tooltip='Simple coil invert/rewind'),
                                                    Item('inner_corner_rad', label='Rect. Inner Corner Radius'),
                                                    Item('inner_coil_width', label='Rect. Width Inner Coil', visible_when='not stadium_Coil_Bool'),
                                                    Item('inner_coil_height', label='Rect. Height Inner Coil'),
                                                    Item('rect_coil_straight_section_segs_X', label='X: Number of segments in straight sections'),
                                                    Item('rect_coil_straight_section_segs_Y', label='Y: Number of segments in straight sections'),
                                                    visible_when='coil_type == "Rounded Rectangle"',
                                                    ),
                                                Group(
                                                    Item('turns', label='Number of coil turns per layer',visible_when='coil_type!=\'Polygonal Loop\''),
                                                    Item('trace_spacing', label='Trace-to-trace Spacing', visible_when='coil_type!=\'Polygonal Loop\''),
                                                    Item('line_segs_per_turn', label='Number of line segments per turn'),
                                                    Item('midPoints_segments', label='Intermediate Mid-Points', tooltip='Increase number of segments between primary geometry points'),
                                                    Item('axial_offset_x', label='X Axis Offset'),
                                                    Item('axial_offset_y', label='Y Axis Offset'),
                                                    visible_when='coil_type != "Import SVG"',
                                                ),
                                                Item('number_layers', label='Number of Layers'),
                                                Group(
                                                    Group(
                                                        Item('evenLayerSVG',editor=FileEditor(entries=10, filter=['SVG Files *.svg', 'All files *.*']),label='Even Layer SVG'),
                                                        Item('even_transXSVG', label='Translate on x-axis'),
                                                        Item('even_transYSVG', label='Translate on y-axis'),
                                                        Item('even_flipHorzSVG', label='Mirror across y-axis'),
                                                        Item('even_flipVertSVG', label='Mirror across x-axis'),
                                                        Item('even_autoCenterSVG', label='Auto Center'),
                                                        Item('even_scaleSVG', label='Scale'),
                                                        label = 'Even Layers', show_border = True
                                                        ),
                                                    Group(
                                                        Item('oddLayerSVG',editor=FileEditor(entries=10, filter=['SVG Files *.svg', 'All files *.*']),label='Odd Layer SVG'),
                                                        Item('odd_transXSVG', label='Translate on x-axis'),
                                                        Item('odd_transYSVG', label='Translate on y-axis'),
                                                        Item('odd_flipHorzSVG', label='Mirror across y-axis'),
                                                        Item('odd_flipVertSVG', label='Mirror across x-axis'),
                                                        Item('odd_autoCenterSVG', label='Auto Center'),
                                                        Item('odd_scaleSVG', label='Scale'),
                                                        label = 'Odd Layers', show_border = True
                                                        ),
                                                    #Item('lockEvenOddSVG', label='Lock even and odd layers'),
                                                    Item('sampPointsSVG', label='Sampling Points per Path'),
                                                    Item('importFilesSVG', show_label = False),
                                                    visible_when='coil_type == "Import SVG"',
                                                    ),
                                                )
        #The following simply makes all variables globally available across files.
        #Could just rewrite all variables everytime a change.
        @on_trait_change('coil_type')
        def coilTypeHandler(self):
            config.coil_type=self.coil_type
            self.svgImportOK = False
        @on_trait_change('turns')
        def turnsHandler(self):
            config.turns=self.turns
            self.lineSegsPerTurnHandler()
        @on_trait_change('inner_corner_rad')
        def innerCornerRadHandler(self):
            config.inner_corner_rad=self.inner_corner_rad
        @on_trait_change('inner_coil_width')
        def innerCoilWidthHandler(self):
            if(not self.stadium_Coil_Bool):
                #We should lock this control!
                config.inner_coil_width=self.inner_coil_width
        @on_trait_change('inner_coil_height')
        def innerCoilHeightHandler(self):
            config.inner_coil_height=self.inner_coil_height
        @on_trait_change('rect_coil_straight_section_segs_X')
        def rectCoilStraightSegsXHandler(self):
            config.rectCoilStraightSegsX = self.rect_coil_straight_section_segs_X
        @on_trait_change('rect_coil_straight_section_segs_Y')
        def rectCoilStraightSegsYHandler(self):
            config.rectCoilStraightSegsY = self.rect_coil_straight_section_segs_Y
        @on_trait_change('midPoints_segments')
        def midPointsSegsHandler(self):
            config.midPointsSegs = self.midPoints_segments
        @on_trait_change('outer_diam')
        def outerDiamHandler(self):
            config.outer_diam=self.outer_diam
        @on_trait_change('trace_spacing')
        def traceSpacingHandler(self):
            config.trace_spacing=self.trace_spacing
            if(self.stadiumCoilBoolHandler):
                self.stadiumCoilBoolHandler()
        @on_trait_change('axial_offset_x')
        def axialOffsetXHandler(self):
            config.axial_offset_x=self.axial_offset_x
        @on_trait_change('axial_offset_y')
        def axialOffsetYHandler(self):
            config.axial_offset_y=self.axial_offset_y
        @on_trait_change('line_segs_per_turn')
        def lineSegsPerTurnHandler(self):
            config.line_segs_per_turn=self.line_segs_per_turn
            self.line_segments=(int(self.line_segs_per_turn)*int(self.turns))+1
        @on_trait_change('line_segments')
        def lineSegmentsHandler(self):
            config.line_segments=self.line_segments
        @on_trait_change('number_layers')
        def numLayersHandler(self):
            config.numLayers=self.number_layers
        @on_trait_change('stadium_Coil_Bool')
        def stadiumCoilBoolHandler(self):
            #Adjust so that inner rect width = 2 x corner radius + 1 space.
            #The additional space width is so that the return coil doesn't produce a non-realizable geometry
            #This is as close as we can come to a true stadium
            if(self.stadium_Coil_Bool):
                self.inner_coil_width = 2 * self.inner_corner_rad + self.trace_spacing
                config.inner_coil_width=self.inner_coil_width
        @on_trait_change('coil_RR_invert_Bool')
        def coilRRInvertBoolHandler(self):
            config.coil_RR_invert_Bool=self.coil_RR_invert_Bool
            #if(self.stadium_Coil_Bool):
                #Make 
        @on_trait_change('evenLayerSVG,even_transXSVG,even_transYSVG,even_scaleSVG,even_flipHorzSVG,even_flipVertSVG,even_autoCenterSVG')
        def evenLayerHandler(self):
            config.evenLayerSVG = self.evenLayerSVG
            config.evenTransXSVG = self.even_transXSVG
            config.evenTransYSVG = self.even_transYSVG
            config.evenScaleSVG = self.even_scaleSVG
            config.evenFlipHorzSVG = self.even_flipHorzSVG
            config.evenFlipVertSVG = self.even_flipVertSVG
            config.evenAutoCenterSVG = self.even_autoCenterSVG
        @on_trait_change('oddLayerSVG,odd_transXSVG,odd_transYSVG,odd_scaleSVG,odd_flipHorzSVG,odd_flipVertSVG,odd_autoCenterSVG')
        def oddLayerSVGHandler(self):
            config.oddLayerSVG = self.oddLayerSVG
            config.oddTransXSVG = self.odd_transXSVG
            config.oddTransYSVG = self.odd_transYSVG
            config.oddScaleSVG = self.odd_scaleSVG
            config.oddFlipHorzSVG = self.odd_flipHorzSVG
            config.oddFlipVertSVG = self.odd_flipVertSVG
            config.oddAutoCenterSVG = self.odd_autoCenterSVG
        @on_trait_change('lockEvenOddSVG')
        def lockEvenOddSVGHandler(self):
            config.lockEvenOddSVG = self.lockEvenOddSVG
        @on_trait_change('sampPointsSVG')
        def sampPointsSVGHandler(self):
            config.sampPointsSVG = self.sampPointsSVG
        @on_trait_change('importFilesSVG')
        def importFilesSVGHandler(self):
            #Verify Files exists and test run through converter
            try:
                #xa, ya = importSVG(config.evenLayerSVG, config.sampPointsSVG)
                #xb, yb = importSVG(config.oddLayerSVG, config.sampPointsSVG)
                xa, ya = importSVG(config.evenLayerSVG,config.evenTransXSVG,config.evenTransYSVG,config.evenScaleSVG,config.evenFlipHorzSVG,config.evenFlipVertSVG,config.evenAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                xb, yb = importSVG(config.oddLayerSVG,config.oddTransXSVG,config.oddTransYSVG,config.oddScaleSVG,config.oddFlipHorzSVG,config.oddFlipVertSVG,config.oddAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                if((xa.size) and (ya.size) and (xb.size) and (yb.size)):
                    self.svgImportOK = True
                    writeConsole('SVG Files read OK!','green')
                else:
                    self.svgImportOK = False
                    writeConsole('Error parsing SVG Files!','red')
            except FileNotFoundError:
                self.svgImportOK = False
                writeConsole('Error opening files!','red')
                return
        
class PCBinfoPanel(HasTraits):
    copper_weight=Enum('0.5','1.0','1.5','2.0','2.5','3.0','3.5','4.0','4.5','5.0')
    copper_thickness_CALC_init = ozPerSqft_to_mil(float(config.copperWeight_init))
    copper_thickness_CALC = String(str(copper_thickness_CALC_init))
    trace_width = Range(0.0, 1000.0, config.traceWidth_init)
    pcb_construction=Enum('PrePreg-Core-PrePreg','Core-PrePreg-Core')
    #If Pre-preg first, then standard stackup. Remember coil 1 is at ZERO and stacks up.
    #So stackup: 4-layer Copper-PP-Copper-Core-Copper-PP-Copper
    #Coil 0 is first coil. An even coil. So in this case, 1st odd coil is at Copper+PP
    #If Core-first, then Copper+Core 
    pcb_Width = String(config.pcbWidth_init)
    pcb_Height = String(config.pcbHeight_init)
    prepreg_thickness=Range(0.0,1000.0, config.prepreg_thickness_init)
    core_thickness=Range(0.0,1000.0,config.core_thickness_init)
    total_thickness_CALC_init = getTotalPCBThickness(config.prepreg_thickness, config.core_thickness, ozPerSqft_to_mil(float(config.copperWeight_init)), config.numLayers)
    total_thickness_CALC = String(str(total_thickness_CALC_init))
    #Items in GPCBinfoList will be saved.
    PCBinfoList = ['copper_weight','trace_width','pcb_construction','pcb_Width','pcb_Height','prepreg_thickness','core_thickness']
    pcbInfoView = View(
                    Item('copper_weight',label='Copper Weight (oz./sqft)'),
                    Item('copper_thickness_CALC', label='Copper Thickness (mil)', style = 'readonly'),
                    Item('trace_width',label='Copper Trace Width'),
                    Item('pcb_Width', label='Width (X-dim of PCB)'),
                    Item('pcb_Height', label='Height (Y-dim of PCB)'),
                    Item('pcb_construction',label='Stack-up Order'),
                    Item('prepreg_thickness',label='Pre-preg layer thickness'),
                    Item('core_thickness',label='Core layer thickness'),
                    Item('total_thickness_CALC', label='Total Thickness (mil)', style = 'readonly')
                    )
    #Update shared variables
    @on_trait_change('copper_weight')
    def copperWeightHandler(self):
        config.copper_weight=self.copper_weight
        self.copper_thickness_CALC = ozPerSqft_to_mil(float(self.copper_weight))
    @on_trait_change('trace_width')
    def traceWidthHandler(self):
        config.trace_width=self.trace_width
    @on_trait_change('pcb_Width')
    def pcbWidthHandler(self):
        config.pcb_width=self.pcb_Width
    @on_trait_change('pcb_Height')
    def pcbHeightHandler(self):
        config.pcb_height=self.pcb_Height
    @on_trait_change('pcb_construction')
    def pcbConstructionHandler(self):
        config.pcb_construction=self.pcb_construction
    @on_trait_change('prepreg_thickness')
    def prepregThicknessHandler(self):
        config.prepreg_thickness=self.prepreg_thickness
    @on_trait_change('core_thickness')
    def coreThicknessHandler(self):
        config.core_thickness=self.core_thickness
    @on_trait_change('copper_weight, pcb_construction, prepreg_thickness, core_thickness')
    def totalThicknessHandler(self):
        self.total_thickness_CALC = getTotalPCBThickness(config.odd_layer_separation, config.even_layer_separation, ozPerSqft_to_mil(float(self.copper_weight)), config.numLayers)

'''
***********************
* Main GUI Constuctor *
***********************
'''

                                           
class Applique_Main(HasTraits):
#Core Designer Panels
    coilPanel = Instance(CoilPanel, ())
    pcbPanel = Instance(PCBinfoPanel, ())
    graphicsPanel = Instance(GraphicsPanel, ())
    filePanel = Instance(filePanel, ())
    helpPanel = Instance(helpPanel, ())
#Modules Import Dynamically
    instanceString = ""
    itemString = """module_group=Group("""
    for i, module in enumerate(moduleList):
        instanceString ="tab_" + str(i) + " = Instance(moduleList[" + str(i) + "].SSL_designer_Tab(), ())"
        exec(instanceString)
        itemString = itemString + """Item(name='tab_""" + str(i) + """', style='custom', label='""" + str(moduleList[i].sslLabel) +"""', show_label=False)"""
        if i < (len(moduleList)-1):
            itemString = itemString + ","
    itemString = itemString + """,layout='tabbed', label='Plug-Ins')"""
    exec(itemString)
    #End Dynamic Initialization

#Initialize Scene
    plotMode = -1
    scene = Instance(MlabSceneModel, ())
    plotInstList = []
    firstPlot = True
    firstPCBdraw = True
    pcbOutlinePlot = None
    config.odd_layer_separation = 0.0
    config.even_layer_separation = 0.0
    config.copper_thickness = 0.0
    coilSVG = False
    @on_trait_change('scene.activated')
    def defaultGFX(self):
        self.graphicsPanel.plotType = config.plotType_init
        self.plotMode = self.graphicsPanel.plotTypeValues.index(config.plotType_init)
    @on_trait_change('scene.activated, coilPanel.svgImportOK')
    def updateSVGstatus(self):
        self.coilSVG = self.coilPanel.svgImportOK
    @on_trait_change('scene.activated, pcbPanel.copper_weight,pcbPanel.pcb_construction,pcbPanel.prepreg_thickness,pcbPanel.core_thickness')
    def define_coil_separation(self):
        config.copper_thickness = ozPerSqft_to_mil(float(self.pcbPanel.copper_weight))
            #print(config.copper_thickness)
            #Zero Coil is copper_thickness/2
            #One is 2 * copper_thickness/2 + odd
            #Two is 3 * copper_thickness/2 + odd + even
            #Add (n+1) * copper_thickness/2
        if(self.pcbPanel.pcb_construction=='PrePreg-Core-PrePreg'):
            #print('PP-CORE-PP')
            config.odd_layer_separation=self.pcbPanel.prepreg_thickness
            config.even_layer_separation=self.pcbPanel.core_thickness
            #print(self.pcbPanel.prepreg_thickness)
        elif(self.pcbPanel.pcb_construction=='Core-PrePreg-Core'):
            #print('CORE-PP-CORE')
             config.odd_layer_separation=self.pcbPanel.core_thickness
             config.even_layer_separation=self.pcbPanel.prepreg_thickness

    @on_trait_change('graphicsPanel.plotType')
    def change_plotType(self):
        if((self.coilPanel.coil_type == 'Circular' or self.coilPanel.coil_type == 'Rounded Rectangle') and not self.firstPlot):
            writeConsole('Changing to ' + self.graphicsPanel.plotType + ' mode','green')
            self.plotMode = self.graphicsPanel.plotTypeValues.index(self.graphicsPanel.plotType)
            self.new_line_seg()
        elif(self.coilPanel.coil_type == 'Import SVG' and self.graphicsPanel.plotType != 'Single Tube'):
            writeConsole('Cannot change to ' + self.graphicsPanel.plotType + ' mode','yellow')
            self.graphicsPanel.plotType == 'Single Tube'
    def updateBar(self):
        self.scene.mlab.scalarbar(orientation='vertical',title='Z(mils)')
    #First generate necessary plot instances
    @on_trait_change('scene.activated')
    def generate_plot_instances(self):
        i = 0
        self.plotInstList = [None] * self.coilPanel.number_layers
        while i < self.coilPanel.number_layers :
            self.plotInstList[i] = Instance(PipelineBase)
            #print("Generating Plot number "+str(i))
            i+=1
        #Call Update plot now
        self.update_plot_dyn()

            
    @on_trait_change('coilPanel.turns')
    def coil_type_redirect(self):
        if self.coilPanel.coil_type == 'Circular':
            self.update_plot_dyn()
        elif self.coilPanel.coil_type == 'Rounded Rectangle':
            self.new_line_seg()
#***********
#GFX Methods
#***********
    @on_trait_change('''coilPanel.inner_corner_rad,pcbPanel.trace_width,coilPanel.coil_type,coilPanel.inner_coil_width,coilPanel.inner_coil_height,
    coilPanel.trace_spacing,coilPanel.outer_diam,coilPanel.turns''')
    def check_geometry(self):
        #Stop non-physical rounded rectangle geometries
        if(self.coilPanel.coil_type == 'Rounded Rectangle'):
            if(self.coilPanel.inner_corner_rad < self.pcbPanel.trace_width/2.0):
                writeConsole('Bad Geometry Warning: Increase inner corner radius or reduce trace width.','yellow')
            if(self.coilPanel.inner_corner_rad>self.coilPanel.inner_coil_width/2.0 or self.coilPanel.inner_corner_rad>self.coilPanel.inner_coil_height/2.0):
                writeConsole('Bad Geometry Warning: Decrease inner corner radius or increase inner rectangle size.','yellow')
        elif(self.coilPanel.coil_type == 'Circular'):
            spaceRate = (self.pcbPanel.trace_width + self.coilPanel.trace_spacing)/float(2*np.pi)
            #print(spaceRate)
            if(abs(spaceRate) < 1e-14):
                spaceRate = 1e-14
            beginTurns = (((self.coilPanel.outer_diam-self.pcbPanel.trace_width)/2+self.pcbPanel.trace_width/2)-(spaceRate*2*np.pi*self.coilPanel.turns))/(spaceRate*2*np.pi)
            #We need to make sure that beginTurns is larger than zero. We can't have conductor at (0,0,0) until we modify reference point in FasterCap
            if(beginTurns <= 0):
                writeConsole('Bad Geometry Warning: Increase outer diameter, decrease number of turns, decrease trace width or spacing','yellow')
    #****************************************************************************
    #In polygonal loop mode, a change in port MAY cause a change in line segments
    #****************************************************************************
    @on_trait_change('''coilPanel.poly_port_location, coilPanel.poly_port_gap''')
    def check_polygon_loop_segments(self):
        #if(poly_port_gap >= delTheta) then NEW SEGS
        delTheta = 360/self.coilPanel.line_segs_per_turn
        if(self.coilPanel.poly_port_gap >= delTheta):
            self.new_line_seg()
        else:
            self.update_plot_dyn()

    @on_trait_change('''coilPanel.inner_coil_width,coilPanel.inner_coil_height,coilPanel.inner_corner_rad,pcbPanel.trace_width,
    coilPanel.trace_spacing, coilPanel.outer_diam,coilPanel.axial_offset_x,coilPanel.axial_offset_y,pcbPanel.prepreg_thickness,
    pcbPanel.core_thickness,pcbPanel.pcb_construction,coilPanel.even_flipHorzSVG,coilPanel.even_flipVertSVG,coilPanel.odd_flipHorzSVG,
    coilPanel.odd_flipVertSVG,coilPanel.even_scaleSVG,coilPanel.odd_scaleSVG,coilPanel.even_transXSVG,coilPanel.even_transYSVG,
    coilPanel.odd_transXSVG,coilPanel.odd_transYSVG,coilPanel.even_autoCenterSVG,coilPanel.odd_autoCenterSVG,pcbPanel.copper_weight''')
    def update_plot_dyn(self):
        #Graphics Generation for single tube representation
        if self.plotMode == 0 or self.plotMode == 1 or self.plotMode == 2:
            if(not self.coilSVG):
                xa, ya, xb, yb = single_tube_master(self.coilPanel.inner_coil_width,self.coilPanel.inner_coil_height,self.coilPanel.inner_corner_rad,self.pcbPanel.trace_width, self.coilPanel.trace_spacing, self.coilPanel.turns,
                                            self.coilPanel.outer_diam, self.coilPanel.axial_offset_x, self.coilPanel.axial_offset_y,
                                            self.coilPanel.line_segments, self.coilPanel.rect_coil_straight_section_segs_X, self.coilPanel.rect_coil_straight_section_segs_Y, self.coilPanel.midPoints_segments, 0,
                                            self.coilPanel.number_layers,self.coilPanel.coil_type,self.plotMode, self.coilPanel.poly_port_location, self.coilPanel.poly_port_gap)
            else:
                #This may do nothing here. Axial_offset?
                xa, ya = importSVG(config.evenLayerSVG,config.evenTransXSVG,config.evenTransYSVG,config.evenScaleSVG,config.evenFlipHorzSVG,config.evenFlipVertSVG,config.evenAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                xb, yb = importSVG(config.oddLayerSVG,config.oddTransXSVG,config.oddTransYSVG,config.oddScaleSVG,config.oddFlipHorzSVG,config.oddFlipVertSVG,config.oddAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
            z_Tube = max(self.graphicsPanel.min_tube_radius,config.copper_thickness/2) #Keeps Plot 3D tube radius from complaining when the plot extends below zero
            zArrayList=generate_z_arrays(len(xa),config.odd_layer_separation,config.even_layer_separation,self.coilPanel.number_layers,config.copper_thickness,self.graphicsPanel.min_tube_radius,'GFX',1)
            if self.firstPlot:
                writeConsole('Initializing Plots...','green')
                vmax_value = self.coilPanel.number_layers * max(self.pcbPanel.core_thickness,self.pcbPanel.prepreg_thickness) #modify this to be accurate
                i=0
                while i < self.coilPanel.number_layers:
                    #print self.plotInstList[i]
                    if i == 0:
                        self.plotInstList[i] = self.scene.mlab.plot3d(xa,ya,zArrayList[i],zArrayList[i], tube_radius = z_Tube, representation = 'surface', vmin=1, vmax=vmax_value, colormap = "jet")
                        self.updateBar()
                    elif (self.coilPanel.number_layers>1 and (i % 2 == 0)):
                        self.plotInstList[i] = self.scene.mlab.plot3d(xa,ya,zArrayList[i],zArrayList[i],
                        tube_radius = z_Tube, representation = 'surface', vmin=1, vmax=vmax_value, colormap = "jet")
                    elif (self.coilPanel.number_layers>1 and (i % 2 != 0)):
                        self.plotInstList[i] = self.scene.mlab.plot3d(xb,yb,zArrayList[i], zArrayList[i],
                        tube_radius = z_Tube, representation = 'surface', vmin=1, vmax=vmax_value, colormap = "jet")
                    i+=1
                self.firstPlot = False
            else:
                vmax_value = self.coilPanel.number_layers * max(self.pcbPanel.core_thickness,self.pcbPanel.prepreg_thickness) #NEED to modify this to be accurate
                i=0
                while i < self.coilPanel.number_layers:
                    if i % 2 == 0:
                        #Even number
                        self.plotInstList[i].mlab_source.set(x=xa,y=ya,z=zArrayList[i])
                    else:
                        self.plotInstList[i].mlab_source.set(x=xb,y=yb,z=zArrayList[i])
                    i+=1

    #**************************************************
    #Changes Here require a new number of line segments
    #**************************************************
    @on_trait_change('''coilPanel.svgImportOK,coilPanel.line_segments,coilPanel.number_layers,coilPanel.coil_type,graphicsPanel.min_tube_radius,
    coilPanel.rect_coil_straight_section_segs_X,coilPanel.rect_coil_straight_section_segs_Y,coilPanel.midPoints_segments''')
    def new_line_seg(self):
            #Remove instances, then generate whats necessary.
            #self.plotInstList = []        
            #self.generate_plot_instances()
            num_curr_layers = len(self.plotInstList)
            i=0
            while i<num_curr_layers :
                    self.plotInstList[i].remove()
                    i+=1
            self.plotInstList = [None] * self.coilPanel.number_layers
            i=0
            while i < self.coilPanel.number_layers :
                    self.plotInstList[i] = Instance(PipelineBase)
                    i+=1
            if self.plotMode == 0 or self.plotMode == 1 or self.plotMode == 2:
                if(not self.coilSVG):
                    xa, ya, xb, yb = single_tube_master(self.coilPanel.inner_coil_width,self.coilPanel.inner_coil_height,self.coilPanel.inner_corner_rad,self.pcbPanel.trace_width, self.coilPanel.trace_spacing, self.coilPanel.turns, self.coilPanel.outer_diam,
                                                self.coilPanel.axial_offset_x, self.coilPanel.axial_offset_y, self.coilPanel.line_segments, self.coilPanel.rect_coil_straight_section_segs_X, self.coilPanel.rect_coil_straight_section_segs_Y,
                                                self.coilPanel.midPoints_segments, 0, self.coilPanel.number_layers,self.coilPanel.coil_type,self.plotMode, self.coilPanel.poly_port_location, self.coilPanel.poly_port_gap)
                else:
                    xa, ya = importSVG(config.evenLayerSVG,config.evenTransXSVG,config.evenTransYSVG,config.evenScaleSVG,config.evenFlipHorzSVG,config.evenFlipVertSVG,config.evenAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
                    xb, yb = importSVG(config.oddLayerSVG,config.oddTransXSVG,config.oddTransYSVG,config.oddScaleSVG,config.oddFlipHorzSVG,config.oddFlipVertSVG,config.oddAutoCenterSVG,config.sampPointsSVG,config.trace_width,0)
            z_Tube = max(self.graphicsPanel.min_tube_radius,config.copper_thickness/2) #Keeps Plot 3D tube radius from complaining when the plot extends below zero
            zArrayList=generate_z_arrays(len(xa),config.odd_layer_separation,config.even_layer_separation,self.coilPanel.number_layers,config.copper_thickness,self.graphicsPanel.min_tube_radius,'GFX',1)
            vmax_value = self.coilPanel.number_layers * max(self.pcbPanel.core_thickness,self.pcbPanel.prepreg_thickness)
            i=0
            while i < self.coilPanel.number_layers:
                if self.plotMode == 0 or self.plotMode == 1 or self.plotMode == 2:
                    if i % 2 == 0:
                            self.plotInstList[i] = self.scene.mlab.plot3d(xa,ya,zArrayList[i],zArrayList[i], tube_radius = z_Tube, representation = 'surface', vmin=1, vmax=vmax_value, colormap = "jet")
                    else:
                            self.plotInstList[i] = self.scene.mlab.plot3d(xb,yb,zArrayList[i],zArrayList[i], tube_radius = z_Tube, representation = 'surface', vmin=1, vmax=vmax_value, colormap = "jet")
                i+=1
    #***********************
    #PCB Outline Generation
    #***********************
    @on_trait_change('scene.activated, graphicsPanel.showPCB_Bool')
    def show_remove_PCB_outline(self):
        if(self.graphicsPanel.showPCB_Bool):
            self.pcbOutlinePlot = Instance(PipelineBase)
            self.update_PCB_outline()
        else:
            #Remove it
            self.pcbOutlinePlot.remove()
            self.pcbOutlinePlot = None
            self.firstPCBdraw = True
    @on_trait_change('pcbPanel.copper_weight, pcbPanel.prepreg_thickness, pcbPanel.core_thickness, pcbPanel.pcb_construction, pcbPanel.pcb_Width, pcbPanel.pcb_Height,coilPanel.number_layers')
    def update_PCB_outline(self):
        if(self.graphicsPanel.showPCB_Bool):
            x_PCB, y_PCB, z_PCB = gen_PCBcuboid(config.pcb_width, config.pcb_height, config.odd_layer_separation, config.even_layer_separation, config.copper_thickness, config.numLayers)
            if(self.pcbOutlinePlot is None or self.firstPCBdraw):
                #Draw it
                self.pcbOutlinePlot = self.scene.mlab.plot3d(x_PCB, y_PCB, z_PCB , z_PCB, tube_radius = 5, representation = 'surface', color = (0.050, 0.611, 0.0944))
                self.firstPCBdraw = False
            else:
                #Update it
                self.pcbOutlinePlot.mlab_source.set(x=x_PCB,y=y_PCB,z=z_PCB)

#*********************
#Settings Save Methods
#*********************
    @on_trait_change('filePanel.saveButton')
    def save_Settings(self):
            writeConsole('Saving User Settings','green')
            #Gather all user settings and build dictionary
            settingsDict = {}
            #GFX Settings
            for setting in self.graphicsPanel.GraphicsList:
                    settingsDict['graphicsPanel_'+setting]=['graphicsPanel', setting, getattr(self.graphicsPanel,setting)]
            #Coil Settings
            for setting in self.coilPanel.CoilList:
                    settingsDict['coilPanel_'+setting]=['coilPanel', setting, getattr(self.coilPanel,setting)]
            #PCBINFO Settings
            for setting in self.pcbPanel.PCBinfoList:
                settingsDict['pcbPanel_'+setting]=['pcbPanel', setting, getattr(self.pcbPanel,setting)]
            if self.filePanel.saveFileName == '':
                    self.filePanel.saveFileName = "applique_user_settings.dat"
            save_settings_to_file(settingsDict,self.filePanel.saveLoc,self.filePanel.saveFileName)
            return
    @on_trait_change('filePanel.loadButton')
    def load_Settings(self):
        writeConsole('Loading User Settings','green')
        if self.filePanel.loadFileName == '':
            writeConsole('Please enter a valid file name','red')
            return
        settingsDict = load_settings_from_file(self.filePanel.loadFileName)
        if(settingsDict is None):
            return
        for key,value in settingsDict.items():
            #subObj = eval('self.'+value[0])
            setattr(eval('self.'+value[0]), value[1], value[2])

#***********
#GUI Layout
#***********
        # The layout of the dialog created
    view = View(HSplit(
                    Item('scene', editor=SceneEditor(scene_class=MayaviScene),height=450, width=600, show_label=False, style_sheet=sceneItem),
                        Group(
                            Item('coilPanel', style='custom', label='Coil Geometry', show_label=False, width = 500),
                            Item('pcbPanel', style='custom', label='PCB', show_label=False),
                            Item(name='graphicsPanel',style='custom', label='GFX Settings', show_label=False),
                            Include('module_group'),
                            Item(name='filePanel',style='custom', label='Save / Load', show_label=False),
                            Item(name='helpPanel',style='custom',label='Help', show_label=False),layout='tabbed'
                             ), style_sheet = mainGroup
                        ),resizable=True, title='Appliqué', icon=ImageResource('applique_icon.png')
                    )

#************************
#* Main GUI Opens Here  *
#************************
#load default settings
#load modules from settings file.
if __name__== "__main__":
    mainGUI = Applique_Main()
    mainGUI.configure_traits()

