# Appliqué

## Overview

Appliqué is a GUI-based, printed circuit board (PCB), multi-layer planar-spiral coil modelling design environment. It is open-source and free. Appliqué leverages open-source and free external applications 
to rapidly predict impedance during design time. Inductive, capacitive and resistive parasitics are extracted with [FastHenry2](https://www.fastfieldsolvers.com/fasthenry2.htm) and 
[FasterCap](https://www.fastfieldsolvers.com/fastercap.htm).The parasitic values are then used to quantify a SPICE compatible circuit model, thereby allowing quick, lumped-element numerical analysis. 
[NgSpice](http://ngspice.sourceforge.net/) and [LTSpice](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html#) are supported.

Although not required for simulation, Appliqué can export designs to ASCII-based files for use in [KiCad](https://kicad-pcb.org/) or [Altium Designer](https://www.altium.com/altium-designer/).

## Requirements

### Operating System

-  Windows 10 64-bit: Presently Appliqué is only supported on Windows 10 64-bit. While this application most likely works on other versions of Windows, it has not been tested.

### Software and Installation

- [Python v3.6.x to v3.7.x, Windows](https://www.python.org/downloads/windows/)
- [FastHenry2](https://www.fastfieldsolvers.com/fasthenry2.htm) 
- [FasterCap](https://www.fastfieldsolvers.com/fastercap.htm)
- [NgSpice](http://ngspice.sourceforge.net/) or [LTSpice](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html#)

### Installation Instructions

#### Python

##### Python 3 Base

Python v3.6.x to v3.7.x.

Presently Python v3.8.x is not supported do to dependency issues with pyqtwebkit.

Suggested version is [v3.7.7](https://www.python.org/downloads/release/python-377/).

During the installation of Python 3, please check the "add to path" option. If not selected during installation, the Python 3 application path may be added to the
Environment variables later, followed by a reboot.

##### Microsoft Visual C++ Build Tools #####

Traits requires Microsoft Visual C++ Build Tools. Before installing traits install the latest version [here](https://visualstudio.microsoft.com/visual-cpp-build-tools/).
##### Extrinsic Python 3 Modules

-  [NumPy](https://numpy.org/)
-  [SciPy](https://www.scipy.org/)
-  [MatplotLib](https://matplotlib.org/)

-  [SVG Path Tools](https://pypi.org/project/svgpathtools/)
-  [Descartes](https://pypi.org/project/descartes/)
-  [Traits](https://docs.enthought.com/traits/)
-  [TraitsUI](https://docs.enthought.com/traitsui/)
-  [MayaVi](https://docs.enthought.com/mayavi/mayavi/)
-  [PyFace](https://docs.enthought.com/pyface/)
-  [Colorama](https://pypi.org/project/colorama/)
-  [Termcolor](https://pypi.org/project/termcolor/)

###### Manual Installation Instructions

The Python modules required for applique are installed via the Windows Command Prompt or PowerShell. Run cmd.exe and enter the following commands:

	pip install termcolor colorama pywin32 descartes svgpathtools numpy scipy matplotlib

Python VTK is most easily installed by using [pre-built binaries](https://www.lfd.uci.edu/~gohlke/pythonlibs/). Make sure the pre-built binary is appropriate
for the Python version and the Windows platform. 

For Python v3.6.x on Windows 64-bit, VTK may be installed via the following command:

	pip install -index-url https://download.lfd.uci.edu/pythonlibs/w3jqiv8s/VTK-8.2.0-cp36-cp36m-win_amd64.whl

For Python v3.7.x on Windows 64-bit:

	pip install -index-url https://download.lfd.uci.edu/pythonlibs/w3jqiv8s/VTK-8.2.0-cp37-cp37m-win_amd64.whl

Lastly:

	pip install pyqt5 pyqtwebkit traitsui mayavi

###### Automatic Installation

A Python script, entitled install_modules.py, automatically installs the above modules via pip. Before installation, Python v3.6.x or v3.7.x must be installed, and
Python must be added to the system path.

To run the script, enter the following at the prompt for v3.6.x:

	python install_modules.py -v 6

Or for v3.7.x:

	python install_modules.py -v 7


#### FastHenry2 and FasterCap

Both FastHenry2 and FasterCap may be compiled and installed independently. For ease of installation, it is recommended to install the [FastFieldSolvers bundle binary for Windows](https://www.fastfieldsolvers.com/download.htm).
The FastFieldSolvers bundle includes FastHenry2 and FasterCap, as well as E.M. Workbench, which is a GUI for editing and 3D visualization. To install the bundle, download the binary and install using default options.

##### SPICE

There are two options for the SPICE simulator.

##### NgSpice

[NgSpice](http://ngspice.sourceforge.net/) may be downloaded and placed in any location on the computer.

##### LTSpice

[LTSpice](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html#) may be installed using the default installation options.

## User Instructions

User manual is located in the [user_manual](user_manual/README.md) folder.

## Bugs
	Automodel crashes on erroneous FasterCap exit: Positive off-diagonal values.
	When 2-layers used, pre-preg used and not core.
	Total thickness changes unexpectantly when manipulating copper weight, pre-preg thickness and core thickness.
	Faster Cap and Fast Henry write locations other than default may cause issues.
	Check number warning on Faster Cap dissipation factor.

## Future Work

	Python Plot markers with Frequency of operation and SRF.
	Add Apparent Extracted values to export report.
	Improved delpoyment of software. Single executable for installation.
	Expanded coil geometry.
	Wireless power system analysis: 2, 3 and 4-coil systems.
	PCB CAD via placement
	Add "terminate" ability to manual control of apps.

